document
.getElementById('beautify-button')
.addEventListener('click', (e)=>{
    let input = document.getElementById('js-input')
    let output = document.getElementById('js-output')
    
    output.innerText = js_beautify(input.value)
})
