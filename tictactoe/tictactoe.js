const possibleWins = [ [0,1,2], [3,4,5], [6,4,8], [0,3,6], [1,4,7], [2,5,8], [0,4,8], [2,4,6] ]

class TicTacToe {
    constructor(first_turn){
        this.board = Array(9).fill(null, 0, 9)
        this.turn = first_turn
    }

    getCell(x, y){
        return this.board[x + y * 3]
    }

    setCell(x, y, value){
        this.board[x + y * 3] = value
    }

    play(x, y){
        if(this.getCell(x,y) == null){
            let last_turn = this.turn
            this.setCell(x, y, this.turn)
            this.updateState()
            
            return {
                state : this.state,
                last_turn,
                last_move : [x, y],
            }
        }
        
        return {
            state : 'invalid_play'
        }
    }

    updateState(){
        let wins = possibleWins.some( poss => poss.every(v=>this.board[v] === this.turn))

        if(wins){
            this.state = this.turn === 'x' ? 'x_wins' : 'o_wins'
        }
        else{
            if(this.board.every(Boolean)){
                this.state = 'tie'
            }
            else{
                this.state = 'playing'
                this.turn = this.turn === 'x' ? 'o' : 'x'
            }
        }
    }
}

class ConsoleTicTacToe {
    constructor(first_turn){
        this.engine = new TicTacToe(first_turn)
    }

    play(x, y){
        let state = this.engine.play(x, y)
        this.printBoard()
        console.log(state)
    }

    printBoard(){
        console.log(this.engine.board)
    }
}