document
.getElementById('beautify-button')
.addEventListener('click', (e)=>{
    let input = document.getElementById('html-input')
    let output = document.getElementById('html-output')
    
    output.innerText = html_beautify(input.value)
})