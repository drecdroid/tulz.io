# GIT
git pull origin master
git push origin master


From [vscode documentation](https://code.visualstudio.com/updates/vMarch#_setup)

```
Wait support and Git patch/diff mode
When you run VS Code from the command line, there is a new argument (--wait) you can pass to make the command wait until you have closed the current VS Code instance. This can be used with applications that can configure an external editor for file changes.

For example, Git allows you to configure an external editor and here are the steps to do so:

Make sure you can run code --help from the command line and you get help.
if you do not see help, please follow these steps:
Mac: Select Shell Command: Install 'Code' command in path from the Command Palette.
Windows: Make sure you selected Add to PATH during the installation.
Linux: Make sure you installed Code via our new .deb or .rpm packages.
From the command line, run git config --global core.editor "code --wait"
Now you can run git config --global -e and use VS Code as editor for configuring Git.



Add the following to enable support for using VS Code as diff tool:

[diff]
    tool = default-difftool
[difftool "default-difftool"]
    cmd = code --wait --diff $LOCAL $REMOTE
This leverages the new --diff option you can pass to VS Code to compare two files side by side.

To summarize, here are some examples of where you can use Git with VS Code:

git rebase HEAD~3 -i allows to interactive rebase using VS Code
git commit allows to use VS Code for the commit message
git add -p followed by e for interactive add
git difftool <commit>^ <commit> allows to use VS Code as diff editor for changes
```

# Surge

# NPM

`npm init`

Es un comando auxiliar para crear usando una plantilla el programa `package.json` preguntando por ciertos valores.

`npm install --save {package_name}`

Es un comando que descarga e instala el paquete especificado dentro de la carpeta `node_modules` del proyecto
y como fue ejecutado con el modificador `--save` agregará el nombre del paquete y la versión que instalado
en el archivo `package.json`

`npm install {package_name}`

Instal un paquete pero no lo agrega al `package.json`

`npm install`

Lee el valor `dependencies` y `dev_dependencies` del archivo `package.json` y descargará e instalará los paquetes
que se encuentren esos valores y que no estén aun instalados en `node_modules`.

`npm install -g {package_name}`

Instala un paquete a nivel global dentro de la carpeta que está especificada en el path para la instalación de
paquetes globales de npm. Y estos paquetes instalados podrán ser ejecutados directamente desde la consola.
Por lo general se instalan de esta manera los programas ejecutables.

# Node

# Browserify

Browserify es un programa de consola de comandos que interpreta las llamadas a la función `require` como lo haría
Node para generar un archivo único de javascript el cuál será el que se utilice en el navegador

## Instalando Browserify

```
npm install -g browserify
npm install -g watchify
```

## Comandos Básicos de Browserify

`browserify {archivo_principal} -o {archivo_de_salida}`

Genera el archivo `{archivo_de_salida}` a partir de los contenido de `{archivo_principal}

`browserify --debug {archivo_principal} -o {archivo_de_salida}`

Incorpora los sourcemaps(una vista a las estructura original de los archivos) para facilitar la depuración
en el navegador

`watchify ...`

Es una variante de browserify que puede recibir los mismo comandos de este pero añade la funcionalidad de ejecutar
una recompilación cada vez que un archivo de los que se va a compilar se modifica.

# Serve

Serve es un programa de consola de comandos que ejecuta un servidor HTTP local simple para poder hacer pruebas.

## Instalando Serve

```
npm install -g serve
```

## Comandos básicos de serve

`serve {carpeta_raiz}`
Corre un servidor local que tiene como ruta principal a la carpeta `{carpeta_raiz}` si no se especifica este argumento,
serve corre sobre el directorio actual

`serve -s {carpeta_raiz}`
Corre un servidor local como el anterior comando, con la diferencia de que cualquier ruta que resulte en 404 se redirigirá
automáticamente al index.html

