function called(){
    console.log(this)
}

called()

called.call("hola mundo")

called.bind("hola mundo")

function bind(thisArg){
    let that = this
    return function(){
        that.call(thisArg)
    }
}

