function setProto(obj, proto){
    Object.setPrototypeOf(obj, proto)
}

function getProto(obj){
    return Object.getPrototypeOf(obj)
}

function PCls(){

}

function Cls(){
    if(!Cls.prototype.something){
        Cls.prototype.something = function(){
            this.a += 10;
        }
    }

    if(Object.getPrototypeOf(Cls.prototype).constructor === Object){
        Object.setPrototypeOf(Cls.prototype, PCls.prototype)
    }

    let inst = Object.create(Cls.prototype)
    inst.a = 10
    return inst
}

var a = Cls();