//Array
let list1 = [ 1, 2, 3, 4 ]

let list2 = [ "as", "", "eadsd" ]

let list3 = [ false, true, false]

let list4 = [ function(){}, {}, 1, [1,2,3], "asdasd", ]


for(let i=0;i<list1.length;i++){
    console.log( list1[i] )
}

list1.push( 1, 2, 3, 4)
list1.splice(1)
let list5 = list1.concat([1,2,3], [4,5,6])


let lista_de_cuadrados = []

for(let i=0;i<list1.length;i++){
    lista_de_cuadrados.push(list1[i] * list1[i])
    //[1, 4, 9, 16]
}

let lista_de_cuadrados = list1.map(val=>val*val)
[1,2,3,4].reduce((pv, cv) => pv + cv , 0)
[1,2,3,4].foreach(v => console.log(v))
[1,2,3,4].foreach(console.log)

/**
 * El párametro handler tiene que ser una función que
 * reciba tres argumentos, el primero será el valor del elemento
 * actual, el segundo el indice actual, el tercero, el arreglo completo
 * y debe retornar un valor que se irá añadiendo al arreglo que se retornará
 * al final
 */
function map(handler){
    let to_return = []
    for(let i=0; i<this.length; i++){
        to_return.push(handler(this[i], i, this))
    }
    return to_return
}
