#!/usr/bin/env node
const fs = require('fs')
const h = require('hyperscript')

function html(data){
    return `
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>${data.site.title || ""}</title>
        <meta name="description" content="${data.site.description|| ""}">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <header>
            <a class="logo" href="/"><img src="img/logo.png"></a>
        </header>
        ${body(data)}
    </body>
</html>
`
}

function body(data){
    let menu_data = data

    let html =
    menu_data["menu"]
    .map(e=>menu_data["sections"].find(section=> section.id === e ))
    .map( section =>
        h('.category',
            h('h2.category-title', section.title),
            h('.tools-container',
                section.tools
                .map(t => Object.assign( t, menu_data.tools.find(tool=>tool.id === t.tool_id)))
                .map(tool => {
                    return h('.tool-wrapper',
                        h('a', { href : tool.link || "#" },
                            h(`.tool.tool-${tool.size}`,
                                tool.img_big || tool.img_medium ?
                                    h('.tool-image',
                                        h('img', { src : (tool.size === "big" ? tool.img_big : tool.img_medium) || ""})
                                    ):null,
                                h('.tool-info',
                                    h('h3.tool-title', tool.title),
                                    h('.tool-description', tool.description)
                                )
                            )                            
                        )
                    )
                })
            )
        ).outerHTML
    ).join('')

    return html
}

fs.readFile('src/data.json', 'utf-8', (err, data)=>{
    let html_output = html(JSON.parse(data))

    fs.writeFile('public/index.html', html_output)
})