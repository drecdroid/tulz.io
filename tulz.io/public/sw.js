function html(body){
    return `
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Tulz.io</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/style.css" rel="stylesheet">
        <link rel="preload" href="main.js" as="script">
    </head>
    <body>
        <header>
            <div class="logo"><img src="img/logo.png"></div>
        </header>
        ${body}
        <script src="main.js"></script>
    </body>
</html>
`
}

function body(data){
    let menu_data = data

    let html =
    menu_data["menu"]
    .map(e=>menu_data["sections"]
    .find(section=> section.id === e ))
    .map( section =>{
        return `
            <div class="category">
                <h2 class="category-title">${section.title}</h2>
                <div class="tools-container">
                ${
                    section.tools
                    .map(t => Object.assign( t, menu_data.tools.find(tool=>tool.id === t.tool_id)))
                    .map(tool => {
                        return `
                            <div class="tool-wrapper">
                                <a href="${tool.link||"#"}">
                                    <div class="tool tool-${tool.size}">
                                        ${tool.img_big || tool.img_medium ?
                                            `<div class="tool-image">
                                                <img src="${(tool.size === "big" ? tool.img_big : tool.img_medium) || ""}"/>
                                            </div>`: ""
                                        }                                    
                                        <div class="tool-info">
                                            <h3 class="tool-title">${tool.title}</h3>
                                            <p class="tool-description">${tool.description}</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        `
                    })
                    .join('')
                }
                </div>
            </div>
        `    
    }).join('')

    return html
}



const urls = [
    '/data.json',
    '/',
    '/index.html',
    '/main.js',
    '/css/style.css'
]


self.addEventListener('install', e => {
})

self.addEventListener('activate', e=>{
  e.waitUntil(self.clients.claim());
})

self.addEventListener('fetch', e=>{
    const url = new URL(e.request.url)

    if(url.origin === location.origin){
        if(url.pathname === '/'){
            e.respondWith(
                caches
                .match(url, { cacheName : 'v1' })
                .then(response => {
                    if(response){
                        return response
                    }

                    return fetch('/data.json')
                    .then(response => response.json())
                    .then(json => {
                        console.log(json)
                        let response = new Response(html(body(json)))
                        
                        response.headers.set('Content-Type', 'text/html')

                        return caches.open('v1').then(
                            cache => {
                                cache.put(url, response.clone())
                                return response
                            }
                        )
                    })
                    
                })
            )
        }
    }
})