(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/*jshint eqnull:true*/
(function (root) {
  "use strict";

  var GLOBAL_KEY = "Random";

  var imul = (typeof Math.imul !== "function" || Math.imul(0xffffffff, 5) !== -5 ?
    function (a, b) {
      var ah = (a >>> 16) & 0xffff;
      var al = a & 0xffff;
      var bh = (b >>> 16) & 0xffff;
      var bl = b & 0xffff;
      // the shift by 0 fixes the sign on the high part
      // the final |0 converts the unsigned value into a signed value
      return (al * bl) + (((ah * bl + al * bh) << 16) >>> 0) | 0;
    } :
    Math.imul);

  var stringRepeat = (typeof String.prototype.repeat === "function" && "x".repeat(3) === "xxx" ?
    function (x, y) {
      return x.repeat(y);
    } : function (pattern, count) {
      var result = "";
      while (count > 0) {
        if (count & 1) {
          result += pattern;
        }
        count >>= 1;
        pattern += pattern;
      }
      return result;
    });

  function Random(engine) {
    if (!(this instanceof Random)) {
      return new Random(engine);
    }

    if (engine == null) {
      engine = Random.engines.nativeMath;
    } else if (typeof engine !== "function") {
      throw new TypeError("Expected engine to be a function, got " + typeof engine);
    }
    this.engine = engine;
  }
  var proto = Random.prototype;

  Random.engines = {
    nativeMath: function () {
      return (Math.random() * 0x100000000) | 0;
    },
    mt19937: (function (Int32Array) {
      // http://en.wikipedia.org/wiki/Mersenne_twister
      function refreshData(data) {
        var k = 0;
        var tmp = 0;
        for (;
          (k | 0) < 227; k = (k + 1) | 0) {
          tmp = (data[k] & 0x80000000) | (data[(k + 1) | 0] & 0x7fffffff);
          data[k] = data[(k + 397) | 0] ^ (tmp >>> 1) ^ ((tmp & 0x1) ? 0x9908b0df : 0);
        }

        for (;
          (k | 0) < 623; k = (k + 1) | 0) {
          tmp = (data[k] & 0x80000000) | (data[(k + 1) | 0] & 0x7fffffff);
          data[k] = data[(k - 227) | 0] ^ (tmp >>> 1) ^ ((tmp & 0x1) ? 0x9908b0df : 0);
        }

        tmp = (data[623] & 0x80000000) | (data[0] & 0x7fffffff);
        data[623] = data[396] ^ (tmp >>> 1) ^ ((tmp & 0x1) ? 0x9908b0df : 0);
      }

      function temper(value) {
        value ^= value >>> 11;
        value ^= (value << 7) & 0x9d2c5680;
        value ^= (value << 15) & 0xefc60000;
        return value ^ (value >>> 18);
      }

      function seedWithArray(data, source) {
        var i = 1;
        var j = 0;
        var sourceLength = source.length;
        var k = Math.max(sourceLength, 624) | 0;
        var previous = data[0] | 0;
        for (;
          (k | 0) > 0; --k) {
          data[i] = previous = ((data[i] ^ imul((previous ^ (previous >>> 30)), 0x0019660d)) + (source[j] | 0) + (j | 0)) | 0;
          i = (i + 1) | 0;
          ++j;
          if ((i | 0) > 623) {
            data[0] = data[623];
            i = 1;
          }
          if (j >= sourceLength) {
            j = 0;
          }
        }
        for (k = 623;
          (k | 0) > 0; --k) {
          data[i] = previous = ((data[i] ^ imul((previous ^ (previous >>> 30)), 0x5d588b65)) - i) | 0;
          i = (i + 1) | 0;
          if ((i | 0) > 623) {
            data[0] = data[623];
            i = 1;
          }
        }
        data[0] = 0x80000000;
      }

      function mt19937() {
        var data = new Int32Array(624);
        var index = 0;
        var uses = 0;

        function next() {
          if ((index | 0) >= 624) {
            refreshData(data);
            index = 0;
          }

          var value = data[index];
          index = (index + 1) | 0;
          uses += 1;
          return temper(value) | 0;
        }
        next.getUseCount = function() {
          return uses;
        };
        next.discard = function (count) {
          uses += count;
          if ((index | 0) >= 624) {
            refreshData(data);
            index = 0;
          }
          while ((count - index) > 624) {
            count -= 624 - index;
            refreshData(data);
            index = 0;
          }
          index = (index + count) | 0;
          return next;
        };
        next.seed = function (initial) {
          var previous = 0;
          data[0] = previous = initial | 0;

          for (var i = 1; i < 624; i = (i + 1) | 0) {
            data[i] = previous = (imul((previous ^ (previous >>> 30)), 0x6c078965) + i) | 0;
          }
          index = 624;
          uses = 0;
          return next;
        };
        next.seedWithArray = function (source) {
          next.seed(0x012bd6aa);
          seedWithArray(data, source);
          return next;
        };
        next.autoSeed = function () {
          return next.seedWithArray(Random.generateEntropyArray());
        };
        return next;
      }

      return mt19937;
    }(typeof Int32Array === "function" ? Int32Array : Array)),
    browserCrypto: (typeof crypto !== "undefined" && typeof crypto.getRandomValues === "function" && typeof Int32Array === "function") ? (function () {
      var data = null;
      var index = 128;

      return function () {
        if (index >= 128) {
          if (data === null) {
            data = new Int32Array(128);
          }
          crypto.getRandomValues(data);
          index = 0;
        }

        return data[index++] | 0;
      };
    }()) : null
  };

  Random.generateEntropyArray = function () {
    var array = [];
    var engine = Random.engines.nativeMath;
    for (var i = 0; i < 16; ++i) {
      array[i] = engine() | 0;
    }
    array.push(new Date().getTime() | 0);
    return array;
  };

  function returnValue(value) {
    return function () {
      return value;
    };
  }

  // [-0x80000000, 0x7fffffff]
  Random.int32 = function (engine) {
    return engine() | 0;
  };
  proto.int32 = function () {
    return Random.int32(this.engine);
  };

  // [0, 0xffffffff]
  Random.uint32 = function (engine) {
    return engine() >>> 0;
  };
  proto.uint32 = function () {
    return Random.uint32(this.engine);
  };

  // [0, 0x1fffffffffffff]
  Random.uint53 = function (engine) {
    var high = engine() & 0x1fffff;
    var low = engine() >>> 0;
    return (high * 0x100000000) + low;
  };
  proto.uint53 = function () {
    return Random.uint53(this.engine);
  };

  // [0, 0x20000000000000]
  Random.uint53Full = function (engine) {
    while (true) {
      var high = engine() | 0;
      if (high & 0x200000) {
        if ((high & 0x3fffff) === 0x200000 && (engine() | 0) === 0) {
          return 0x20000000000000;
        }
      } else {
        var low = engine() >>> 0;
        return ((high & 0x1fffff) * 0x100000000) + low;
      }
    }
  };
  proto.uint53Full = function () {
    return Random.uint53Full(this.engine);
  };

  // [-0x20000000000000, 0x1fffffffffffff]
  Random.int53 = function (engine) {
    var high = engine() | 0;
    var low = engine() >>> 0;
    return ((high & 0x1fffff) * 0x100000000) + low + (high & 0x200000 ? -0x20000000000000 : 0);
  };
  proto.int53 = function () {
    return Random.int53(this.engine);
  };

  // [-0x20000000000000, 0x20000000000000]
  Random.int53Full = function (engine) {
    while (true) {
      var high = engine() | 0;
      if (high & 0x400000) {
        if ((high & 0x7fffff) === 0x400000 && (engine() | 0) === 0) {
          return 0x20000000000000;
        }
      } else {
        var low = engine() >>> 0;
        return ((high & 0x1fffff) * 0x100000000) + low + (high & 0x200000 ? -0x20000000000000 : 0);
      }
    }
  };
  proto.int53Full = function () {
    return Random.int53Full(this.engine);
  };

  function add(generate, addend) {
    if (addend === 0) {
      return generate;
    } else {
      return function (engine) {
        return generate(engine) + addend;
      };
    }
  }

  Random.integer = (function () {
    function isPowerOfTwoMinusOne(value) {
      return ((value + 1) & value) === 0;
    }

    function bitmask(masking) {
      return function (engine) {
        return engine() & masking;
      };
    }

    function downscaleToLoopCheckedRange(range) {
      var extendedRange = range + 1;
      var maximum = extendedRange * Math.floor(0x100000000 / extendedRange);
      return function (engine) {
        var value = 0;
        do {
          value = engine() >>> 0;
        } while (value >= maximum);
        return value % extendedRange;
      };
    }

    function downscaleToRange(range) {
      if (isPowerOfTwoMinusOne(range)) {
        return bitmask(range);
      } else {
        return downscaleToLoopCheckedRange(range);
      }
    }

    function isEvenlyDivisibleByMaxInt32(value) {
      return (value | 0) === 0;
    }

    function upscaleWithHighMasking(masking) {
      return function (engine) {
        var high = engine() & masking;
        var low = engine() >>> 0;
        return (high * 0x100000000) + low;
      };
    }

    function upscaleToLoopCheckedRange(extendedRange) {
      var maximum = extendedRange * Math.floor(0x20000000000000 / extendedRange);
      return function (engine) {
        var ret = 0;
        do {
          var high = engine() & 0x1fffff;
          var low = engine() >>> 0;
          ret = (high * 0x100000000) + low;
        } while (ret >= maximum);
        return ret % extendedRange;
      };
    }

    function upscaleWithinU53(range) {
      var extendedRange = range + 1;
      if (isEvenlyDivisibleByMaxInt32(extendedRange)) {
        var highRange = ((extendedRange / 0x100000000) | 0) - 1;
        if (isPowerOfTwoMinusOne(highRange)) {
          return upscaleWithHighMasking(highRange);
        }
      }
      return upscaleToLoopCheckedRange(extendedRange);
    }

    function upscaleWithinI53AndLoopCheck(min, max) {
      return function (engine) {
        var ret = 0;
        do {
          var high = engine() | 0;
          var low = engine() >>> 0;
          ret = ((high & 0x1fffff) * 0x100000000) + low + (high & 0x200000 ? -0x20000000000000 : 0);
        } while (ret < min || ret > max);
        return ret;
      };
    }

    return function (min, max) {
      min = Math.floor(min);
      max = Math.floor(max);
      if (min < -0x20000000000000 || !isFinite(min)) {
        throw new RangeError("Expected min to be at least " + (-0x20000000000000));
      } else if (max > 0x20000000000000 || !isFinite(max)) {
        throw new RangeError("Expected max to be at most " + 0x20000000000000);
      }

      var range = max - min;
      if (range <= 0 || !isFinite(range)) {
        return returnValue(min);
      } else if (range === 0xffffffff) {
        if (min === 0) {
          return Random.uint32;
        } else {
          return add(Random.int32, min + 0x80000000);
        }
      } else if (range < 0xffffffff) {
        return add(downscaleToRange(range), min);
      } else if (range === 0x1fffffffffffff) {
        return add(Random.uint53, min);
      } else if (range < 0x1fffffffffffff) {
        return add(upscaleWithinU53(range), min);
      } else if (max - 1 - min === 0x1fffffffffffff) {
        return add(Random.uint53Full, min);
      } else if (min === -0x20000000000000 && max === 0x20000000000000) {
        return Random.int53Full;
      } else if (min === -0x20000000000000 && max === 0x1fffffffffffff) {
        return Random.int53;
      } else if (min === -0x1fffffffffffff && max === 0x20000000000000) {
        return add(Random.int53, 1);
      } else if (max === 0x20000000000000) {
        return add(upscaleWithinI53AndLoopCheck(min - 1, max - 1), 1);
      } else {
        return upscaleWithinI53AndLoopCheck(min, max);
      }
    };
  }());
  proto.integer = function (min, max) {
    return Random.integer(min, max)(this.engine);
  };

  // [0, 1] (floating point)
  Random.realZeroToOneInclusive = function (engine) {
    return Random.uint53Full(engine) / 0x20000000000000;
  };
  proto.realZeroToOneInclusive = function () {
    return Random.realZeroToOneInclusive(this.engine);
  };

  // [0, 1) (floating point)
  Random.realZeroToOneExclusive = function (engine) {
    return Random.uint53(engine) / 0x20000000000000;
  };
  proto.realZeroToOneExclusive = function () {
    return Random.realZeroToOneExclusive(this.engine);
  };

  Random.real = (function () {
    function multiply(generate, multiplier) {
      if (multiplier === 1) {
        return generate;
      } else if (multiplier === 0) {
        return function () {
          return 0;
        };
      } else {
        return function (engine) {
          return generate(engine) * multiplier;
        };
      }
    }

    return function (left, right, inclusive) {
      if (!isFinite(left)) {
        throw new RangeError("Expected left to be a finite number");
      } else if (!isFinite(right)) {
        throw new RangeError("Expected right to be a finite number");
      }
      return add(
        multiply(
          inclusive ? Random.realZeroToOneInclusive : Random.realZeroToOneExclusive,
          right - left),
        left);
    };
  }());
  proto.real = function (min, max, inclusive) {
    return Random.real(min, max, inclusive)(this.engine);
  };

  Random.bool = (function () {
    function isLeastBitTrue(engine) {
      return (engine() & 1) === 1;
    }

    function lessThan(generate, value) {
      return function (engine) {
        return generate(engine) < value;
      };
    }

    function probability(percentage) {
      if (percentage <= 0) {
        return returnValue(false);
      } else if (percentage >= 1) {
        return returnValue(true);
      } else {
        var scaled = percentage * 0x100000000;
        if (scaled % 1 === 0) {
          return lessThan(Random.int32, (scaled - 0x80000000) | 0);
        } else {
          return lessThan(Random.uint53, Math.round(percentage * 0x20000000000000));
        }
      }
    }

    return function (numerator, denominator) {
      if (denominator == null) {
        if (numerator == null) {
          return isLeastBitTrue;
        }
        return probability(numerator);
      } else {
        if (numerator <= 0) {
          return returnValue(false);
        } else if (numerator >= denominator) {
          return returnValue(true);
        }
        return lessThan(Random.integer(0, denominator - 1), numerator);
      }
    };
  }());
  proto.bool = function (numerator, denominator) {
    return Random.bool(numerator, denominator)(this.engine);
  };

  function toInteger(value) {
    var number = +value;
    if (number < 0) {
      return Math.ceil(number);
    } else {
      return Math.floor(number);
    }
  }

  function convertSliceArgument(value, length) {
    if (value < 0) {
      return Math.max(value + length, 0);
    } else {
      return Math.min(value, length);
    }
  }
  Random.pick = function (engine, array, begin, end) {
    var length = array.length;
    var start = begin == null ? 0 : convertSliceArgument(toInteger(begin), length);
    var finish = end === void 0 ? length : convertSliceArgument(toInteger(end), length);
    if (start >= finish) {
      return void 0;
    }
    var distribution = Random.integer(start, finish - 1);
    return array[distribution(engine)];
  };
  proto.pick = function (array, begin, end) {
    return Random.pick(this.engine, array, begin, end);
  };

  function returnUndefined() {
    return void 0;
  }
  var slice = Array.prototype.slice;
  Random.picker = function (array, begin, end) {
    var clone = slice.call(array, begin, end);
    if (!clone.length) {
      return returnUndefined;
    }
    var distribution = Random.integer(0, clone.length - 1);
    return function (engine) {
      return clone[distribution(engine)];
    };
  };

  Random.shuffle = function (engine, array, downTo) {
    var length = array.length;
    if (length) {
      if (downTo == null) {
        downTo = 0;
      }
      for (var i = (length - 1) >>> 0; i > downTo; --i) {
        var distribution = Random.integer(0, i);
        var j = distribution(engine);
        if (i !== j) {
          var tmp = array[i];
          array[i] = array[j];
          array[j] = tmp;
        }
      }
    }
    return array;
  };
  proto.shuffle = function (array) {
    return Random.shuffle(this.engine, array);
  };

  Random.sample = function (engine, population, sampleSize) {
    if (sampleSize < 0 || sampleSize > population.length || !isFinite(sampleSize)) {
      throw new RangeError("Expected sampleSize to be within 0 and the length of the population");
    }

    if (sampleSize === 0) {
      return [];
    }

    var clone = slice.call(population);
    var length = clone.length;
    if (length === sampleSize) {
      return Random.shuffle(engine, clone, 0);
    }
    var tailLength = length - sampleSize;
    return Random.shuffle(engine, clone, tailLength - 1).slice(tailLength);
  };
  proto.sample = function (population, sampleSize) {
    return Random.sample(this.engine, population, sampleSize);
  };

  Random.die = function (sideCount) {
    return Random.integer(1, sideCount);
  };
  proto.die = function (sideCount) {
    return Random.die(sideCount)(this.engine);
  };

  Random.dice = function (sideCount, dieCount) {
    var distribution = Random.die(sideCount);
    return function (engine) {
      var result = [];
      result.length = dieCount;
      for (var i = 0; i < dieCount; ++i) {
        result[i] = distribution(engine);
      }
      return result;
    };
  };
  proto.dice = function (sideCount, dieCount) {
    return Random.dice(sideCount, dieCount)(this.engine);
  };

  // http://en.wikipedia.org/wiki/Universally_unique_identifier
  Random.uuid4 = (function () {
    function zeroPad(string, zeroCount) {
      return stringRepeat("0", zeroCount - string.length) + string;
    }

    return function (engine) {
      var a = engine() >>> 0;
      var b = engine() | 0;
      var c = engine() | 0;
      var d = engine() >>> 0;

      return (
        zeroPad(a.toString(16), 8) +
        "-" +
        zeroPad((b & 0xffff).toString(16), 4) +
        "-" +
        zeroPad((((b >> 4) & 0x0fff) | 0x4000).toString(16), 4) +
        "-" +
        zeroPad(((c & 0x3fff) | 0x8000).toString(16), 4) +
        "-" +
        zeroPad(((c >> 4) & 0xffff).toString(16), 4) +
        zeroPad(d.toString(16), 8));
    };
  }());
  proto.uuid4 = function () {
    return Random.uuid4(this.engine);
  };

  Random.string = (function () {
    // has 2**x chars, for faster uniform distribution
    var DEFAULT_STRING_POOL = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-";

    return function (pool) {
      if (pool == null) {
        pool = DEFAULT_STRING_POOL;
      }

      var length = pool.length;
      if (!length) {
        throw new Error("Expected pool not to be an empty string");
      }

      var distribution = Random.integer(0, length - 1);
      return function (engine, length) {
        var result = "";
        for (var i = 0; i < length; ++i) {
          var j = distribution(engine);
          result += pool.charAt(j);
        }
        return result;
      };
    };
  }());
  proto.string = function (length, pool) {
    return Random.string(pool)(this.engine, length);
  };

  Random.hex = (function () {
    var LOWER_HEX_POOL = "0123456789abcdef";
    var lowerHex = Random.string(LOWER_HEX_POOL);
    var upperHex = Random.string(LOWER_HEX_POOL.toUpperCase());

    return function (upper) {
      if (upper) {
        return upperHex;
      } else {
        return lowerHex;
      }
    };
  }());
  proto.hex = function (length, upper) {
    return Random.hex(upper)(this.engine, length);
  };

  Random.date = function (start, end) {
    if (!(start instanceof Date)) {
      throw new TypeError("Expected start to be a Date, got " + typeof start);
    } else if (!(end instanceof Date)) {
      throw new TypeError("Expected end to be a Date, got " + typeof end);
    }
    var distribution = Random.integer(start.getTime(), end.getTime());
    return function (engine) {
      return new Date(distribution(engine));
    };
  };
  proto.date = function (start, end) {
    return Random.date(start, end)(this.engine);
  };

  if (typeof define === "function" && define.amd) {
    define(function () {
      return Random;
    });
  } else if (typeof module !== "undefined" && typeof require === "function") {
    module.exports = Random;
  } else {
    (function () {
      var oldGlobal = root[GLOBAL_KEY];
      Random.noConflict = function () {
        root[GLOBAL_KEY] = oldGlobal;
        return this;
      };
    }());
    root[GLOBAL_KEY] = Random;
  }
}(this));
},{}],2:[function(require,module,exports){
(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
	typeof define === 'function' && define.amd ? define(['exports'], factory) :
	(factory((global.redom = global.redom || {})));
}(this, (function (exports) { 'use strict';

var HASH = '#'.charCodeAt(0);
var DOT = '.'.charCodeAt(0);

function createElement (query, ns) {
  var tag;
  var id;
  var className;

  var mode = 0;
  var start = 0;

  for (var i = 0; i <= query.length; i++) {
    var char = query.charCodeAt(i);

    if (char === HASH || char === DOT || !char) {
      if (mode === 0) {
        if (i === 0) {
          tag = 'div';
        } else if (!char) {
          tag = query;
        } else {
          tag = query.substring(start, i);
        }
      } else {
        var slice = query.substring(start, i);

        if (mode === 1) {
          id = slice;
        } else if (className) {
          className += ' ' + slice;
        } else {
          className = slice;
        }
      }

      start = i + 1;

      if (char === HASH) {
        mode = 1;
      } else {
        mode = 2;
      }
    }
  }

  var element = ns ? document.createElementNS(ns, tag) : document.createElement(tag);

  if (id) {
    element.id = id;
  }

  if (className) {
    if (ns) {
      element.setAttribute('class', className);
    } else {
      element.className = className;
    }
  }

  return element;
}

var hookNames = ['onmount', 'onunmount'];

function mount (parent, child, before) {
  var parentEl = getEl(parent);
  var childEl = getEl(child);

  if (child === childEl && childEl.__redom_view) {
    // try to look up the view if not provided
    child = childEl.__redom_view;
  }

  if (child !== childEl) {
    childEl.__redom_view = child;
  }

  var wasMounted = childEl.__redom_mounted;
  var oldParent = childEl.parentNode;

  if (wasMounted && (oldParent !== parentEl)) {
    doUnmount(child, childEl, oldParent);
  }

  if (before != null) {
    parentEl.insertBefore(childEl, getEl(before));
  } else {
    parentEl.appendChild(childEl);
  }

  doMount(child, childEl, parentEl, oldParent);

  return child;
}

function unmount (parent, child) {
  var parentEl = parent.el || parent;
  var childEl = child.el || child;

  if (child === childEl && childEl.__redom_view) {
    // try to look up the view if not provided
    child = childEl.__redom_view;
  }

  doUnmount(child, childEl, parentEl);

  parentEl.removeChild(childEl);

  return child;
}

function doMount (child, childEl, parentEl, oldParent) {
  var hooks = childEl.__redom_lifecycle || (childEl.__redom_lifecycle = {});
  var remount = (parentEl === oldParent);
  var hooksFound = false;

  for (var i = 0; i < hookNames.length; i++) {
    var hookName = hookNames[i];

    if (!remount && (child !== childEl) && (hookName in child)) {
      hooks[hookName] = (hooks[hookName] || 0) + 1;
    }
    if (hooks[hookName]) {
      hooksFound = true;
    }
  }

  if (!hooksFound) {
    childEl.__redom_mounted = true;
    return;
  }

  var traverse = parentEl;
  var triggered = false;

  if (remount || (!triggered && (traverse && traverse.__redom_mounted))) {
    trigger(childEl, remount ? 'onremount' : 'onmount');
    triggered = true;
  }

  if (remount) {
    return;
  }

  while (traverse) {
    var parent = traverse.parentNode;
    var parentHooks = traverse.__redom_lifecycle || (traverse.__redom_lifecycle = {});

    for (var hook in hooks) {
      parentHooks[hook] = (parentHooks[hook] || 0) + hooks[hook];
    }

    if (!triggered && (traverse === document || (parent && parent.__redom_mounted))) {
      trigger(traverse, remount ? 'onremount' : 'onmount');
      triggered = true;
    }

    traverse = parent;
  }
}

function doUnmount (child, childEl, parentEl) {
  var hooks = childEl.__redom_lifecycle;

  if (!hooks) {
    childEl.__redom_mounted = false;
    return;
  }

  var traverse = parentEl;

  if (childEl.__redom_mounted) {
    trigger(childEl, 'onunmount');
  }

  while (traverse) {
    var parentHooks = traverse.__redom_lifecycle || (traverse.__redom_lifecycle = {});
    var hooksFound = false;

    for (var hook in hooks) {
      if (parentHooks[hook]) {
        parentHooks[hook] -= hooks[hook];
      }
      if (parentHooks[hook]) {
        hooksFound = true;
      }
    }

    if (!hooksFound) {
      traverse.__redom_lifecycle = null;
    }

    traverse = traverse.parentNode;
  }
}

function trigger (el, eventName) {
  if (eventName === 'onmount') {
    el.__redom_mounted = true;
  } else if (eventName === 'onunmount') {
    el.__redom_mounted = false;
  }

  var hooks = el.__redom_lifecycle;

  if (!hooks) {
    return;
  }

  var view = el.__redom_view;
  var hookCount = 0;

  view && view[eventName] && view[eventName]();

  for (var hook in hooks) {
    if (hook) {
      hookCount++;
    }
  }

  if (hookCount) {
    var traverse = el.firstChild;

    while (traverse) {
      var next = traverse.nextSibling;

      trigger(traverse, eventName);

      traverse = next;
    }
  }
}

function setStyle (view, arg1, arg2) {
  var el = getEl(view);

  if (arg2 !== undefined) {
    el.style[arg1] = arg2;
  } else if (isString(arg1)) {
    el.setAttribute('style', arg1);
  } else {
    for (var key in arg1) {
      setStyle(el, key, arg1[key]);
    }
  }
}

function setAttr (view, arg1, arg2) {
  var el = getEl(view);
  var isSVG = el instanceof window.SVGElement;

  if (arg2 !== undefined) {
    if (arg1 === 'style') {
      setStyle(el, arg2);
    } else if (isSVG && isFunction(arg2)) {
      el[arg1] = arg2;
    } else if (!isSVG && (arg1 in el || isFunction(arg2))) {
      el[arg1] = arg2;
    } else {
      el.setAttribute(arg1, arg2);
    }
  } else {
    for (var key in arg1) {
      setAttr(el, key, arg1[key]);
    }
  }
}

var text = function (str) { return document.createTextNode(str); };

function parseArguments (element, args) {
  for (var i = 0; i < args.length; i++) {
    var arg = args[i];

    if (arg !== 0 && !arg) {
      continue;
    }

    // support middleware
    if (typeof arg === 'function') {
      arg(element);
    } else if (isString(arg) || isNumber(arg)) {
      element.appendChild(text(arg));
    } else if (isNode(getEl(arg))) {
      mount(element, arg);
    } else if (arg.length) {
      parseArguments(element, arg);
    } else if (typeof arg === 'object') {
      setAttr(element, arg);
    }
  }
}

var ensureEl = function (parent) { return isString(parent) ? html(parent) : getEl(parent); };
var getEl = function (parent) { return (parent.nodeType && parent) || (!parent.el && parent) || getEl(parent.el); };

var isString = function (a) { return typeof a === 'string'; };
var isNumber = function (a) { return typeof a === 'number'; };
var isFunction = function (a) { return typeof a === 'function'; };

var isNode = function (a) { return a && a.nodeType; };

var htmlCache = {};

var memoizeHTML = function (query) { return htmlCache[query] || (htmlCache[query] = createElement(query)); };

function html (query) {
  var args = [], len = arguments.length - 1;
  while ( len-- > 0 ) args[ len ] = arguments[ len + 1 ];

  var element;

  if (isString(query)) {
    element = memoizeHTML(query).cloneNode(false);
  } else if (isNode(query)) {
    element = query.cloneNode(false);
  } else {
    throw new Error('At least one argument required');
  }

  parseArguments(element, args);

  return element;
}

html.extend = function (query) {
  var clone = memoizeHTML(query);

  return html.bind(this, clone);
};

var el = html;

function setChildren (parent, children) {
  if (children.length === undefined) {
    return setChildren(parent, [children]);
  }

  var parentEl = getEl(parent);
  var traverse = parentEl.firstChild;

  for (var i = 0; i < children.length; i++) {
    var child = children[i];

    if (!child) {
      continue;
    }

    var childEl = getEl(child);

    if (childEl === traverse) {
      traverse = traverse.nextSibling;
      continue;
    }

    mount(parent, child, traverse);
  }

  while (traverse) {
    var next = traverse.nextSibling;

    unmount(parent, traverse);

    traverse = next;
  }
}

var propKey = function (key) { return function (item) { return item[key]; }; };

function list (parent, View, key, initData) {
  return new List(parent, View, key, initData);
}

function List (parent, View, key, initData) {
  this.__redom_list = true;
  this.View = View;
  this.initData = initData;
  this.views = [];
  this.el = ensureEl(parent);

  if (key != null) {
    this.lookup = {};
    this.key = isFunction(key) ? key : propKey(key);
  }
}

List.extend = function (parent, View, key, initData) {
  return List.bind(List, parent, View, key, initData);
};

list.extend = List.extend;

List.prototype.update = function (data) {
  var this$1 = this;
  if ( data === void 0 ) data = [];

  var View = this.View;
  var key = this.key;
  var keySet = key != null;
  var initData = this.initData;
  var newViews = new Array(data.length);
  var oldViews = this.views;
  var newLookup = key && {};
  var oldLookup = key && this.lookup;

  for (var i = 0; i < data.length; i++) {
    var item = data[i];
    var view = (void 0);

    if (keySet) {
      var id = key(item);
      view = newViews[i] = oldLookup[id] || new View(initData, item, i, data);
      newLookup[id] = view;
      view.__id = id;
    } else {
      view = newViews[i] = oldViews[i] || new View(initData, item, i, data);
    }
    var el = view.el;
    if (el.__redom_list) {
      el = el.el;
    }
    el.__redom_view = view;
    view.update && view.update(item, i, data);
  }

  if (keySet) {
    for (var i$1 = 0; i$1 < oldViews.length; i$1++) {
      var id$1 = oldViews[i$1].__id;

      if (!(id$1 in newLookup)) {
        unmount(this$1, oldLookup[id$1]);
      }
    }
  }

  setChildren(this, newViews);

  if (keySet) {
    this.lookup = newLookup;
  }
  this.views = newViews;
};

function router (parent, Views, initData) {
  return new Router(parent, Views, initData);
}

var Router = function Router (parent, Views, initData) {
  this.el = ensureEl(parent);
  this.Views = Views;
  this.initData = initData;
};
Router.prototype.update = function update (route, data) {
  if (route !== this.route) {
    var Views = this.Views;
    var View = Views[route];

    this.view = View && new View(this.initData, data);
    this.route = route;

    setChildren(this.el, [ this.view ]);
  }
  this.view && this.view.update && this.view.update(data, route);
};

var SVG = 'http://www.w3.org/2000/svg';

var svgCache = {};

var memoizeSVG = function (query) { return svgCache[query] || (svgCache[query] = createElement(query, SVG)); };

function svg (query) {
  var args = [], len = arguments.length - 1;
  while ( len-- > 0 ) args[ len ] = arguments[ len + 1 ];

  var element;

  if (isString(query)) {
    element = memoizeSVG(query).cloneNode(false);
  } else if (isNode(query)) {
    element = query.cloneNode(false);
  } else {
    throw new Error('At least one argument required');
  }

  parseArguments(element, args);

  return element;
}

svg.extend = function (query) {
  var clone = memoizeSVG(query);

  return svg.bind(this, clone);
};

exports.html = html;
exports.el = el;
exports.list = list;
exports.List = List;
exports.mount = mount;
exports.unmount = unmount;
exports.router = router;
exports.Router = Router;
exports.setAttr = setAttr;
exports.setStyle = setStyle;
exports.setChildren = setChildren;
exports.svg = svg;
exports.text = text;

Object.defineProperty(exports, '__esModule', { value: true });

})));

},{}],3:[function(require,module,exports){
const { el, mount, list, unmount}  = require('redom')
const Random = require('random-js')

let random = new Random(Random.engines.browserCrypto)

class Item {
    constructor(){
        this.remove = this.remove.bind(this)
        this.el = el('.item',
            this.itemName = el('.item-name'),
            this.itemDelete = el('.item-delete', 'x')
        )

        this.itemDelete.onclick = this.remove
    }

    remove(){
        const { id } = this.data;
        const event = new CustomEvent('delete_item', { detail: id, bubbles: true })
        this.el.dispatchEvent(event)
    }

    update(data){
        this.itemName.textContent = this.name = data.name
        this.data = data
    }
}

class ItemsList{
    constructor(){
        this.el = el('.item-container')
        this.list = list(this.el, Item, 'id')
        this.id = 0
        this.items = []

        this.el.addEventListener('delete_item', e => {
            this.items = this.items.filter(el=>el.id !== e.detail)
            this.list.update(this.items)
        })
    }

    update(data){
        if(data.action === 'add_item' && data.name){
            this.items.push({
                id : this.id++,
                name : data.name
            })
            this.list.update(this.items)
        }
    }
}

class ItemInput {
    constructor(addItem){
        this.pop = this.pop.bind(this)

        this.el = el('form.item-input',
            this.newItemName = el('input', {type:'text', autofocus: true}),
            this.addItemBtn = el('input', {type:'submit', value:'Add Item'}),
            el('label', 'Add one or many items separated by commas')
        )

        this.addItemBtn.onclick = addItem
        this.el.onsubmit = addItem
    }

    pop(){
        let value = this.newItemName.value
        this.newItemName.value = ""
        this.newItemName.focus()
        return value
    }
}

class ItemPicker {
    constructor(){  
        this.addItem = this.addItem.bind(this)

        this.el = el('div.global-container',
            el('div.header',
                this.newItemForm = new ItemInput(this.addItem)
            ),
            this.items = new ItemsList(),
            el('label', this.removePicked = el('input', {type:"checkbox", checked: false}), 'Remove picked'),
            this.pickRandom = el('button', 'Pick Random'),
            this.pickedItem = el('.picked-item')
        )

        this.pickRandom.onclick = (e)=>{
            let pickedItem = random.pick(this.items.list.views)
            let name = (pickedItem || {}).name
            if(pickedItem && this.removePicked.checked){
                pickedItem.remove()
            }
            this.pickedItem.textContent = name
        }
    }

    addItem(e){
        e.preventDefault()
        this
        .newItemForm
        .pop()
        .split(',')
        .forEach(item=>{
            this.items.update({
                action : 'add_item',
                name : item.trim()
            })
        })
        
    }
}

document.addEventListener('DOMContentLoaded', main)
function main(){
    mount(document.body, new ItemPicker())
}
},{"random-js":1,"redom":2}]},{},[3])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy5udm0vdmVyc2lvbnMvbm9kZS92Ny43LjEvbGliL25vZGVfbW9kdWxlcy93YXRjaGlmeS9ub2RlX21vZHVsZXMvYnJvd3Nlci1wYWNrL19wcmVsdWRlLmpzIiwibm9kZV9tb2R1bGVzL3JhbmRvbS1qcy9saWIvcmFuZG9tLmpzIiwibm9kZV9tb2R1bGVzL3JlZG9tL2Rpc3QvcmVkb20uanMiLCJzcmMvaW5kZXguanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzNzQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDdmdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCIvKmpzaGludCBlcW51bGw6dHJ1ZSovXG4oZnVuY3Rpb24gKHJvb3QpIHtcbiAgXCJ1c2Ugc3RyaWN0XCI7XG5cbiAgdmFyIEdMT0JBTF9LRVkgPSBcIlJhbmRvbVwiO1xuXG4gIHZhciBpbXVsID0gKHR5cGVvZiBNYXRoLmltdWwgIT09IFwiZnVuY3Rpb25cIiB8fCBNYXRoLmltdWwoMHhmZmZmZmZmZiwgNSkgIT09IC01ID9cbiAgICBmdW5jdGlvbiAoYSwgYikge1xuICAgICAgdmFyIGFoID0gKGEgPj4+IDE2KSAmIDB4ZmZmZjtcbiAgICAgIHZhciBhbCA9IGEgJiAweGZmZmY7XG4gICAgICB2YXIgYmggPSAoYiA+Pj4gMTYpICYgMHhmZmZmO1xuICAgICAgdmFyIGJsID0gYiAmIDB4ZmZmZjtcbiAgICAgIC8vIHRoZSBzaGlmdCBieSAwIGZpeGVzIHRoZSBzaWduIG9uIHRoZSBoaWdoIHBhcnRcbiAgICAgIC8vIHRoZSBmaW5hbCB8MCBjb252ZXJ0cyB0aGUgdW5zaWduZWQgdmFsdWUgaW50byBhIHNpZ25lZCB2YWx1ZVxuICAgICAgcmV0dXJuIChhbCAqIGJsKSArICgoKGFoICogYmwgKyBhbCAqIGJoKSA8PCAxNikgPj4+IDApIHwgMDtcbiAgICB9IDpcbiAgICBNYXRoLmltdWwpO1xuXG4gIHZhciBzdHJpbmdSZXBlYXQgPSAodHlwZW9mIFN0cmluZy5wcm90b3R5cGUucmVwZWF0ID09PSBcImZ1bmN0aW9uXCIgJiYgXCJ4XCIucmVwZWF0KDMpID09PSBcInh4eFwiID9cbiAgICBmdW5jdGlvbiAoeCwgeSkge1xuICAgICAgcmV0dXJuIHgucmVwZWF0KHkpO1xuICAgIH0gOiBmdW5jdGlvbiAocGF0dGVybiwgY291bnQpIHtcbiAgICAgIHZhciByZXN1bHQgPSBcIlwiO1xuICAgICAgd2hpbGUgKGNvdW50ID4gMCkge1xuICAgICAgICBpZiAoY291bnQgJiAxKSB7XG4gICAgICAgICAgcmVzdWx0ICs9IHBhdHRlcm47XG4gICAgICAgIH1cbiAgICAgICAgY291bnQgPj49IDE7XG4gICAgICAgIHBhdHRlcm4gKz0gcGF0dGVybjtcbiAgICAgIH1cbiAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgfSk7XG5cbiAgZnVuY3Rpb24gUmFuZG9tKGVuZ2luZSkge1xuICAgIGlmICghKHRoaXMgaW5zdGFuY2VvZiBSYW5kb20pKSB7XG4gICAgICByZXR1cm4gbmV3IFJhbmRvbShlbmdpbmUpO1xuICAgIH1cblxuICAgIGlmIChlbmdpbmUgPT0gbnVsbCkge1xuICAgICAgZW5naW5lID0gUmFuZG9tLmVuZ2luZXMubmF0aXZlTWF0aDtcbiAgICB9IGVsc2UgaWYgKHR5cGVvZiBlbmdpbmUgIT09IFwiZnVuY3Rpb25cIikge1xuICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkV4cGVjdGVkIGVuZ2luZSB0byBiZSBhIGZ1bmN0aW9uLCBnb3QgXCIgKyB0eXBlb2YgZW5naW5lKTtcbiAgICB9XG4gICAgdGhpcy5lbmdpbmUgPSBlbmdpbmU7XG4gIH1cbiAgdmFyIHByb3RvID0gUmFuZG9tLnByb3RvdHlwZTtcblxuICBSYW5kb20uZW5naW5lcyA9IHtcbiAgICBuYXRpdmVNYXRoOiBmdW5jdGlvbiAoKSB7XG4gICAgICByZXR1cm4gKE1hdGgucmFuZG9tKCkgKiAweDEwMDAwMDAwMCkgfCAwO1xuICAgIH0sXG4gICAgbXQxOTkzNzogKGZ1bmN0aW9uIChJbnQzMkFycmF5KSB7XG4gICAgICAvLyBodHRwOi8vZW4ud2lraXBlZGlhLm9yZy93aWtpL01lcnNlbm5lX3R3aXN0ZXJcbiAgICAgIGZ1bmN0aW9uIHJlZnJlc2hEYXRhKGRhdGEpIHtcbiAgICAgICAgdmFyIGsgPSAwO1xuICAgICAgICB2YXIgdG1wID0gMDtcbiAgICAgICAgZm9yICg7XG4gICAgICAgICAgKGsgfCAwKSA8IDIyNzsgayA9IChrICsgMSkgfCAwKSB7XG4gICAgICAgICAgdG1wID0gKGRhdGFba10gJiAweDgwMDAwMDAwKSB8IChkYXRhWyhrICsgMSkgfCAwXSAmIDB4N2ZmZmZmZmYpO1xuICAgICAgICAgIGRhdGFba10gPSBkYXRhWyhrICsgMzk3KSB8IDBdIF4gKHRtcCA+Pj4gMSkgXiAoKHRtcCAmIDB4MSkgPyAweDk5MDhiMGRmIDogMCk7XG4gICAgICAgIH1cblxuICAgICAgICBmb3IgKDtcbiAgICAgICAgICAoayB8IDApIDwgNjIzOyBrID0gKGsgKyAxKSB8IDApIHtcbiAgICAgICAgICB0bXAgPSAoZGF0YVtrXSAmIDB4ODAwMDAwMDApIHwgKGRhdGFbKGsgKyAxKSB8IDBdICYgMHg3ZmZmZmZmZik7XG4gICAgICAgICAgZGF0YVtrXSA9IGRhdGFbKGsgLSAyMjcpIHwgMF0gXiAodG1wID4+PiAxKSBeICgodG1wICYgMHgxKSA/IDB4OTkwOGIwZGYgOiAwKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRtcCA9IChkYXRhWzYyM10gJiAweDgwMDAwMDAwKSB8IChkYXRhWzBdICYgMHg3ZmZmZmZmZik7XG4gICAgICAgIGRhdGFbNjIzXSA9IGRhdGFbMzk2XSBeICh0bXAgPj4+IDEpIF4gKCh0bXAgJiAweDEpID8gMHg5OTA4YjBkZiA6IDApO1xuICAgICAgfVxuXG4gICAgICBmdW5jdGlvbiB0ZW1wZXIodmFsdWUpIHtcbiAgICAgICAgdmFsdWUgXj0gdmFsdWUgPj4+IDExO1xuICAgICAgICB2YWx1ZSBePSAodmFsdWUgPDwgNykgJiAweDlkMmM1NjgwO1xuICAgICAgICB2YWx1ZSBePSAodmFsdWUgPDwgMTUpICYgMHhlZmM2MDAwMDtcbiAgICAgICAgcmV0dXJuIHZhbHVlIF4gKHZhbHVlID4+PiAxOCk7XG4gICAgICB9XG5cbiAgICAgIGZ1bmN0aW9uIHNlZWRXaXRoQXJyYXkoZGF0YSwgc291cmNlKSB7XG4gICAgICAgIHZhciBpID0gMTtcbiAgICAgICAgdmFyIGogPSAwO1xuICAgICAgICB2YXIgc291cmNlTGVuZ3RoID0gc291cmNlLmxlbmd0aDtcbiAgICAgICAgdmFyIGsgPSBNYXRoLm1heChzb3VyY2VMZW5ndGgsIDYyNCkgfCAwO1xuICAgICAgICB2YXIgcHJldmlvdXMgPSBkYXRhWzBdIHwgMDtcbiAgICAgICAgZm9yICg7XG4gICAgICAgICAgKGsgfCAwKSA+IDA7IC0taykge1xuICAgICAgICAgIGRhdGFbaV0gPSBwcmV2aW91cyA9ICgoZGF0YVtpXSBeIGltdWwoKHByZXZpb3VzIF4gKHByZXZpb3VzID4+PiAzMCkpLCAweDAwMTk2NjBkKSkgKyAoc291cmNlW2pdIHwgMCkgKyAoaiB8IDApKSB8IDA7XG4gICAgICAgICAgaSA9IChpICsgMSkgfCAwO1xuICAgICAgICAgICsrajtcbiAgICAgICAgICBpZiAoKGkgfCAwKSA+IDYyMykge1xuICAgICAgICAgICAgZGF0YVswXSA9IGRhdGFbNjIzXTtcbiAgICAgICAgICAgIGkgPSAxO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZiAoaiA+PSBzb3VyY2VMZW5ndGgpIHtcbiAgICAgICAgICAgIGogPSAwO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBmb3IgKGsgPSA2MjM7XG4gICAgICAgICAgKGsgfCAwKSA+IDA7IC0taykge1xuICAgICAgICAgIGRhdGFbaV0gPSBwcmV2aW91cyA9ICgoZGF0YVtpXSBeIGltdWwoKHByZXZpb3VzIF4gKHByZXZpb3VzID4+PiAzMCkpLCAweDVkNTg4YjY1KSkgLSBpKSB8IDA7XG4gICAgICAgICAgaSA9IChpICsgMSkgfCAwO1xuICAgICAgICAgIGlmICgoaSB8IDApID4gNjIzKSB7XG4gICAgICAgICAgICBkYXRhWzBdID0gZGF0YVs2MjNdO1xuICAgICAgICAgICAgaSA9IDE7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGRhdGFbMF0gPSAweDgwMDAwMDAwO1xuICAgICAgfVxuXG4gICAgICBmdW5jdGlvbiBtdDE5OTM3KCkge1xuICAgICAgICB2YXIgZGF0YSA9IG5ldyBJbnQzMkFycmF5KDYyNCk7XG4gICAgICAgIHZhciBpbmRleCA9IDA7XG4gICAgICAgIHZhciB1c2VzID0gMDtcblxuICAgICAgICBmdW5jdGlvbiBuZXh0KCkge1xuICAgICAgICAgIGlmICgoaW5kZXggfCAwKSA+PSA2MjQpIHtcbiAgICAgICAgICAgIHJlZnJlc2hEYXRhKGRhdGEpO1xuICAgICAgICAgICAgaW5kZXggPSAwO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIHZhciB2YWx1ZSA9IGRhdGFbaW5kZXhdO1xuICAgICAgICAgIGluZGV4ID0gKGluZGV4ICsgMSkgfCAwO1xuICAgICAgICAgIHVzZXMgKz0gMTtcbiAgICAgICAgICByZXR1cm4gdGVtcGVyKHZhbHVlKSB8IDA7XG4gICAgICAgIH1cbiAgICAgICAgbmV4dC5nZXRVc2VDb3VudCA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIHJldHVybiB1c2VzO1xuICAgICAgICB9O1xuICAgICAgICBuZXh0LmRpc2NhcmQgPSBmdW5jdGlvbiAoY291bnQpIHtcbiAgICAgICAgICB1c2VzICs9IGNvdW50O1xuICAgICAgICAgIGlmICgoaW5kZXggfCAwKSA+PSA2MjQpIHtcbiAgICAgICAgICAgIHJlZnJlc2hEYXRhKGRhdGEpO1xuICAgICAgICAgICAgaW5kZXggPSAwO1xuICAgICAgICAgIH1cbiAgICAgICAgICB3aGlsZSAoKGNvdW50IC0gaW5kZXgpID4gNjI0KSB7XG4gICAgICAgICAgICBjb3VudCAtPSA2MjQgLSBpbmRleDtcbiAgICAgICAgICAgIHJlZnJlc2hEYXRhKGRhdGEpO1xuICAgICAgICAgICAgaW5kZXggPSAwO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpbmRleCA9IChpbmRleCArIGNvdW50KSB8IDA7XG4gICAgICAgICAgcmV0dXJuIG5leHQ7XG4gICAgICAgIH07XG4gICAgICAgIG5leHQuc2VlZCA9IGZ1bmN0aW9uIChpbml0aWFsKSB7XG4gICAgICAgICAgdmFyIHByZXZpb3VzID0gMDtcbiAgICAgICAgICBkYXRhWzBdID0gcHJldmlvdXMgPSBpbml0aWFsIHwgMDtcblxuICAgICAgICAgIGZvciAodmFyIGkgPSAxOyBpIDwgNjI0OyBpID0gKGkgKyAxKSB8IDApIHtcbiAgICAgICAgICAgIGRhdGFbaV0gPSBwcmV2aW91cyA9IChpbXVsKChwcmV2aW91cyBeIChwcmV2aW91cyA+Pj4gMzApKSwgMHg2YzA3ODk2NSkgKyBpKSB8IDA7XG4gICAgICAgICAgfVxuICAgICAgICAgIGluZGV4ID0gNjI0O1xuICAgICAgICAgIHVzZXMgPSAwO1xuICAgICAgICAgIHJldHVybiBuZXh0O1xuICAgICAgICB9O1xuICAgICAgICBuZXh0LnNlZWRXaXRoQXJyYXkgPSBmdW5jdGlvbiAoc291cmNlKSB7XG4gICAgICAgICAgbmV4dC5zZWVkKDB4MDEyYmQ2YWEpO1xuICAgICAgICAgIHNlZWRXaXRoQXJyYXkoZGF0YSwgc291cmNlKTtcbiAgICAgICAgICByZXR1cm4gbmV4dDtcbiAgICAgICAgfTtcbiAgICAgICAgbmV4dC5hdXRvU2VlZCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICByZXR1cm4gbmV4dC5zZWVkV2l0aEFycmF5KFJhbmRvbS5nZW5lcmF0ZUVudHJvcHlBcnJheSgpKTtcbiAgICAgICAgfTtcbiAgICAgICAgcmV0dXJuIG5leHQ7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBtdDE5OTM3O1xuICAgIH0odHlwZW9mIEludDMyQXJyYXkgPT09IFwiZnVuY3Rpb25cIiA/IEludDMyQXJyYXkgOiBBcnJheSkpLFxuICAgIGJyb3dzZXJDcnlwdG86ICh0eXBlb2YgY3J5cHRvICE9PSBcInVuZGVmaW5lZFwiICYmIHR5cGVvZiBjcnlwdG8uZ2V0UmFuZG9tVmFsdWVzID09PSBcImZ1bmN0aW9uXCIgJiYgdHlwZW9mIEludDMyQXJyYXkgPT09IFwiZnVuY3Rpb25cIikgPyAoZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIGRhdGEgPSBudWxsO1xuICAgICAgdmFyIGluZGV4ID0gMTI4O1xuXG4gICAgICByZXR1cm4gZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAoaW5kZXggPj0gMTI4KSB7XG4gICAgICAgICAgaWYgKGRhdGEgPT09IG51bGwpIHtcbiAgICAgICAgICAgIGRhdGEgPSBuZXcgSW50MzJBcnJheSgxMjgpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBjcnlwdG8uZ2V0UmFuZG9tVmFsdWVzKGRhdGEpO1xuICAgICAgICAgIGluZGV4ID0gMDtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBkYXRhW2luZGV4KytdIHwgMDtcbiAgICAgIH07XG4gICAgfSgpKSA6IG51bGxcbiAgfTtcblxuICBSYW5kb20uZ2VuZXJhdGVFbnRyb3B5QXJyYXkgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIGFycmF5ID0gW107XG4gICAgdmFyIGVuZ2luZSA9IFJhbmRvbS5lbmdpbmVzLm5hdGl2ZU1hdGg7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCAxNjsgKytpKSB7XG4gICAgICBhcnJheVtpXSA9IGVuZ2luZSgpIHwgMDtcbiAgICB9XG4gICAgYXJyYXkucHVzaChuZXcgRGF0ZSgpLmdldFRpbWUoKSB8IDApO1xuICAgIHJldHVybiBhcnJheTtcbiAgfTtcblxuICBmdW5jdGlvbiByZXR1cm5WYWx1ZSh2YWx1ZSkge1xuICAgIHJldHVybiBmdW5jdGlvbiAoKSB7XG4gICAgICByZXR1cm4gdmFsdWU7XG4gICAgfTtcbiAgfVxuXG4gIC8vIFstMHg4MDAwMDAwMCwgMHg3ZmZmZmZmZl1cbiAgUmFuZG9tLmludDMyID0gZnVuY3Rpb24gKGVuZ2luZSkge1xuICAgIHJldHVybiBlbmdpbmUoKSB8IDA7XG4gIH07XG4gIHByb3RvLmludDMyID0gZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiBSYW5kb20uaW50MzIodGhpcy5lbmdpbmUpO1xuICB9O1xuXG4gIC8vIFswLCAweGZmZmZmZmZmXVxuICBSYW5kb20udWludDMyID0gZnVuY3Rpb24gKGVuZ2luZSkge1xuICAgIHJldHVybiBlbmdpbmUoKSA+Pj4gMDtcbiAgfTtcbiAgcHJvdG8udWludDMyID0gZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiBSYW5kb20udWludDMyKHRoaXMuZW5naW5lKTtcbiAgfTtcblxuICAvLyBbMCwgMHgxZmZmZmZmZmZmZmZmZl1cbiAgUmFuZG9tLnVpbnQ1MyA9IGZ1bmN0aW9uIChlbmdpbmUpIHtcbiAgICB2YXIgaGlnaCA9IGVuZ2luZSgpICYgMHgxZmZmZmY7XG4gICAgdmFyIGxvdyA9IGVuZ2luZSgpID4+PiAwO1xuICAgIHJldHVybiAoaGlnaCAqIDB4MTAwMDAwMDAwKSArIGxvdztcbiAgfTtcbiAgcHJvdG8udWludDUzID0gZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiBSYW5kb20udWludDUzKHRoaXMuZW5naW5lKTtcbiAgfTtcblxuICAvLyBbMCwgMHgyMDAwMDAwMDAwMDAwMF1cbiAgUmFuZG9tLnVpbnQ1M0Z1bGwgPSBmdW5jdGlvbiAoZW5naW5lKSB7XG4gICAgd2hpbGUgKHRydWUpIHtcbiAgICAgIHZhciBoaWdoID0gZW5naW5lKCkgfCAwO1xuICAgICAgaWYgKGhpZ2ggJiAweDIwMDAwMCkge1xuICAgICAgICBpZiAoKGhpZ2ggJiAweDNmZmZmZikgPT09IDB4MjAwMDAwICYmIChlbmdpbmUoKSB8IDApID09PSAwKSB7XG4gICAgICAgICAgcmV0dXJuIDB4MjAwMDAwMDAwMDAwMDA7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHZhciBsb3cgPSBlbmdpbmUoKSA+Pj4gMDtcbiAgICAgICAgcmV0dXJuICgoaGlnaCAmIDB4MWZmZmZmKSAqIDB4MTAwMDAwMDAwKSArIGxvdztcbiAgICAgIH1cbiAgICB9XG4gIH07XG4gIHByb3RvLnVpbnQ1M0Z1bGwgPSBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIFJhbmRvbS51aW50NTNGdWxsKHRoaXMuZW5naW5lKTtcbiAgfTtcblxuICAvLyBbLTB4MjAwMDAwMDAwMDAwMDAsIDB4MWZmZmZmZmZmZmZmZmZdXG4gIFJhbmRvbS5pbnQ1MyA9IGZ1bmN0aW9uIChlbmdpbmUpIHtcbiAgICB2YXIgaGlnaCA9IGVuZ2luZSgpIHwgMDtcbiAgICB2YXIgbG93ID0gZW5naW5lKCkgPj4+IDA7XG4gICAgcmV0dXJuICgoaGlnaCAmIDB4MWZmZmZmKSAqIDB4MTAwMDAwMDAwKSArIGxvdyArIChoaWdoICYgMHgyMDAwMDAgPyAtMHgyMDAwMDAwMDAwMDAwMCA6IDApO1xuICB9O1xuICBwcm90by5pbnQ1MyA9IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gUmFuZG9tLmludDUzKHRoaXMuZW5naW5lKTtcbiAgfTtcblxuICAvLyBbLTB4MjAwMDAwMDAwMDAwMDAsIDB4MjAwMDAwMDAwMDAwMDBdXG4gIFJhbmRvbS5pbnQ1M0Z1bGwgPSBmdW5jdGlvbiAoZW5naW5lKSB7XG4gICAgd2hpbGUgKHRydWUpIHtcbiAgICAgIHZhciBoaWdoID0gZW5naW5lKCkgfCAwO1xuICAgICAgaWYgKGhpZ2ggJiAweDQwMDAwMCkge1xuICAgICAgICBpZiAoKGhpZ2ggJiAweDdmZmZmZikgPT09IDB4NDAwMDAwICYmIChlbmdpbmUoKSB8IDApID09PSAwKSB7XG4gICAgICAgICAgcmV0dXJuIDB4MjAwMDAwMDAwMDAwMDA7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHZhciBsb3cgPSBlbmdpbmUoKSA+Pj4gMDtcbiAgICAgICAgcmV0dXJuICgoaGlnaCAmIDB4MWZmZmZmKSAqIDB4MTAwMDAwMDAwKSArIGxvdyArIChoaWdoICYgMHgyMDAwMDAgPyAtMHgyMDAwMDAwMDAwMDAwMCA6IDApO1xuICAgICAgfVxuICAgIH1cbiAgfTtcbiAgcHJvdG8uaW50NTNGdWxsID0gZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiBSYW5kb20uaW50NTNGdWxsKHRoaXMuZW5naW5lKTtcbiAgfTtcblxuICBmdW5jdGlvbiBhZGQoZ2VuZXJhdGUsIGFkZGVuZCkge1xuICAgIGlmIChhZGRlbmQgPT09IDApIHtcbiAgICAgIHJldHVybiBnZW5lcmF0ZTtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIGZ1bmN0aW9uIChlbmdpbmUpIHtcbiAgICAgICAgcmV0dXJuIGdlbmVyYXRlKGVuZ2luZSkgKyBhZGRlbmQ7XG4gICAgICB9O1xuICAgIH1cbiAgfVxuXG4gIFJhbmRvbS5pbnRlZ2VyID0gKGZ1bmN0aW9uICgpIHtcbiAgICBmdW5jdGlvbiBpc1Bvd2VyT2ZUd29NaW51c09uZSh2YWx1ZSkge1xuICAgICAgcmV0dXJuICgodmFsdWUgKyAxKSAmIHZhbHVlKSA9PT0gMDtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBiaXRtYXNrKG1hc2tpbmcpIHtcbiAgICAgIHJldHVybiBmdW5jdGlvbiAoZW5naW5lKSB7XG4gICAgICAgIHJldHVybiBlbmdpbmUoKSAmIG1hc2tpbmc7XG4gICAgICB9O1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGRvd25zY2FsZVRvTG9vcENoZWNrZWRSYW5nZShyYW5nZSkge1xuICAgICAgdmFyIGV4dGVuZGVkUmFuZ2UgPSByYW5nZSArIDE7XG4gICAgICB2YXIgbWF4aW11bSA9IGV4dGVuZGVkUmFuZ2UgKiBNYXRoLmZsb29yKDB4MTAwMDAwMDAwIC8gZXh0ZW5kZWRSYW5nZSk7XG4gICAgICByZXR1cm4gZnVuY3Rpb24gKGVuZ2luZSkge1xuICAgICAgICB2YXIgdmFsdWUgPSAwO1xuICAgICAgICBkbyB7XG4gICAgICAgICAgdmFsdWUgPSBlbmdpbmUoKSA+Pj4gMDtcbiAgICAgICAgfSB3aGlsZSAodmFsdWUgPj0gbWF4aW11bSk7XG4gICAgICAgIHJldHVybiB2YWx1ZSAlIGV4dGVuZGVkUmFuZ2U7XG4gICAgICB9O1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGRvd25zY2FsZVRvUmFuZ2UocmFuZ2UpIHtcbiAgICAgIGlmIChpc1Bvd2VyT2ZUd29NaW51c09uZShyYW5nZSkpIHtcbiAgICAgICAgcmV0dXJuIGJpdG1hc2socmFuZ2UpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIGRvd25zY2FsZVRvTG9vcENoZWNrZWRSYW5nZShyYW5nZSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gaXNFdmVubHlEaXZpc2libGVCeU1heEludDMyKHZhbHVlKSB7XG4gICAgICByZXR1cm4gKHZhbHVlIHwgMCkgPT09IDA7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gdXBzY2FsZVdpdGhIaWdoTWFza2luZyhtYXNraW5nKSB7XG4gICAgICByZXR1cm4gZnVuY3Rpb24gKGVuZ2luZSkge1xuICAgICAgICB2YXIgaGlnaCA9IGVuZ2luZSgpICYgbWFza2luZztcbiAgICAgICAgdmFyIGxvdyA9IGVuZ2luZSgpID4+PiAwO1xuICAgICAgICByZXR1cm4gKGhpZ2ggKiAweDEwMDAwMDAwMCkgKyBsb3c7XG4gICAgICB9O1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHVwc2NhbGVUb0xvb3BDaGVja2VkUmFuZ2UoZXh0ZW5kZWRSYW5nZSkge1xuICAgICAgdmFyIG1heGltdW0gPSBleHRlbmRlZFJhbmdlICogTWF0aC5mbG9vcigweDIwMDAwMDAwMDAwMDAwIC8gZXh0ZW5kZWRSYW5nZSk7XG4gICAgICByZXR1cm4gZnVuY3Rpb24gKGVuZ2luZSkge1xuICAgICAgICB2YXIgcmV0ID0gMDtcbiAgICAgICAgZG8ge1xuICAgICAgICAgIHZhciBoaWdoID0gZW5naW5lKCkgJiAweDFmZmZmZjtcbiAgICAgICAgICB2YXIgbG93ID0gZW5naW5lKCkgPj4+IDA7XG4gICAgICAgICAgcmV0ID0gKGhpZ2ggKiAweDEwMDAwMDAwMCkgKyBsb3c7XG4gICAgICAgIH0gd2hpbGUgKHJldCA+PSBtYXhpbXVtKTtcbiAgICAgICAgcmV0dXJuIHJldCAlIGV4dGVuZGVkUmFuZ2U7XG4gICAgICB9O1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHVwc2NhbGVXaXRoaW5VNTMocmFuZ2UpIHtcbiAgICAgIHZhciBleHRlbmRlZFJhbmdlID0gcmFuZ2UgKyAxO1xuICAgICAgaWYgKGlzRXZlbmx5RGl2aXNpYmxlQnlNYXhJbnQzMihleHRlbmRlZFJhbmdlKSkge1xuICAgICAgICB2YXIgaGlnaFJhbmdlID0gKChleHRlbmRlZFJhbmdlIC8gMHgxMDAwMDAwMDApIHwgMCkgLSAxO1xuICAgICAgICBpZiAoaXNQb3dlck9mVHdvTWludXNPbmUoaGlnaFJhbmdlKSkge1xuICAgICAgICAgIHJldHVybiB1cHNjYWxlV2l0aEhpZ2hNYXNraW5nKGhpZ2hSYW5nZSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHJldHVybiB1cHNjYWxlVG9Mb29wQ2hlY2tlZFJhbmdlKGV4dGVuZGVkUmFuZ2UpO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHVwc2NhbGVXaXRoaW5JNTNBbmRMb29wQ2hlY2sobWluLCBtYXgpIHtcbiAgICAgIHJldHVybiBmdW5jdGlvbiAoZW5naW5lKSB7XG4gICAgICAgIHZhciByZXQgPSAwO1xuICAgICAgICBkbyB7XG4gICAgICAgICAgdmFyIGhpZ2ggPSBlbmdpbmUoKSB8IDA7XG4gICAgICAgICAgdmFyIGxvdyA9IGVuZ2luZSgpID4+PiAwO1xuICAgICAgICAgIHJldCA9ICgoaGlnaCAmIDB4MWZmZmZmKSAqIDB4MTAwMDAwMDAwKSArIGxvdyArIChoaWdoICYgMHgyMDAwMDAgPyAtMHgyMDAwMDAwMDAwMDAwMCA6IDApO1xuICAgICAgICB9IHdoaWxlIChyZXQgPCBtaW4gfHwgcmV0ID4gbWF4KTtcbiAgICAgICAgcmV0dXJuIHJldDtcbiAgICAgIH07XG4gICAgfVxuXG4gICAgcmV0dXJuIGZ1bmN0aW9uIChtaW4sIG1heCkge1xuICAgICAgbWluID0gTWF0aC5mbG9vcihtaW4pO1xuICAgICAgbWF4ID0gTWF0aC5mbG9vcihtYXgpO1xuICAgICAgaWYgKG1pbiA8IC0weDIwMDAwMDAwMDAwMDAwIHx8ICFpc0Zpbml0ZShtaW4pKSB7XG4gICAgICAgIHRocm93IG5ldyBSYW5nZUVycm9yKFwiRXhwZWN0ZWQgbWluIHRvIGJlIGF0IGxlYXN0IFwiICsgKC0weDIwMDAwMDAwMDAwMDAwKSk7XG4gICAgICB9IGVsc2UgaWYgKG1heCA+IDB4MjAwMDAwMDAwMDAwMDAgfHwgIWlzRmluaXRlKG1heCkpIHtcbiAgICAgICAgdGhyb3cgbmV3IFJhbmdlRXJyb3IoXCJFeHBlY3RlZCBtYXggdG8gYmUgYXQgbW9zdCBcIiArIDB4MjAwMDAwMDAwMDAwMDApO1xuICAgICAgfVxuXG4gICAgICB2YXIgcmFuZ2UgPSBtYXggLSBtaW47XG4gICAgICBpZiAocmFuZ2UgPD0gMCB8fCAhaXNGaW5pdGUocmFuZ2UpKSB7XG4gICAgICAgIHJldHVybiByZXR1cm5WYWx1ZShtaW4pO1xuICAgICAgfSBlbHNlIGlmIChyYW5nZSA9PT0gMHhmZmZmZmZmZikge1xuICAgICAgICBpZiAobWluID09PSAwKSB7XG4gICAgICAgICAgcmV0dXJuIFJhbmRvbS51aW50MzI7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgcmV0dXJuIGFkZChSYW5kb20uaW50MzIsIG1pbiArIDB4ODAwMDAwMDApO1xuICAgICAgICB9XG4gICAgICB9IGVsc2UgaWYgKHJhbmdlIDwgMHhmZmZmZmZmZikge1xuICAgICAgICByZXR1cm4gYWRkKGRvd25zY2FsZVRvUmFuZ2UocmFuZ2UpLCBtaW4pO1xuICAgICAgfSBlbHNlIGlmIChyYW5nZSA9PT0gMHgxZmZmZmZmZmZmZmZmZikge1xuICAgICAgICByZXR1cm4gYWRkKFJhbmRvbS51aW50NTMsIG1pbik7XG4gICAgICB9IGVsc2UgaWYgKHJhbmdlIDwgMHgxZmZmZmZmZmZmZmZmZikge1xuICAgICAgICByZXR1cm4gYWRkKHVwc2NhbGVXaXRoaW5VNTMocmFuZ2UpLCBtaW4pO1xuICAgICAgfSBlbHNlIGlmIChtYXggLSAxIC0gbWluID09PSAweDFmZmZmZmZmZmZmZmZmKSB7XG4gICAgICAgIHJldHVybiBhZGQoUmFuZG9tLnVpbnQ1M0Z1bGwsIG1pbik7XG4gICAgICB9IGVsc2UgaWYgKG1pbiA9PT0gLTB4MjAwMDAwMDAwMDAwMDAgJiYgbWF4ID09PSAweDIwMDAwMDAwMDAwMDAwKSB7XG4gICAgICAgIHJldHVybiBSYW5kb20uaW50NTNGdWxsO1xuICAgICAgfSBlbHNlIGlmIChtaW4gPT09IC0weDIwMDAwMDAwMDAwMDAwICYmIG1heCA9PT0gMHgxZmZmZmZmZmZmZmZmZikge1xuICAgICAgICByZXR1cm4gUmFuZG9tLmludDUzO1xuICAgICAgfSBlbHNlIGlmIChtaW4gPT09IC0weDFmZmZmZmZmZmZmZmZmICYmIG1heCA9PT0gMHgyMDAwMDAwMDAwMDAwMCkge1xuICAgICAgICByZXR1cm4gYWRkKFJhbmRvbS5pbnQ1MywgMSk7XG4gICAgICB9IGVsc2UgaWYgKG1heCA9PT0gMHgyMDAwMDAwMDAwMDAwMCkge1xuICAgICAgICByZXR1cm4gYWRkKHVwc2NhbGVXaXRoaW5JNTNBbmRMb29wQ2hlY2sobWluIC0gMSwgbWF4IC0gMSksIDEpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIHVwc2NhbGVXaXRoaW5JNTNBbmRMb29wQ2hlY2sobWluLCBtYXgpO1xuICAgICAgfVxuICAgIH07XG4gIH0oKSk7XG4gIHByb3RvLmludGVnZXIgPSBmdW5jdGlvbiAobWluLCBtYXgpIHtcbiAgICByZXR1cm4gUmFuZG9tLmludGVnZXIobWluLCBtYXgpKHRoaXMuZW5naW5lKTtcbiAgfTtcblxuICAvLyBbMCwgMV0gKGZsb2F0aW5nIHBvaW50KVxuICBSYW5kb20ucmVhbFplcm9Ub09uZUluY2x1c2l2ZSA9IGZ1bmN0aW9uIChlbmdpbmUpIHtcbiAgICByZXR1cm4gUmFuZG9tLnVpbnQ1M0Z1bGwoZW5naW5lKSAvIDB4MjAwMDAwMDAwMDAwMDA7XG4gIH07XG4gIHByb3RvLnJlYWxaZXJvVG9PbmVJbmNsdXNpdmUgPSBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIFJhbmRvbS5yZWFsWmVyb1RvT25lSW5jbHVzaXZlKHRoaXMuZW5naW5lKTtcbiAgfTtcblxuICAvLyBbMCwgMSkgKGZsb2F0aW5nIHBvaW50KVxuICBSYW5kb20ucmVhbFplcm9Ub09uZUV4Y2x1c2l2ZSA9IGZ1bmN0aW9uIChlbmdpbmUpIHtcbiAgICByZXR1cm4gUmFuZG9tLnVpbnQ1MyhlbmdpbmUpIC8gMHgyMDAwMDAwMDAwMDAwMDtcbiAgfTtcbiAgcHJvdG8ucmVhbFplcm9Ub09uZUV4Y2x1c2l2ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gUmFuZG9tLnJlYWxaZXJvVG9PbmVFeGNsdXNpdmUodGhpcy5lbmdpbmUpO1xuICB9O1xuXG4gIFJhbmRvbS5yZWFsID0gKGZ1bmN0aW9uICgpIHtcbiAgICBmdW5jdGlvbiBtdWx0aXBseShnZW5lcmF0ZSwgbXVsdGlwbGllcikge1xuICAgICAgaWYgKG11bHRpcGxpZXIgPT09IDEpIHtcbiAgICAgICAgcmV0dXJuIGdlbmVyYXRlO1xuICAgICAgfSBlbHNlIGlmIChtdWx0aXBsaWVyID09PSAwKSB7XG4gICAgICAgIHJldHVybiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgcmV0dXJuIDA7XG4gICAgICAgIH07XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gZnVuY3Rpb24gKGVuZ2luZSkge1xuICAgICAgICAgIHJldHVybiBnZW5lcmF0ZShlbmdpbmUpICogbXVsdGlwbGllcjtcbiAgICAgICAgfTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gZnVuY3Rpb24gKGxlZnQsIHJpZ2h0LCBpbmNsdXNpdmUpIHtcbiAgICAgIGlmICghaXNGaW5pdGUobGVmdCkpIHtcbiAgICAgICAgdGhyb3cgbmV3IFJhbmdlRXJyb3IoXCJFeHBlY3RlZCBsZWZ0IHRvIGJlIGEgZmluaXRlIG51bWJlclwiKTtcbiAgICAgIH0gZWxzZSBpZiAoIWlzRmluaXRlKHJpZ2h0KSkge1xuICAgICAgICB0aHJvdyBuZXcgUmFuZ2VFcnJvcihcIkV4cGVjdGVkIHJpZ2h0IHRvIGJlIGEgZmluaXRlIG51bWJlclwiKTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBhZGQoXG4gICAgICAgIG11bHRpcGx5KFxuICAgICAgICAgIGluY2x1c2l2ZSA/IFJhbmRvbS5yZWFsWmVyb1RvT25lSW5jbHVzaXZlIDogUmFuZG9tLnJlYWxaZXJvVG9PbmVFeGNsdXNpdmUsXG4gICAgICAgICAgcmlnaHQgLSBsZWZ0KSxcbiAgICAgICAgbGVmdCk7XG4gICAgfTtcbiAgfSgpKTtcbiAgcHJvdG8ucmVhbCA9IGZ1bmN0aW9uIChtaW4sIG1heCwgaW5jbHVzaXZlKSB7XG4gICAgcmV0dXJuIFJhbmRvbS5yZWFsKG1pbiwgbWF4LCBpbmNsdXNpdmUpKHRoaXMuZW5naW5lKTtcbiAgfTtcblxuICBSYW5kb20uYm9vbCA9IChmdW5jdGlvbiAoKSB7XG4gICAgZnVuY3Rpb24gaXNMZWFzdEJpdFRydWUoZW5naW5lKSB7XG4gICAgICByZXR1cm4gKGVuZ2luZSgpICYgMSkgPT09IDE7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gbGVzc1RoYW4oZ2VuZXJhdGUsIHZhbHVlKSB7XG4gICAgICByZXR1cm4gZnVuY3Rpb24gKGVuZ2luZSkge1xuICAgICAgICByZXR1cm4gZ2VuZXJhdGUoZW5naW5lKSA8IHZhbHVlO1xuICAgICAgfTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBwcm9iYWJpbGl0eShwZXJjZW50YWdlKSB7XG4gICAgICBpZiAocGVyY2VudGFnZSA8PSAwKSB7XG4gICAgICAgIHJldHVybiByZXR1cm5WYWx1ZShmYWxzZSk7XG4gICAgICB9IGVsc2UgaWYgKHBlcmNlbnRhZ2UgPj0gMSkge1xuICAgICAgICByZXR1cm4gcmV0dXJuVmFsdWUodHJ1ZSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB2YXIgc2NhbGVkID0gcGVyY2VudGFnZSAqIDB4MTAwMDAwMDAwO1xuICAgICAgICBpZiAoc2NhbGVkICUgMSA9PT0gMCkge1xuICAgICAgICAgIHJldHVybiBsZXNzVGhhbihSYW5kb20uaW50MzIsIChzY2FsZWQgLSAweDgwMDAwMDAwKSB8IDApO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHJldHVybiBsZXNzVGhhbihSYW5kb20udWludDUzLCBNYXRoLnJvdW5kKHBlcmNlbnRhZ2UgKiAweDIwMDAwMDAwMDAwMDAwKSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gZnVuY3Rpb24gKG51bWVyYXRvciwgZGVub21pbmF0b3IpIHtcbiAgICAgIGlmIChkZW5vbWluYXRvciA9PSBudWxsKSB7XG4gICAgICAgIGlmIChudW1lcmF0b3IgPT0gbnVsbCkge1xuICAgICAgICAgIHJldHVybiBpc0xlYXN0Qml0VHJ1ZTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gcHJvYmFiaWxpdHkobnVtZXJhdG9yKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGlmIChudW1lcmF0b3IgPD0gMCkge1xuICAgICAgICAgIHJldHVybiByZXR1cm5WYWx1ZShmYWxzZSk7XG4gICAgICAgIH0gZWxzZSBpZiAobnVtZXJhdG9yID49IGRlbm9taW5hdG9yKSB7XG4gICAgICAgICAgcmV0dXJuIHJldHVyblZhbHVlKHRydWUpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBsZXNzVGhhbihSYW5kb20uaW50ZWdlcigwLCBkZW5vbWluYXRvciAtIDEpLCBudW1lcmF0b3IpO1xuICAgICAgfVxuICAgIH07XG4gIH0oKSk7XG4gIHByb3RvLmJvb2wgPSBmdW5jdGlvbiAobnVtZXJhdG9yLCBkZW5vbWluYXRvcikge1xuICAgIHJldHVybiBSYW5kb20uYm9vbChudW1lcmF0b3IsIGRlbm9taW5hdG9yKSh0aGlzLmVuZ2luZSk7XG4gIH07XG5cbiAgZnVuY3Rpb24gdG9JbnRlZ2VyKHZhbHVlKSB7XG4gICAgdmFyIG51bWJlciA9ICt2YWx1ZTtcbiAgICBpZiAobnVtYmVyIDwgMCkge1xuICAgICAgcmV0dXJuIE1hdGguY2VpbChudW1iZXIpO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gTWF0aC5mbG9vcihudW1iZXIpO1xuICAgIH1cbiAgfVxuXG4gIGZ1bmN0aW9uIGNvbnZlcnRTbGljZUFyZ3VtZW50KHZhbHVlLCBsZW5ndGgpIHtcbiAgICBpZiAodmFsdWUgPCAwKSB7XG4gICAgICByZXR1cm4gTWF0aC5tYXgodmFsdWUgKyBsZW5ndGgsIDApO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gTWF0aC5taW4odmFsdWUsIGxlbmd0aCk7XG4gICAgfVxuICB9XG4gIFJhbmRvbS5waWNrID0gZnVuY3Rpb24gKGVuZ2luZSwgYXJyYXksIGJlZ2luLCBlbmQpIHtcbiAgICB2YXIgbGVuZ3RoID0gYXJyYXkubGVuZ3RoO1xuICAgIHZhciBzdGFydCA9IGJlZ2luID09IG51bGwgPyAwIDogY29udmVydFNsaWNlQXJndW1lbnQodG9JbnRlZ2VyKGJlZ2luKSwgbGVuZ3RoKTtcbiAgICB2YXIgZmluaXNoID0gZW5kID09PSB2b2lkIDAgPyBsZW5ndGggOiBjb252ZXJ0U2xpY2VBcmd1bWVudCh0b0ludGVnZXIoZW5kKSwgbGVuZ3RoKTtcbiAgICBpZiAoc3RhcnQgPj0gZmluaXNoKSB7XG4gICAgICByZXR1cm4gdm9pZCAwO1xuICAgIH1cbiAgICB2YXIgZGlzdHJpYnV0aW9uID0gUmFuZG9tLmludGVnZXIoc3RhcnQsIGZpbmlzaCAtIDEpO1xuICAgIHJldHVybiBhcnJheVtkaXN0cmlidXRpb24oZW5naW5lKV07XG4gIH07XG4gIHByb3RvLnBpY2sgPSBmdW5jdGlvbiAoYXJyYXksIGJlZ2luLCBlbmQpIHtcbiAgICByZXR1cm4gUmFuZG9tLnBpY2sodGhpcy5lbmdpbmUsIGFycmF5LCBiZWdpbiwgZW5kKTtcbiAgfTtcblxuICBmdW5jdGlvbiByZXR1cm5VbmRlZmluZWQoKSB7XG4gICAgcmV0dXJuIHZvaWQgMDtcbiAgfVxuICB2YXIgc2xpY2UgPSBBcnJheS5wcm90b3R5cGUuc2xpY2U7XG4gIFJhbmRvbS5waWNrZXIgPSBmdW5jdGlvbiAoYXJyYXksIGJlZ2luLCBlbmQpIHtcbiAgICB2YXIgY2xvbmUgPSBzbGljZS5jYWxsKGFycmF5LCBiZWdpbiwgZW5kKTtcbiAgICBpZiAoIWNsb25lLmxlbmd0aCkge1xuICAgICAgcmV0dXJuIHJldHVyblVuZGVmaW5lZDtcbiAgICB9XG4gICAgdmFyIGRpc3RyaWJ1dGlvbiA9IFJhbmRvbS5pbnRlZ2VyKDAsIGNsb25lLmxlbmd0aCAtIDEpO1xuICAgIHJldHVybiBmdW5jdGlvbiAoZW5naW5lKSB7XG4gICAgICByZXR1cm4gY2xvbmVbZGlzdHJpYnV0aW9uKGVuZ2luZSldO1xuICAgIH07XG4gIH07XG5cbiAgUmFuZG9tLnNodWZmbGUgPSBmdW5jdGlvbiAoZW5naW5lLCBhcnJheSwgZG93blRvKSB7XG4gICAgdmFyIGxlbmd0aCA9IGFycmF5Lmxlbmd0aDtcbiAgICBpZiAobGVuZ3RoKSB7XG4gICAgICBpZiAoZG93blRvID09IG51bGwpIHtcbiAgICAgICAgZG93blRvID0gMDtcbiAgICAgIH1cbiAgICAgIGZvciAodmFyIGkgPSAobGVuZ3RoIC0gMSkgPj4+IDA7IGkgPiBkb3duVG87IC0taSkge1xuICAgICAgICB2YXIgZGlzdHJpYnV0aW9uID0gUmFuZG9tLmludGVnZXIoMCwgaSk7XG4gICAgICAgIHZhciBqID0gZGlzdHJpYnV0aW9uKGVuZ2luZSk7XG4gICAgICAgIGlmIChpICE9PSBqKSB7XG4gICAgICAgICAgdmFyIHRtcCA9IGFycmF5W2ldO1xuICAgICAgICAgIGFycmF5W2ldID0gYXJyYXlbal07XG4gICAgICAgICAgYXJyYXlbal0gPSB0bXA7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIGFycmF5O1xuICB9O1xuICBwcm90by5zaHVmZmxlID0gZnVuY3Rpb24gKGFycmF5KSB7XG4gICAgcmV0dXJuIFJhbmRvbS5zaHVmZmxlKHRoaXMuZW5naW5lLCBhcnJheSk7XG4gIH07XG5cbiAgUmFuZG9tLnNhbXBsZSA9IGZ1bmN0aW9uIChlbmdpbmUsIHBvcHVsYXRpb24sIHNhbXBsZVNpemUpIHtcbiAgICBpZiAoc2FtcGxlU2l6ZSA8IDAgfHwgc2FtcGxlU2l6ZSA+IHBvcHVsYXRpb24ubGVuZ3RoIHx8ICFpc0Zpbml0ZShzYW1wbGVTaXplKSkge1xuICAgICAgdGhyb3cgbmV3IFJhbmdlRXJyb3IoXCJFeHBlY3RlZCBzYW1wbGVTaXplIHRvIGJlIHdpdGhpbiAwIGFuZCB0aGUgbGVuZ3RoIG9mIHRoZSBwb3B1bGF0aW9uXCIpO1xuICAgIH1cblxuICAgIGlmIChzYW1wbGVTaXplID09PSAwKSB7XG4gICAgICByZXR1cm4gW107XG4gICAgfVxuXG4gICAgdmFyIGNsb25lID0gc2xpY2UuY2FsbChwb3B1bGF0aW9uKTtcbiAgICB2YXIgbGVuZ3RoID0gY2xvbmUubGVuZ3RoO1xuICAgIGlmIChsZW5ndGggPT09IHNhbXBsZVNpemUpIHtcbiAgICAgIHJldHVybiBSYW5kb20uc2h1ZmZsZShlbmdpbmUsIGNsb25lLCAwKTtcbiAgICB9XG4gICAgdmFyIHRhaWxMZW5ndGggPSBsZW5ndGggLSBzYW1wbGVTaXplO1xuICAgIHJldHVybiBSYW5kb20uc2h1ZmZsZShlbmdpbmUsIGNsb25lLCB0YWlsTGVuZ3RoIC0gMSkuc2xpY2UodGFpbExlbmd0aCk7XG4gIH07XG4gIHByb3RvLnNhbXBsZSA9IGZ1bmN0aW9uIChwb3B1bGF0aW9uLCBzYW1wbGVTaXplKSB7XG4gICAgcmV0dXJuIFJhbmRvbS5zYW1wbGUodGhpcy5lbmdpbmUsIHBvcHVsYXRpb24sIHNhbXBsZVNpemUpO1xuICB9O1xuXG4gIFJhbmRvbS5kaWUgPSBmdW5jdGlvbiAoc2lkZUNvdW50KSB7XG4gICAgcmV0dXJuIFJhbmRvbS5pbnRlZ2VyKDEsIHNpZGVDb3VudCk7XG4gIH07XG4gIHByb3RvLmRpZSA9IGZ1bmN0aW9uIChzaWRlQ291bnQpIHtcbiAgICByZXR1cm4gUmFuZG9tLmRpZShzaWRlQ291bnQpKHRoaXMuZW5naW5lKTtcbiAgfTtcblxuICBSYW5kb20uZGljZSA9IGZ1bmN0aW9uIChzaWRlQ291bnQsIGRpZUNvdW50KSB7XG4gICAgdmFyIGRpc3RyaWJ1dGlvbiA9IFJhbmRvbS5kaWUoc2lkZUNvdW50KTtcbiAgICByZXR1cm4gZnVuY3Rpb24gKGVuZ2luZSkge1xuICAgICAgdmFyIHJlc3VsdCA9IFtdO1xuICAgICAgcmVzdWx0Lmxlbmd0aCA9IGRpZUNvdW50O1xuICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBkaWVDb3VudDsgKytpKSB7XG4gICAgICAgIHJlc3VsdFtpXSA9IGRpc3RyaWJ1dGlvbihlbmdpbmUpO1xuICAgICAgfVxuICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICB9O1xuICB9O1xuICBwcm90by5kaWNlID0gZnVuY3Rpb24gKHNpZGVDb3VudCwgZGllQ291bnQpIHtcbiAgICByZXR1cm4gUmFuZG9tLmRpY2Uoc2lkZUNvdW50LCBkaWVDb3VudCkodGhpcy5lbmdpbmUpO1xuICB9O1xuXG4gIC8vIGh0dHA6Ly9lbi53aWtpcGVkaWEub3JnL3dpa2kvVW5pdmVyc2FsbHlfdW5pcXVlX2lkZW50aWZpZXJcbiAgUmFuZG9tLnV1aWQ0ID0gKGZ1bmN0aW9uICgpIHtcbiAgICBmdW5jdGlvbiB6ZXJvUGFkKHN0cmluZywgemVyb0NvdW50KSB7XG4gICAgICByZXR1cm4gc3RyaW5nUmVwZWF0KFwiMFwiLCB6ZXJvQ291bnQgLSBzdHJpbmcubGVuZ3RoKSArIHN0cmluZztcbiAgICB9XG5cbiAgICByZXR1cm4gZnVuY3Rpb24gKGVuZ2luZSkge1xuICAgICAgdmFyIGEgPSBlbmdpbmUoKSA+Pj4gMDtcbiAgICAgIHZhciBiID0gZW5naW5lKCkgfCAwO1xuICAgICAgdmFyIGMgPSBlbmdpbmUoKSB8IDA7XG4gICAgICB2YXIgZCA9IGVuZ2luZSgpID4+PiAwO1xuXG4gICAgICByZXR1cm4gKFxuICAgICAgICB6ZXJvUGFkKGEudG9TdHJpbmcoMTYpLCA4KSArXG4gICAgICAgIFwiLVwiICtcbiAgICAgICAgemVyb1BhZCgoYiAmIDB4ZmZmZikudG9TdHJpbmcoMTYpLCA0KSArXG4gICAgICAgIFwiLVwiICtcbiAgICAgICAgemVyb1BhZCgoKChiID4+IDQpICYgMHgwZmZmKSB8IDB4NDAwMCkudG9TdHJpbmcoMTYpLCA0KSArXG4gICAgICAgIFwiLVwiICtcbiAgICAgICAgemVyb1BhZCgoKGMgJiAweDNmZmYpIHwgMHg4MDAwKS50b1N0cmluZygxNiksIDQpICtcbiAgICAgICAgXCItXCIgK1xuICAgICAgICB6ZXJvUGFkKCgoYyA+PiA0KSAmIDB4ZmZmZikudG9TdHJpbmcoMTYpLCA0KSArXG4gICAgICAgIHplcm9QYWQoZC50b1N0cmluZygxNiksIDgpKTtcbiAgICB9O1xuICB9KCkpO1xuICBwcm90by51dWlkNCA9IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gUmFuZG9tLnV1aWQ0KHRoaXMuZW5naW5lKTtcbiAgfTtcblxuICBSYW5kb20uc3RyaW5nID0gKGZ1bmN0aW9uICgpIHtcbiAgICAvLyBoYXMgMioqeCBjaGFycywgZm9yIGZhc3RlciB1bmlmb3JtIGRpc3RyaWJ1dGlvblxuICAgIHZhciBERUZBVUxUX1NUUklOR19QT09MID0gXCJhYmNkZWZnaGlqa2xtbm9wcXJzdHV2d3h5ekFCQ0RFRkdISUpLTE1OT1BRUlNUVVZXWFlaMDEyMzQ1Njc4OV8tXCI7XG5cbiAgICByZXR1cm4gZnVuY3Rpb24gKHBvb2wpIHtcbiAgICAgIGlmIChwb29sID09IG51bGwpIHtcbiAgICAgICAgcG9vbCA9IERFRkFVTFRfU1RSSU5HX1BPT0w7XG4gICAgICB9XG5cbiAgICAgIHZhciBsZW5ndGggPSBwb29sLmxlbmd0aDtcbiAgICAgIGlmICghbGVuZ3RoKSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcihcIkV4cGVjdGVkIHBvb2wgbm90IHRvIGJlIGFuIGVtcHR5IHN0cmluZ1wiKTtcbiAgICAgIH1cblxuICAgICAgdmFyIGRpc3RyaWJ1dGlvbiA9IFJhbmRvbS5pbnRlZ2VyKDAsIGxlbmd0aCAtIDEpO1xuICAgICAgcmV0dXJuIGZ1bmN0aW9uIChlbmdpbmUsIGxlbmd0aCkge1xuICAgICAgICB2YXIgcmVzdWx0ID0gXCJcIjtcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW5ndGg7ICsraSkge1xuICAgICAgICAgIHZhciBqID0gZGlzdHJpYnV0aW9uKGVuZ2luZSk7XG4gICAgICAgICAgcmVzdWx0ICs9IHBvb2wuY2hhckF0KGopO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgICB9O1xuICAgIH07XG4gIH0oKSk7XG4gIHByb3RvLnN0cmluZyA9IGZ1bmN0aW9uIChsZW5ndGgsIHBvb2wpIHtcbiAgICByZXR1cm4gUmFuZG9tLnN0cmluZyhwb29sKSh0aGlzLmVuZ2luZSwgbGVuZ3RoKTtcbiAgfTtcblxuICBSYW5kb20uaGV4ID0gKGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgTE9XRVJfSEVYX1BPT0wgPSBcIjAxMjM0NTY3ODlhYmNkZWZcIjtcbiAgICB2YXIgbG93ZXJIZXggPSBSYW5kb20uc3RyaW5nKExPV0VSX0hFWF9QT09MKTtcbiAgICB2YXIgdXBwZXJIZXggPSBSYW5kb20uc3RyaW5nKExPV0VSX0hFWF9QT09MLnRvVXBwZXJDYXNlKCkpO1xuXG4gICAgcmV0dXJuIGZ1bmN0aW9uICh1cHBlcikge1xuICAgICAgaWYgKHVwcGVyKSB7XG4gICAgICAgIHJldHVybiB1cHBlckhleDtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiBsb3dlckhleDtcbiAgICAgIH1cbiAgICB9O1xuICB9KCkpO1xuICBwcm90by5oZXggPSBmdW5jdGlvbiAobGVuZ3RoLCB1cHBlcikge1xuICAgIHJldHVybiBSYW5kb20uaGV4KHVwcGVyKSh0aGlzLmVuZ2luZSwgbGVuZ3RoKTtcbiAgfTtcblxuICBSYW5kb20uZGF0ZSA9IGZ1bmN0aW9uIChzdGFydCwgZW5kKSB7XG4gICAgaWYgKCEoc3RhcnQgaW5zdGFuY2VvZiBEYXRlKSkge1xuICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkV4cGVjdGVkIHN0YXJ0IHRvIGJlIGEgRGF0ZSwgZ290IFwiICsgdHlwZW9mIHN0YXJ0KTtcbiAgICB9IGVsc2UgaWYgKCEoZW5kIGluc3RhbmNlb2YgRGF0ZSkpIHtcbiAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoXCJFeHBlY3RlZCBlbmQgdG8gYmUgYSBEYXRlLCBnb3QgXCIgKyB0eXBlb2YgZW5kKTtcbiAgICB9XG4gICAgdmFyIGRpc3RyaWJ1dGlvbiA9IFJhbmRvbS5pbnRlZ2VyKHN0YXJ0LmdldFRpbWUoKSwgZW5kLmdldFRpbWUoKSk7XG4gICAgcmV0dXJuIGZ1bmN0aW9uIChlbmdpbmUpIHtcbiAgICAgIHJldHVybiBuZXcgRGF0ZShkaXN0cmlidXRpb24oZW5naW5lKSk7XG4gICAgfTtcbiAgfTtcbiAgcHJvdG8uZGF0ZSA9IGZ1bmN0aW9uIChzdGFydCwgZW5kKSB7XG4gICAgcmV0dXJuIFJhbmRvbS5kYXRlKHN0YXJ0LCBlbmQpKHRoaXMuZW5naW5lKTtcbiAgfTtcblxuICBpZiAodHlwZW9mIGRlZmluZSA9PT0gXCJmdW5jdGlvblwiICYmIGRlZmluZS5hbWQpIHtcbiAgICBkZWZpbmUoZnVuY3Rpb24gKCkge1xuICAgICAgcmV0dXJuIFJhbmRvbTtcbiAgICB9KTtcbiAgfSBlbHNlIGlmICh0eXBlb2YgbW9kdWxlICE9PSBcInVuZGVmaW5lZFwiICYmIHR5cGVvZiByZXF1aXJlID09PSBcImZ1bmN0aW9uXCIpIHtcbiAgICBtb2R1bGUuZXhwb3J0cyA9IFJhbmRvbTtcbiAgfSBlbHNlIHtcbiAgICAoZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIG9sZEdsb2JhbCA9IHJvb3RbR0xPQkFMX0tFWV07XG4gICAgICBSYW5kb20ubm9Db25mbGljdCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcm9vdFtHTE9CQUxfS0VZXSA9IG9sZEdsb2JhbDtcbiAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgICB9O1xuICAgIH0oKSk7XG4gICAgcm9vdFtHTE9CQUxfS0VZXSA9IFJhbmRvbTtcbiAgfVxufSh0aGlzKSk7IiwiKGZ1bmN0aW9uIChnbG9iYWwsIGZhY3RvcnkpIHtcblx0dHlwZW9mIGV4cG9ydHMgPT09ICdvYmplY3QnICYmIHR5cGVvZiBtb2R1bGUgIT09ICd1bmRlZmluZWQnID8gZmFjdG9yeShleHBvcnRzKSA6XG5cdHR5cGVvZiBkZWZpbmUgPT09ICdmdW5jdGlvbicgJiYgZGVmaW5lLmFtZCA/IGRlZmluZShbJ2V4cG9ydHMnXSwgZmFjdG9yeSkgOlxuXHQoZmFjdG9yeSgoZ2xvYmFsLnJlZG9tID0gZ2xvYmFsLnJlZG9tIHx8IHt9KSkpO1xufSh0aGlzLCAoZnVuY3Rpb24gKGV4cG9ydHMpIHsgJ3VzZSBzdHJpY3QnO1xuXG52YXIgSEFTSCA9ICcjJy5jaGFyQ29kZUF0KDApO1xudmFyIERPVCA9ICcuJy5jaGFyQ29kZUF0KDApO1xuXG5mdW5jdGlvbiBjcmVhdGVFbGVtZW50IChxdWVyeSwgbnMpIHtcbiAgdmFyIHRhZztcbiAgdmFyIGlkO1xuICB2YXIgY2xhc3NOYW1lO1xuXG4gIHZhciBtb2RlID0gMDtcbiAgdmFyIHN0YXJ0ID0gMDtcblxuICBmb3IgKHZhciBpID0gMDsgaSA8PSBxdWVyeS5sZW5ndGg7IGkrKykge1xuICAgIHZhciBjaGFyID0gcXVlcnkuY2hhckNvZGVBdChpKTtcblxuICAgIGlmIChjaGFyID09PSBIQVNIIHx8IGNoYXIgPT09IERPVCB8fCAhY2hhcikge1xuICAgICAgaWYgKG1vZGUgPT09IDApIHtcbiAgICAgICAgaWYgKGkgPT09IDApIHtcbiAgICAgICAgICB0YWcgPSAnZGl2JztcbiAgICAgICAgfSBlbHNlIGlmICghY2hhcikge1xuICAgICAgICAgIHRhZyA9IHF1ZXJ5O1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHRhZyA9IHF1ZXJ5LnN1YnN0cmluZyhzdGFydCwgaSk7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHZhciBzbGljZSA9IHF1ZXJ5LnN1YnN0cmluZyhzdGFydCwgaSk7XG5cbiAgICAgICAgaWYgKG1vZGUgPT09IDEpIHtcbiAgICAgICAgICBpZCA9IHNsaWNlO1xuICAgICAgICB9IGVsc2UgaWYgKGNsYXNzTmFtZSkge1xuICAgICAgICAgIGNsYXNzTmFtZSArPSAnICcgKyBzbGljZTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBjbGFzc05hbWUgPSBzbGljZTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBzdGFydCA9IGkgKyAxO1xuXG4gICAgICBpZiAoY2hhciA9PT0gSEFTSCkge1xuICAgICAgICBtb2RlID0gMTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIG1vZGUgPSAyO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHZhciBlbGVtZW50ID0gbnMgPyBkb2N1bWVudC5jcmVhdGVFbGVtZW50TlMobnMsIHRhZykgOiBkb2N1bWVudC5jcmVhdGVFbGVtZW50KHRhZyk7XG5cbiAgaWYgKGlkKSB7XG4gICAgZWxlbWVudC5pZCA9IGlkO1xuICB9XG5cbiAgaWYgKGNsYXNzTmFtZSkge1xuICAgIGlmIChucykge1xuICAgICAgZWxlbWVudC5zZXRBdHRyaWJ1dGUoJ2NsYXNzJywgY2xhc3NOYW1lKTtcbiAgICB9IGVsc2Uge1xuICAgICAgZWxlbWVudC5jbGFzc05hbWUgPSBjbGFzc05hbWU7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIGVsZW1lbnQ7XG59XG5cbnZhciBob29rTmFtZXMgPSBbJ29ubW91bnQnLCAnb251bm1vdW50J107XG5cbmZ1bmN0aW9uIG1vdW50IChwYXJlbnQsIGNoaWxkLCBiZWZvcmUpIHtcbiAgdmFyIHBhcmVudEVsID0gZ2V0RWwocGFyZW50KTtcbiAgdmFyIGNoaWxkRWwgPSBnZXRFbChjaGlsZCk7XG5cbiAgaWYgKGNoaWxkID09PSBjaGlsZEVsICYmIGNoaWxkRWwuX19yZWRvbV92aWV3KSB7XG4gICAgLy8gdHJ5IHRvIGxvb2sgdXAgdGhlIHZpZXcgaWYgbm90IHByb3ZpZGVkXG4gICAgY2hpbGQgPSBjaGlsZEVsLl9fcmVkb21fdmlldztcbiAgfVxuXG4gIGlmIChjaGlsZCAhPT0gY2hpbGRFbCkge1xuICAgIGNoaWxkRWwuX19yZWRvbV92aWV3ID0gY2hpbGQ7XG4gIH1cblxuICB2YXIgd2FzTW91bnRlZCA9IGNoaWxkRWwuX19yZWRvbV9tb3VudGVkO1xuICB2YXIgb2xkUGFyZW50ID0gY2hpbGRFbC5wYXJlbnROb2RlO1xuXG4gIGlmICh3YXNNb3VudGVkICYmIChvbGRQYXJlbnQgIT09IHBhcmVudEVsKSkge1xuICAgIGRvVW5tb3VudChjaGlsZCwgY2hpbGRFbCwgb2xkUGFyZW50KTtcbiAgfVxuXG4gIGlmIChiZWZvcmUgIT0gbnVsbCkge1xuICAgIHBhcmVudEVsLmluc2VydEJlZm9yZShjaGlsZEVsLCBnZXRFbChiZWZvcmUpKTtcbiAgfSBlbHNlIHtcbiAgICBwYXJlbnRFbC5hcHBlbmRDaGlsZChjaGlsZEVsKTtcbiAgfVxuXG4gIGRvTW91bnQoY2hpbGQsIGNoaWxkRWwsIHBhcmVudEVsLCBvbGRQYXJlbnQpO1xuXG4gIHJldHVybiBjaGlsZDtcbn1cblxuZnVuY3Rpb24gdW5tb3VudCAocGFyZW50LCBjaGlsZCkge1xuICB2YXIgcGFyZW50RWwgPSBwYXJlbnQuZWwgfHwgcGFyZW50O1xuICB2YXIgY2hpbGRFbCA9IGNoaWxkLmVsIHx8IGNoaWxkO1xuXG4gIGlmIChjaGlsZCA9PT0gY2hpbGRFbCAmJiBjaGlsZEVsLl9fcmVkb21fdmlldykge1xuICAgIC8vIHRyeSB0byBsb29rIHVwIHRoZSB2aWV3IGlmIG5vdCBwcm92aWRlZFxuICAgIGNoaWxkID0gY2hpbGRFbC5fX3JlZG9tX3ZpZXc7XG4gIH1cblxuICBkb1VubW91bnQoY2hpbGQsIGNoaWxkRWwsIHBhcmVudEVsKTtcblxuICBwYXJlbnRFbC5yZW1vdmVDaGlsZChjaGlsZEVsKTtcblxuICByZXR1cm4gY2hpbGQ7XG59XG5cbmZ1bmN0aW9uIGRvTW91bnQgKGNoaWxkLCBjaGlsZEVsLCBwYXJlbnRFbCwgb2xkUGFyZW50KSB7XG4gIHZhciBob29rcyA9IGNoaWxkRWwuX19yZWRvbV9saWZlY3ljbGUgfHwgKGNoaWxkRWwuX19yZWRvbV9saWZlY3ljbGUgPSB7fSk7XG4gIHZhciByZW1vdW50ID0gKHBhcmVudEVsID09PSBvbGRQYXJlbnQpO1xuICB2YXIgaG9va3NGb3VuZCA9IGZhbHNlO1xuXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgaG9va05hbWVzLmxlbmd0aDsgaSsrKSB7XG4gICAgdmFyIGhvb2tOYW1lID0gaG9va05hbWVzW2ldO1xuXG4gICAgaWYgKCFyZW1vdW50ICYmIChjaGlsZCAhPT0gY2hpbGRFbCkgJiYgKGhvb2tOYW1lIGluIGNoaWxkKSkge1xuICAgICAgaG9va3NbaG9va05hbWVdID0gKGhvb2tzW2hvb2tOYW1lXSB8fCAwKSArIDE7XG4gICAgfVxuICAgIGlmIChob29rc1tob29rTmFtZV0pIHtcbiAgICAgIGhvb2tzRm91bmQgPSB0cnVlO1xuICAgIH1cbiAgfVxuXG4gIGlmICghaG9va3NGb3VuZCkge1xuICAgIGNoaWxkRWwuX19yZWRvbV9tb3VudGVkID0gdHJ1ZTtcbiAgICByZXR1cm47XG4gIH1cblxuICB2YXIgdHJhdmVyc2UgPSBwYXJlbnRFbDtcbiAgdmFyIHRyaWdnZXJlZCA9IGZhbHNlO1xuXG4gIGlmIChyZW1vdW50IHx8ICghdHJpZ2dlcmVkICYmICh0cmF2ZXJzZSAmJiB0cmF2ZXJzZS5fX3JlZG9tX21vdW50ZWQpKSkge1xuICAgIHRyaWdnZXIoY2hpbGRFbCwgcmVtb3VudCA/ICdvbnJlbW91bnQnIDogJ29ubW91bnQnKTtcbiAgICB0cmlnZ2VyZWQgPSB0cnVlO1xuICB9XG5cbiAgaWYgKHJlbW91bnQpIHtcbiAgICByZXR1cm47XG4gIH1cblxuICB3aGlsZSAodHJhdmVyc2UpIHtcbiAgICB2YXIgcGFyZW50ID0gdHJhdmVyc2UucGFyZW50Tm9kZTtcbiAgICB2YXIgcGFyZW50SG9va3MgPSB0cmF2ZXJzZS5fX3JlZG9tX2xpZmVjeWNsZSB8fCAodHJhdmVyc2UuX19yZWRvbV9saWZlY3ljbGUgPSB7fSk7XG5cbiAgICBmb3IgKHZhciBob29rIGluIGhvb2tzKSB7XG4gICAgICBwYXJlbnRIb29rc1tob29rXSA9IChwYXJlbnRIb29rc1tob29rXSB8fCAwKSArIGhvb2tzW2hvb2tdO1xuICAgIH1cblxuICAgIGlmICghdHJpZ2dlcmVkICYmICh0cmF2ZXJzZSA9PT0gZG9jdW1lbnQgfHwgKHBhcmVudCAmJiBwYXJlbnQuX19yZWRvbV9tb3VudGVkKSkpIHtcbiAgICAgIHRyaWdnZXIodHJhdmVyc2UsIHJlbW91bnQgPyAnb25yZW1vdW50JyA6ICdvbm1vdW50Jyk7XG4gICAgICB0cmlnZ2VyZWQgPSB0cnVlO1xuICAgIH1cblxuICAgIHRyYXZlcnNlID0gcGFyZW50O1xuICB9XG59XG5cbmZ1bmN0aW9uIGRvVW5tb3VudCAoY2hpbGQsIGNoaWxkRWwsIHBhcmVudEVsKSB7XG4gIHZhciBob29rcyA9IGNoaWxkRWwuX19yZWRvbV9saWZlY3ljbGU7XG5cbiAgaWYgKCFob29rcykge1xuICAgIGNoaWxkRWwuX19yZWRvbV9tb3VudGVkID0gZmFsc2U7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgdmFyIHRyYXZlcnNlID0gcGFyZW50RWw7XG5cbiAgaWYgKGNoaWxkRWwuX19yZWRvbV9tb3VudGVkKSB7XG4gICAgdHJpZ2dlcihjaGlsZEVsLCAnb251bm1vdW50Jyk7XG4gIH1cblxuICB3aGlsZSAodHJhdmVyc2UpIHtcbiAgICB2YXIgcGFyZW50SG9va3MgPSB0cmF2ZXJzZS5fX3JlZG9tX2xpZmVjeWNsZSB8fCAodHJhdmVyc2UuX19yZWRvbV9saWZlY3ljbGUgPSB7fSk7XG4gICAgdmFyIGhvb2tzRm91bmQgPSBmYWxzZTtcblxuICAgIGZvciAodmFyIGhvb2sgaW4gaG9va3MpIHtcbiAgICAgIGlmIChwYXJlbnRIb29rc1tob29rXSkge1xuICAgICAgICBwYXJlbnRIb29rc1tob29rXSAtPSBob29rc1tob29rXTtcbiAgICAgIH1cbiAgICAgIGlmIChwYXJlbnRIb29rc1tob29rXSkge1xuICAgICAgICBob29rc0ZvdW5kID0gdHJ1ZTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAoIWhvb2tzRm91bmQpIHtcbiAgICAgIHRyYXZlcnNlLl9fcmVkb21fbGlmZWN5Y2xlID0gbnVsbDtcbiAgICB9XG5cbiAgICB0cmF2ZXJzZSA9IHRyYXZlcnNlLnBhcmVudE5vZGU7XG4gIH1cbn1cblxuZnVuY3Rpb24gdHJpZ2dlciAoZWwsIGV2ZW50TmFtZSkge1xuICBpZiAoZXZlbnROYW1lID09PSAnb25tb3VudCcpIHtcbiAgICBlbC5fX3JlZG9tX21vdW50ZWQgPSB0cnVlO1xuICB9IGVsc2UgaWYgKGV2ZW50TmFtZSA9PT0gJ29udW5tb3VudCcpIHtcbiAgICBlbC5fX3JlZG9tX21vdW50ZWQgPSBmYWxzZTtcbiAgfVxuXG4gIHZhciBob29rcyA9IGVsLl9fcmVkb21fbGlmZWN5Y2xlO1xuXG4gIGlmICghaG9va3MpIHtcbiAgICByZXR1cm47XG4gIH1cblxuICB2YXIgdmlldyA9IGVsLl9fcmVkb21fdmlldztcbiAgdmFyIGhvb2tDb3VudCA9IDA7XG5cbiAgdmlldyAmJiB2aWV3W2V2ZW50TmFtZV0gJiYgdmlld1tldmVudE5hbWVdKCk7XG5cbiAgZm9yICh2YXIgaG9vayBpbiBob29rcykge1xuICAgIGlmIChob29rKSB7XG4gICAgICBob29rQ291bnQrKztcbiAgICB9XG4gIH1cblxuICBpZiAoaG9va0NvdW50KSB7XG4gICAgdmFyIHRyYXZlcnNlID0gZWwuZmlyc3RDaGlsZDtcblxuICAgIHdoaWxlICh0cmF2ZXJzZSkge1xuICAgICAgdmFyIG5leHQgPSB0cmF2ZXJzZS5uZXh0U2libGluZztcblxuICAgICAgdHJpZ2dlcih0cmF2ZXJzZSwgZXZlbnROYW1lKTtcblxuICAgICAgdHJhdmVyc2UgPSBuZXh0O1xuICAgIH1cbiAgfVxufVxuXG5mdW5jdGlvbiBzZXRTdHlsZSAodmlldywgYXJnMSwgYXJnMikge1xuICB2YXIgZWwgPSBnZXRFbCh2aWV3KTtcblxuICBpZiAoYXJnMiAhPT0gdW5kZWZpbmVkKSB7XG4gICAgZWwuc3R5bGVbYXJnMV0gPSBhcmcyO1xuICB9IGVsc2UgaWYgKGlzU3RyaW5nKGFyZzEpKSB7XG4gICAgZWwuc2V0QXR0cmlidXRlKCdzdHlsZScsIGFyZzEpO1xuICB9IGVsc2Uge1xuICAgIGZvciAodmFyIGtleSBpbiBhcmcxKSB7XG4gICAgICBzZXRTdHlsZShlbCwga2V5LCBhcmcxW2tleV0pO1xuICAgIH1cbiAgfVxufVxuXG5mdW5jdGlvbiBzZXRBdHRyICh2aWV3LCBhcmcxLCBhcmcyKSB7XG4gIHZhciBlbCA9IGdldEVsKHZpZXcpO1xuICB2YXIgaXNTVkcgPSBlbCBpbnN0YW5jZW9mIHdpbmRvdy5TVkdFbGVtZW50O1xuXG4gIGlmIChhcmcyICE9PSB1bmRlZmluZWQpIHtcbiAgICBpZiAoYXJnMSA9PT0gJ3N0eWxlJykge1xuICAgICAgc2V0U3R5bGUoZWwsIGFyZzIpO1xuICAgIH0gZWxzZSBpZiAoaXNTVkcgJiYgaXNGdW5jdGlvbihhcmcyKSkge1xuICAgICAgZWxbYXJnMV0gPSBhcmcyO1xuICAgIH0gZWxzZSBpZiAoIWlzU1ZHICYmIChhcmcxIGluIGVsIHx8IGlzRnVuY3Rpb24oYXJnMikpKSB7XG4gICAgICBlbFthcmcxXSA9IGFyZzI7XG4gICAgfSBlbHNlIHtcbiAgICAgIGVsLnNldEF0dHJpYnV0ZShhcmcxLCBhcmcyKTtcbiAgICB9XG4gIH0gZWxzZSB7XG4gICAgZm9yICh2YXIga2V5IGluIGFyZzEpIHtcbiAgICAgIHNldEF0dHIoZWwsIGtleSwgYXJnMVtrZXldKTtcbiAgICB9XG4gIH1cbn1cblxudmFyIHRleHQgPSBmdW5jdGlvbiAoc3RyKSB7IHJldHVybiBkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZShzdHIpOyB9O1xuXG5mdW5jdGlvbiBwYXJzZUFyZ3VtZW50cyAoZWxlbWVudCwgYXJncykge1xuICBmb3IgKHZhciBpID0gMDsgaSA8IGFyZ3MubGVuZ3RoOyBpKyspIHtcbiAgICB2YXIgYXJnID0gYXJnc1tpXTtcblxuICAgIGlmIChhcmcgIT09IDAgJiYgIWFyZykge1xuICAgICAgY29udGludWU7XG4gICAgfVxuXG4gICAgLy8gc3VwcG9ydCBtaWRkbGV3YXJlXG4gICAgaWYgKHR5cGVvZiBhcmcgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgIGFyZyhlbGVtZW50KTtcbiAgICB9IGVsc2UgaWYgKGlzU3RyaW5nKGFyZykgfHwgaXNOdW1iZXIoYXJnKSkge1xuICAgICAgZWxlbWVudC5hcHBlbmRDaGlsZCh0ZXh0KGFyZykpO1xuICAgIH0gZWxzZSBpZiAoaXNOb2RlKGdldEVsKGFyZykpKSB7XG4gICAgICBtb3VudChlbGVtZW50LCBhcmcpO1xuICAgIH0gZWxzZSBpZiAoYXJnLmxlbmd0aCkge1xuICAgICAgcGFyc2VBcmd1bWVudHMoZWxlbWVudCwgYXJnKTtcbiAgICB9IGVsc2UgaWYgKHR5cGVvZiBhcmcgPT09ICdvYmplY3QnKSB7XG4gICAgICBzZXRBdHRyKGVsZW1lbnQsIGFyZyk7XG4gICAgfVxuICB9XG59XG5cbnZhciBlbnN1cmVFbCA9IGZ1bmN0aW9uIChwYXJlbnQpIHsgcmV0dXJuIGlzU3RyaW5nKHBhcmVudCkgPyBodG1sKHBhcmVudCkgOiBnZXRFbChwYXJlbnQpOyB9O1xudmFyIGdldEVsID0gZnVuY3Rpb24gKHBhcmVudCkgeyByZXR1cm4gKHBhcmVudC5ub2RlVHlwZSAmJiBwYXJlbnQpIHx8ICghcGFyZW50LmVsICYmIHBhcmVudCkgfHwgZ2V0RWwocGFyZW50LmVsKTsgfTtcblxudmFyIGlzU3RyaW5nID0gZnVuY3Rpb24gKGEpIHsgcmV0dXJuIHR5cGVvZiBhID09PSAnc3RyaW5nJzsgfTtcbnZhciBpc051bWJlciA9IGZ1bmN0aW9uIChhKSB7IHJldHVybiB0eXBlb2YgYSA9PT0gJ251bWJlcic7IH07XG52YXIgaXNGdW5jdGlvbiA9IGZ1bmN0aW9uIChhKSB7IHJldHVybiB0eXBlb2YgYSA9PT0gJ2Z1bmN0aW9uJzsgfTtcblxudmFyIGlzTm9kZSA9IGZ1bmN0aW9uIChhKSB7IHJldHVybiBhICYmIGEubm9kZVR5cGU7IH07XG5cbnZhciBodG1sQ2FjaGUgPSB7fTtcblxudmFyIG1lbW9pemVIVE1MID0gZnVuY3Rpb24gKHF1ZXJ5KSB7IHJldHVybiBodG1sQ2FjaGVbcXVlcnldIHx8IChodG1sQ2FjaGVbcXVlcnldID0gY3JlYXRlRWxlbWVudChxdWVyeSkpOyB9O1xuXG5mdW5jdGlvbiBodG1sIChxdWVyeSkge1xuICB2YXIgYXJncyA9IFtdLCBsZW4gPSBhcmd1bWVudHMubGVuZ3RoIC0gMTtcbiAgd2hpbGUgKCBsZW4tLSA+IDAgKSBhcmdzWyBsZW4gXSA9IGFyZ3VtZW50c1sgbGVuICsgMSBdO1xuXG4gIHZhciBlbGVtZW50O1xuXG4gIGlmIChpc1N0cmluZyhxdWVyeSkpIHtcbiAgICBlbGVtZW50ID0gbWVtb2l6ZUhUTUwocXVlcnkpLmNsb25lTm9kZShmYWxzZSk7XG4gIH0gZWxzZSBpZiAoaXNOb2RlKHF1ZXJ5KSkge1xuICAgIGVsZW1lbnQgPSBxdWVyeS5jbG9uZU5vZGUoZmFsc2UpO1xuICB9IGVsc2Uge1xuICAgIHRocm93IG5ldyBFcnJvcignQXQgbGVhc3Qgb25lIGFyZ3VtZW50IHJlcXVpcmVkJyk7XG4gIH1cblxuICBwYXJzZUFyZ3VtZW50cyhlbGVtZW50LCBhcmdzKTtcblxuICByZXR1cm4gZWxlbWVudDtcbn1cblxuaHRtbC5leHRlbmQgPSBmdW5jdGlvbiAocXVlcnkpIHtcbiAgdmFyIGNsb25lID0gbWVtb2l6ZUhUTUwocXVlcnkpO1xuXG4gIHJldHVybiBodG1sLmJpbmQodGhpcywgY2xvbmUpO1xufTtcblxudmFyIGVsID0gaHRtbDtcblxuZnVuY3Rpb24gc2V0Q2hpbGRyZW4gKHBhcmVudCwgY2hpbGRyZW4pIHtcbiAgaWYgKGNoaWxkcmVuLmxlbmd0aCA9PT0gdW5kZWZpbmVkKSB7XG4gICAgcmV0dXJuIHNldENoaWxkcmVuKHBhcmVudCwgW2NoaWxkcmVuXSk7XG4gIH1cblxuICB2YXIgcGFyZW50RWwgPSBnZXRFbChwYXJlbnQpO1xuICB2YXIgdHJhdmVyc2UgPSBwYXJlbnRFbC5maXJzdENoaWxkO1xuXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgY2hpbGRyZW4ubGVuZ3RoOyBpKyspIHtcbiAgICB2YXIgY2hpbGQgPSBjaGlsZHJlbltpXTtcblxuICAgIGlmICghY2hpbGQpIHtcbiAgICAgIGNvbnRpbnVlO1xuICAgIH1cblxuICAgIHZhciBjaGlsZEVsID0gZ2V0RWwoY2hpbGQpO1xuXG4gICAgaWYgKGNoaWxkRWwgPT09IHRyYXZlcnNlKSB7XG4gICAgICB0cmF2ZXJzZSA9IHRyYXZlcnNlLm5leHRTaWJsaW5nO1xuICAgICAgY29udGludWU7XG4gICAgfVxuXG4gICAgbW91bnQocGFyZW50LCBjaGlsZCwgdHJhdmVyc2UpO1xuICB9XG5cbiAgd2hpbGUgKHRyYXZlcnNlKSB7XG4gICAgdmFyIG5leHQgPSB0cmF2ZXJzZS5uZXh0U2libGluZztcblxuICAgIHVubW91bnQocGFyZW50LCB0cmF2ZXJzZSk7XG5cbiAgICB0cmF2ZXJzZSA9IG5leHQ7XG4gIH1cbn1cblxudmFyIHByb3BLZXkgPSBmdW5jdGlvbiAoa2V5KSB7IHJldHVybiBmdW5jdGlvbiAoaXRlbSkgeyByZXR1cm4gaXRlbVtrZXldOyB9OyB9O1xuXG5mdW5jdGlvbiBsaXN0IChwYXJlbnQsIFZpZXcsIGtleSwgaW5pdERhdGEpIHtcbiAgcmV0dXJuIG5ldyBMaXN0KHBhcmVudCwgVmlldywga2V5LCBpbml0RGF0YSk7XG59XG5cbmZ1bmN0aW9uIExpc3QgKHBhcmVudCwgVmlldywga2V5LCBpbml0RGF0YSkge1xuICB0aGlzLl9fcmVkb21fbGlzdCA9IHRydWU7XG4gIHRoaXMuVmlldyA9IFZpZXc7XG4gIHRoaXMuaW5pdERhdGEgPSBpbml0RGF0YTtcbiAgdGhpcy52aWV3cyA9IFtdO1xuICB0aGlzLmVsID0gZW5zdXJlRWwocGFyZW50KTtcblxuICBpZiAoa2V5ICE9IG51bGwpIHtcbiAgICB0aGlzLmxvb2t1cCA9IHt9O1xuICAgIHRoaXMua2V5ID0gaXNGdW5jdGlvbihrZXkpID8ga2V5IDogcHJvcEtleShrZXkpO1xuICB9XG59XG5cbkxpc3QuZXh0ZW5kID0gZnVuY3Rpb24gKHBhcmVudCwgVmlldywga2V5LCBpbml0RGF0YSkge1xuICByZXR1cm4gTGlzdC5iaW5kKExpc3QsIHBhcmVudCwgVmlldywga2V5LCBpbml0RGF0YSk7XG59O1xuXG5saXN0LmV4dGVuZCA9IExpc3QuZXh0ZW5kO1xuXG5MaXN0LnByb3RvdHlwZS51cGRhdGUgPSBmdW5jdGlvbiAoZGF0YSkge1xuICB2YXIgdGhpcyQxID0gdGhpcztcbiAgaWYgKCBkYXRhID09PSB2b2lkIDAgKSBkYXRhID0gW107XG5cbiAgdmFyIFZpZXcgPSB0aGlzLlZpZXc7XG4gIHZhciBrZXkgPSB0aGlzLmtleTtcbiAgdmFyIGtleVNldCA9IGtleSAhPSBudWxsO1xuICB2YXIgaW5pdERhdGEgPSB0aGlzLmluaXREYXRhO1xuICB2YXIgbmV3Vmlld3MgPSBuZXcgQXJyYXkoZGF0YS5sZW5ndGgpO1xuICB2YXIgb2xkVmlld3MgPSB0aGlzLnZpZXdzO1xuICB2YXIgbmV3TG9va3VwID0ga2V5ICYmIHt9O1xuICB2YXIgb2xkTG9va3VwID0ga2V5ICYmIHRoaXMubG9va3VwO1xuXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgZGF0YS5sZW5ndGg7IGkrKykge1xuICAgIHZhciBpdGVtID0gZGF0YVtpXTtcbiAgICB2YXIgdmlldyA9ICh2b2lkIDApO1xuXG4gICAgaWYgKGtleVNldCkge1xuICAgICAgdmFyIGlkID0ga2V5KGl0ZW0pO1xuICAgICAgdmlldyA9IG5ld1ZpZXdzW2ldID0gb2xkTG9va3VwW2lkXSB8fCBuZXcgVmlldyhpbml0RGF0YSwgaXRlbSwgaSwgZGF0YSk7XG4gICAgICBuZXdMb29rdXBbaWRdID0gdmlldztcbiAgICAgIHZpZXcuX19pZCA9IGlkO1xuICAgIH0gZWxzZSB7XG4gICAgICB2aWV3ID0gbmV3Vmlld3NbaV0gPSBvbGRWaWV3c1tpXSB8fCBuZXcgVmlldyhpbml0RGF0YSwgaXRlbSwgaSwgZGF0YSk7XG4gICAgfVxuICAgIHZhciBlbCA9IHZpZXcuZWw7XG4gICAgaWYgKGVsLl9fcmVkb21fbGlzdCkge1xuICAgICAgZWwgPSBlbC5lbDtcbiAgICB9XG4gICAgZWwuX19yZWRvbV92aWV3ID0gdmlldztcbiAgICB2aWV3LnVwZGF0ZSAmJiB2aWV3LnVwZGF0ZShpdGVtLCBpLCBkYXRhKTtcbiAgfVxuXG4gIGlmIChrZXlTZXQpIHtcbiAgICBmb3IgKHZhciBpJDEgPSAwOyBpJDEgPCBvbGRWaWV3cy5sZW5ndGg7IGkkMSsrKSB7XG4gICAgICB2YXIgaWQkMSA9IG9sZFZpZXdzW2kkMV0uX19pZDtcblxuICAgICAgaWYgKCEoaWQkMSBpbiBuZXdMb29rdXApKSB7XG4gICAgICAgIHVubW91bnQodGhpcyQxLCBvbGRMb29rdXBbaWQkMV0pO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHNldENoaWxkcmVuKHRoaXMsIG5ld1ZpZXdzKTtcblxuICBpZiAoa2V5U2V0KSB7XG4gICAgdGhpcy5sb29rdXAgPSBuZXdMb29rdXA7XG4gIH1cbiAgdGhpcy52aWV3cyA9IG5ld1ZpZXdzO1xufTtcblxuZnVuY3Rpb24gcm91dGVyIChwYXJlbnQsIFZpZXdzLCBpbml0RGF0YSkge1xuICByZXR1cm4gbmV3IFJvdXRlcihwYXJlbnQsIFZpZXdzLCBpbml0RGF0YSk7XG59XG5cbnZhciBSb3V0ZXIgPSBmdW5jdGlvbiBSb3V0ZXIgKHBhcmVudCwgVmlld3MsIGluaXREYXRhKSB7XG4gIHRoaXMuZWwgPSBlbnN1cmVFbChwYXJlbnQpO1xuICB0aGlzLlZpZXdzID0gVmlld3M7XG4gIHRoaXMuaW5pdERhdGEgPSBpbml0RGF0YTtcbn07XG5Sb3V0ZXIucHJvdG90eXBlLnVwZGF0ZSA9IGZ1bmN0aW9uIHVwZGF0ZSAocm91dGUsIGRhdGEpIHtcbiAgaWYgKHJvdXRlICE9PSB0aGlzLnJvdXRlKSB7XG4gICAgdmFyIFZpZXdzID0gdGhpcy5WaWV3cztcbiAgICB2YXIgVmlldyA9IFZpZXdzW3JvdXRlXTtcblxuICAgIHRoaXMudmlldyA9IFZpZXcgJiYgbmV3IFZpZXcodGhpcy5pbml0RGF0YSwgZGF0YSk7XG4gICAgdGhpcy5yb3V0ZSA9IHJvdXRlO1xuXG4gICAgc2V0Q2hpbGRyZW4odGhpcy5lbCwgWyB0aGlzLnZpZXcgXSk7XG4gIH1cbiAgdGhpcy52aWV3ICYmIHRoaXMudmlldy51cGRhdGUgJiYgdGhpcy52aWV3LnVwZGF0ZShkYXRhLCByb3V0ZSk7XG59O1xuXG52YXIgU1ZHID0gJ2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJztcblxudmFyIHN2Z0NhY2hlID0ge307XG5cbnZhciBtZW1vaXplU1ZHID0gZnVuY3Rpb24gKHF1ZXJ5KSB7IHJldHVybiBzdmdDYWNoZVtxdWVyeV0gfHwgKHN2Z0NhY2hlW3F1ZXJ5XSA9IGNyZWF0ZUVsZW1lbnQocXVlcnksIFNWRykpOyB9O1xuXG5mdW5jdGlvbiBzdmcgKHF1ZXJ5KSB7XG4gIHZhciBhcmdzID0gW10sIGxlbiA9IGFyZ3VtZW50cy5sZW5ndGggLSAxO1xuICB3aGlsZSAoIGxlbi0tID4gMCApIGFyZ3NbIGxlbiBdID0gYXJndW1lbnRzWyBsZW4gKyAxIF07XG5cbiAgdmFyIGVsZW1lbnQ7XG5cbiAgaWYgKGlzU3RyaW5nKHF1ZXJ5KSkge1xuICAgIGVsZW1lbnQgPSBtZW1vaXplU1ZHKHF1ZXJ5KS5jbG9uZU5vZGUoZmFsc2UpO1xuICB9IGVsc2UgaWYgKGlzTm9kZShxdWVyeSkpIHtcbiAgICBlbGVtZW50ID0gcXVlcnkuY2xvbmVOb2RlKGZhbHNlKTtcbiAgfSBlbHNlIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ0F0IGxlYXN0IG9uZSBhcmd1bWVudCByZXF1aXJlZCcpO1xuICB9XG5cbiAgcGFyc2VBcmd1bWVudHMoZWxlbWVudCwgYXJncyk7XG5cbiAgcmV0dXJuIGVsZW1lbnQ7XG59XG5cbnN2Zy5leHRlbmQgPSBmdW5jdGlvbiAocXVlcnkpIHtcbiAgdmFyIGNsb25lID0gbWVtb2l6ZVNWRyhxdWVyeSk7XG5cbiAgcmV0dXJuIHN2Zy5iaW5kKHRoaXMsIGNsb25lKTtcbn07XG5cbmV4cG9ydHMuaHRtbCA9IGh0bWw7XG5leHBvcnRzLmVsID0gZWw7XG5leHBvcnRzLmxpc3QgPSBsaXN0O1xuZXhwb3J0cy5MaXN0ID0gTGlzdDtcbmV4cG9ydHMubW91bnQgPSBtb3VudDtcbmV4cG9ydHMudW5tb3VudCA9IHVubW91bnQ7XG5leHBvcnRzLnJvdXRlciA9IHJvdXRlcjtcbmV4cG9ydHMuUm91dGVyID0gUm91dGVyO1xuZXhwb3J0cy5zZXRBdHRyID0gc2V0QXR0cjtcbmV4cG9ydHMuc2V0U3R5bGUgPSBzZXRTdHlsZTtcbmV4cG9ydHMuc2V0Q2hpbGRyZW4gPSBzZXRDaGlsZHJlbjtcbmV4cG9ydHMuc3ZnID0gc3ZnO1xuZXhwb3J0cy50ZXh0ID0gdGV4dDtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcblxufSkpKTtcbiIsImNvbnN0IHsgZWwsIG1vdW50LCBsaXN0LCB1bm1vdW50fSAgPSByZXF1aXJlKCdyZWRvbScpXG5jb25zdCBSYW5kb20gPSByZXF1aXJlKCdyYW5kb20tanMnKVxuXG5sZXQgcmFuZG9tID0gbmV3IFJhbmRvbShSYW5kb20uZW5naW5lcy5icm93c2VyQ3J5cHRvKVxuXG5jbGFzcyBJdGVtIHtcbiAgICBjb25zdHJ1Y3Rvcigpe1xuICAgICAgICB0aGlzLnJlbW92ZSA9IHRoaXMucmVtb3ZlLmJpbmQodGhpcylcbiAgICAgICAgdGhpcy5lbCA9IGVsKCcuaXRlbScsXG4gICAgICAgICAgICB0aGlzLml0ZW1OYW1lID0gZWwoJy5pdGVtLW5hbWUnKSxcbiAgICAgICAgICAgIHRoaXMuaXRlbURlbGV0ZSA9IGVsKCcuaXRlbS1kZWxldGUnLCAneCcpXG4gICAgICAgIClcblxuICAgICAgICB0aGlzLml0ZW1EZWxldGUub25jbGljayA9IHRoaXMucmVtb3ZlXG4gICAgfVxuXG4gICAgcmVtb3ZlKCl7XG4gICAgICAgIGNvbnN0IHsgaWQgfSA9IHRoaXMuZGF0YTtcbiAgICAgICAgY29uc3QgZXZlbnQgPSBuZXcgQ3VzdG9tRXZlbnQoJ2RlbGV0ZV9pdGVtJywgeyBkZXRhaWw6IGlkLCBidWJibGVzOiB0cnVlIH0pXG4gICAgICAgIHRoaXMuZWwuZGlzcGF0Y2hFdmVudChldmVudClcbiAgICB9XG5cbiAgICB1cGRhdGUoZGF0YSl7XG4gICAgICAgIHRoaXMuaXRlbU5hbWUudGV4dENvbnRlbnQgPSB0aGlzLm5hbWUgPSBkYXRhLm5hbWVcbiAgICAgICAgdGhpcy5kYXRhID0gZGF0YVxuICAgIH1cbn1cblxuY2xhc3MgSXRlbXNMaXN0e1xuICAgIGNvbnN0cnVjdG9yKCl7XG4gICAgICAgIHRoaXMuZWwgPSBlbCgnLml0ZW0tY29udGFpbmVyJylcbiAgICAgICAgdGhpcy5saXN0ID0gbGlzdCh0aGlzLmVsLCBJdGVtLCAnaWQnKVxuICAgICAgICB0aGlzLmlkID0gMFxuICAgICAgICB0aGlzLml0ZW1zID0gW11cblxuICAgICAgICB0aGlzLmVsLmFkZEV2ZW50TGlzdGVuZXIoJ2RlbGV0ZV9pdGVtJywgZSA9PiB7XG4gICAgICAgICAgICB0aGlzLml0ZW1zID0gdGhpcy5pdGVtcy5maWx0ZXIoZWw9PmVsLmlkICE9PSBlLmRldGFpbClcbiAgICAgICAgICAgIHRoaXMubGlzdC51cGRhdGUodGhpcy5pdGVtcylcbiAgICAgICAgfSlcbiAgICB9XG5cbiAgICB1cGRhdGUoZGF0YSl7XG4gICAgICAgIGlmKGRhdGEuYWN0aW9uID09PSAnYWRkX2l0ZW0nICYmIGRhdGEubmFtZSl7XG4gICAgICAgICAgICB0aGlzLml0ZW1zLnB1c2goe1xuICAgICAgICAgICAgICAgIGlkIDogdGhpcy5pZCsrLFxuICAgICAgICAgICAgICAgIG5hbWUgOiBkYXRhLm5hbWVcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICB0aGlzLmxpc3QudXBkYXRlKHRoaXMuaXRlbXMpXG4gICAgICAgIH1cbiAgICB9XG59XG5cbmNsYXNzIEl0ZW1JbnB1dCB7XG4gICAgY29uc3RydWN0b3IoYWRkSXRlbSl7XG4gICAgICAgIHRoaXMucG9wID0gdGhpcy5wb3AuYmluZCh0aGlzKVxuXG4gICAgICAgIHRoaXMuZWwgPSBlbCgnZm9ybS5pdGVtLWlucHV0JyxcbiAgICAgICAgICAgIHRoaXMubmV3SXRlbU5hbWUgPSBlbCgnaW5wdXQnLCB7dHlwZTondGV4dCcsIGF1dG9mb2N1czogdHJ1ZX0pLFxuICAgICAgICAgICAgdGhpcy5hZGRJdGVtQnRuID0gZWwoJ2lucHV0Jywge3R5cGU6J3N1Ym1pdCcsIHZhbHVlOidBZGQgSXRlbSd9KSxcbiAgICAgICAgICAgIGVsKCdsYWJlbCcsICdBZGQgb25lIG9yIG1hbnkgaXRlbXMgc2VwYXJhdGVkIGJ5IGNvbW1hcycpXG4gICAgICAgIClcblxuICAgICAgICB0aGlzLmFkZEl0ZW1CdG4ub25jbGljayA9IGFkZEl0ZW1cbiAgICAgICAgdGhpcy5lbC5vbnN1Ym1pdCA9IGFkZEl0ZW1cbiAgICB9XG5cbiAgICBwb3AoKXtcbiAgICAgICAgbGV0IHZhbHVlID0gdGhpcy5uZXdJdGVtTmFtZS52YWx1ZVxuICAgICAgICB0aGlzLm5ld0l0ZW1OYW1lLnZhbHVlID0gXCJcIlxuICAgICAgICB0aGlzLm5ld0l0ZW1OYW1lLmZvY3VzKClcbiAgICAgICAgcmV0dXJuIHZhbHVlXG4gICAgfVxufVxuXG5jbGFzcyBJdGVtUGlja2VyIHtcbiAgICBjb25zdHJ1Y3RvcigpeyAgXG4gICAgICAgIHRoaXMuYWRkSXRlbSA9IHRoaXMuYWRkSXRlbS5iaW5kKHRoaXMpXG5cbiAgICAgICAgdGhpcy5lbCA9IGVsKCdkaXYuZ2xvYmFsLWNvbnRhaW5lcicsXG4gICAgICAgICAgICBlbCgnZGl2LmhlYWRlcicsXG4gICAgICAgICAgICAgICAgdGhpcy5uZXdJdGVtRm9ybSA9IG5ldyBJdGVtSW5wdXQodGhpcy5hZGRJdGVtKVxuICAgICAgICAgICAgKSxcbiAgICAgICAgICAgIHRoaXMuaXRlbXMgPSBuZXcgSXRlbXNMaXN0KCksXG4gICAgICAgICAgICBlbCgnbGFiZWwnLCB0aGlzLnJlbW92ZVBpY2tlZCA9IGVsKCdpbnB1dCcsIHt0eXBlOlwiY2hlY2tib3hcIiwgY2hlY2tlZDogZmFsc2V9KSwgJ1JlbW92ZSBwaWNrZWQnKSxcbiAgICAgICAgICAgIHRoaXMucGlja1JhbmRvbSA9IGVsKCdidXR0b24nLCAnUGljayBSYW5kb20nKSxcbiAgICAgICAgICAgIHRoaXMucGlja2VkSXRlbSA9IGVsKCcucGlja2VkLWl0ZW0nKVxuICAgICAgICApXG5cbiAgICAgICAgdGhpcy5waWNrUmFuZG9tLm9uY2xpY2sgPSAoZSk9PntcbiAgICAgICAgICAgIGxldCBwaWNrZWRJdGVtID0gcmFuZG9tLnBpY2sodGhpcy5pdGVtcy5saXN0LnZpZXdzKVxuICAgICAgICAgICAgbGV0IG5hbWUgPSAocGlja2VkSXRlbSB8fCB7fSkubmFtZVxuICAgICAgICAgICAgaWYocGlja2VkSXRlbSAmJiB0aGlzLnJlbW92ZVBpY2tlZC5jaGVja2VkKXtcbiAgICAgICAgICAgICAgICBwaWNrZWRJdGVtLnJlbW92ZSgpXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aGlzLnBpY2tlZEl0ZW0udGV4dENvbnRlbnQgPSBuYW1lXG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBhZGRJdGVtKGUpe1xuICAgICAgICBlLnByZXZlbnREZWZhdWx0KClcbiAgICAgICAgdGhpc1xuICAgICAgICAubmV3SXRlbUZvcm1cbiAgICAgICAgLnBvcCgpXG4gICAgICAgIC5zcGxpdCgnLCcpXG4gICAgICAgIC5mb3JFYWNoKGl0ZW09PntcbiAgICAgICAgICAgIHRoaXMuaXRlbXMudXBkYXRlKHtcbiAgICAgICAgICAgICAgICBhY3Rpb24gOiAnYWRkX2l0ZW0nLFxuICAgICAgICAgICAgICAgIG5hbWUgOiBpdGVtLnRyaW0oKVxuICAgICAgICAgICAgfSlcbiAgICAgICAgfSlcbiAgICAgICAgXG4gICAgfVxufVxuXG5kb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdET01Db250ZW50TG9hZGVkJywgbWFpbilcbmZ1bmN0aW9uIG1haW4oKXtcbiAgICBtb3VudChkb2N1bWVudC5ib2R5LCBuZXcgSXRlbVBpY2tlcigpKVxufSJdfQ==
