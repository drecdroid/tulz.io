document
.getElementById('beautify-button')
.addEventListener('click', (e)=>{
    let input = document.getElementById('css-input')
    let output = document.getElementById('css-output')
    
    output.innerText = css_beautify(input.value)
})