/**
 * API
 * 
 * let history = new TextHistory()
 * 
 * history.write('hola mundo')
 * history.undo()
 * history.redo()
 * history.current
 * 
 */
function TextHistory(){
    let o = Object.create(TextHistory.prototype)
    o._pointer = 0
    o._history = []
    return o
}

TextHistory.prototype.redo = function(){
    (this._pointer<this._history.length-1) && (this._pointer++)
    return this.current
}

TextHistory.prototype.undo = function(){
    (this._pointer>0) && this._pointer--
    return this.current
}

TextHistory.prototype.write = function(version){
    if(this._pointer !== this._history.length - 1 ){
        this._history = this._history.slice(0, this._pointer)
        this._pointer = this._history.length
        this._history.push(version)
    }
}

Object.defineProperty(TextHistory.prototype, "current", {
    enumerable : true,
    get : function(){
        return this._history[this._pointer]
    }
})


module.exports = TextHistory