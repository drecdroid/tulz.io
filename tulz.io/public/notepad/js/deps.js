System.registerDynamic('github:jackmoore/autosize@3.0.21/dist/autosize.js', [], true, function ($__require, exports, module) {
	/* */
	"format cjs";
	/*!
 	Autosize 3.0.21
 	license: MIT
 	http://www.jacklmoore.com/autosize
 */

	var global = this || self,
	    GLOBAL = global;
	(function (global, factory) {
		if (typeof undefined === 'function' && define.amd) {
			define(['exports', 'module'], factory);
		} else if (typeof exports !== 'undefined' && typeof module !== 'undefined') {
			factory(exports, module);
		} else {
			var mod = {
				exports: {}
			};
			factory(mod.exports, mod);
			global.autosize = mod.exports;
		}
	})(this, function (exports, module) {
		'use strict';

		var map = typeof Map === "function" ? new Map() : function () {
			var keys = [];
			var values = [];

			return {
				has: function has(key) {
					return keys.indexOf(key) > -1;
				},
				get: function get(key) {
					return values[keys.indexOf(key)];
				},
				set: function set(key, value) {
					if (keys.indexOf(key) === -1) {
						keys.push(key);
						values.push(value);
					}
				},
				'delete': function _delete(key) {
					var index = keys.indexOf(key);
					if (index > -1) {
						keys.splice(index, 1);
						values.splice(index, 1);
					}
				}
			};
		}();

		var createEvent = function createEvent(name) {
			return new Event(name, { bubbles: true });
		};
		try {
			new Event('test');
		} catch (e) {
			// IE does not support `new Event()`
			createEvent = function (name) {
				var evt = document.createEvent('Event');
				evt.initEvent(name, true, false);
				return evt;
			};
		}

		function assign(ta) {
			if (!ta || !ta.nodeName || ta.nodeName !== 'TEXTAREA' || map.has(ta)) return;

			var heightOffset = null;
			var clientWidth = ta.clientWidth;
			var cachedHeight = null;

			function init() {
				var style = window.getComputedStyle(ta, null);

				if (style.resize === 'vertical') {
					ta.style.resize = 'none';
				} else if (style.resize === 'both') {
					ta.style.resize = 'horizontal';
				}

				if (style.boxSizing === 'content-box') {
					heightOffset = -(parseFloat(style.paddingTop) + parseFloat(style.paddingBottom));
				} else {
					heightOffset = parseFloat(style.borderTopWidth) + parseFloat(style.borderBottomWidth);
				}
				// Fix when a textarea is not on document body and heightOffset is Not a Number
				if (isNaN(heightOffset)) {
					heightOffset = 0;
				}

				update();
			}

			function changeOverflow(value) {
				{
					// Chrome/Safari-specific fix:
					// When the textarea y-overflow is hidden, Chrome/Safari do not reflow the text to account for the space
					// made available by removing the scrollbar. The following forces the necessary text reflow.
					var width = ta.style.width;
					ta.style.width = '0px';
					// Force reflow:
					/* jshint ignore:start */
					ta.offsetWidth;
					/* jshint ignore:end */
					ta.style.width = width;
				}

				ta.style.overflowY = value;
			}

			function getParentOverflows(el) {
				var arr = [];

				while (el && el.parentNode && el.parentNode instanceof Element) {
					if (el.parentNode.scrollTop) {
						arr.push({
							node: el.parentNode,
							scrollTop: el.parentNode.scrollTop
						});
					}
					el = el.parentNode;
				}

				return arr;
			}

			function resize() {
				var originalHeight = ta.style.height;
				var overflows = getParentOverflows(ta);
				var docTop = document.documentElement && document.documentElement.scrollTop; // Needed for Mobile IE (ticket #240)

				ta.style.height = 'auto';

				var endHeight = ta.scrollHeight + heightOffset;

				if (ta.scrollHeight === 0) {
					// If the scrollHeight is 0, then the element probably has display:none or is detached from the DOM.
					ta.style.height = originalHeight;
					return;
				}

				ta.style.height = endHeight + 'px';

				// used to check if an update is actually necessary on window.resize
				clientWidth = ta.clientWidth;

				// prevents scroll-position jumping
				overflows.forEach(function (el) {
					el.node.scrollTop = el.scrollTop;
				});

				if (docTop) {
					document.documentElement.scrollTop = docTop;
				}
			}

			function update() {
				resize();

				var styleHeight = Math.round(parseFloat(ta.style.height));
				var computed = window.getComputedStyle(ta, null);

				// Using offsetHeight as a replacement for computed.height in IE, because IE does not account use of border-box
				var actualHeight = computed.boxSizing === 'content-box' ? Math.round(parseFloat(computed.height)) : ta.offsetHeight;

				// The actual height not matching the style height (set via the resize method) indicates that
				// the max-height has been exceeded, in which case the overflow should be allowed.
				if (actualHeight !== styleHeight) {
					if (computed.overflowY === 'hidden') {
						changeOverflow('scroll');
						resize();
						actualHeight = computed.boxSizing === 'content-box' ? Math.round(parseFloat(window.getComputedStyle(ta, null).height)) : ta.offsetHeight;
					}
				} else {
					// Normally keep overflow set to hidden, to avoid flash of scrollbar as the textarea expands.
					if (computed.overflowY !== 'hidden') {
						changeOverflow('hidden');
						resize();
						actualHeight = computed.boxSizing === 'content-box' ? Math.round(parseFloat(window.getComputedStyle(ta, null).height)) : ta.offsetHeight;
					}
				}

				if (cachedHeight !== actualHeight) {
					cachedHeight = actualHeight;
					var evt = createEvent('autosize:resized');
					try {
						ta.dispatchEvent(evt);
					} catch (err) {
						// Firefox will throw an error on dispatchEvent for a detached element
						// https://bugzilla.mozilla.org/show_bug.cgi?id=889376
					}
				}
			}

			var pageResize = function pageResize() {
				if (ta.clientWidth !== clientWidth) {
					update();
				}
			};

			var destroy = function (style) {
				window.removeEventListener('resize', pageResize, false);
				ta.removeEventListener('input', update, false);
				ta.removeEventListener('keyup', update, false);
				ta.removeEventListener('autosize:destroy', destroy, false);
				ta.removeEventListener('autosize:update', update, false);

				Object.keys(style).forEach(function (key) {
					ta.style[key] = style[key];
				});

				map['delete'](ta);
			}.bind(ta, {
				height: ta.style.height,
				resize: ta.style.resize,
				overflowY: ta.style.overflowY,
				overflowX: ta.style.overflowX,
				wordWrap: ta.style.wordWrap
			});

			ta.addEventListener('autosize:destroy', destroy, false);

			// IE9 does not fire onpropertychange or oninput for deletions,
			// so binding to onkeyup to catch most of those events.
			// There is no way that I know of to detect something like 'cut' in IE9.
			if ('onpropertychange' in ta && 'oninput' in ta) {
				ta.addEventListener('keyup', update, false);
			}

			window.addEventListener('resize', pageResize, false);
			ta.addEventListener('input', update, false);
			ta.addEventListener('autosize:update', update, false);
			ta.style.overflowX = 'hidden';
			ta.style.wordWrap = 'break-word';

			map.set(ta, {
				destroy: destroy,
				update: update
			});

			init();
		}

		function destroy(ta) {
			var methods = map.get(ta);
			if (methods) {
				methods.destroy();
			}
		}

		function update(ta) {
			var methods = map.get(ta);
			if (methods) {
				methods.update();
			}
		}

		var autosize = null;

		// Do nothing in Node.js environment and IE8 (or lower)
		if (typeof window === 'undefined' || typeof window.getComputedStyle !== 'function') {
			autosize = function (el) {
				return el;
			};
			autosize.destroy = function (el) {
				return el;
			};
			autosize.update = function (el) {
				return el;
			};
		} else {
			autosize = function (el, options) {
				if (el) {
					Array.prototype.forEach.call(el.length ? el : [el], function (x) {
						return assign(x, options);
					});
				}
				return el;
			};
			autosize.destroy = function (el) {
				if (el) {
					Array.prototype.forEach.call(el.length ? el : [el], destroy);
				}
				return el;
			};
			autosize.update = function (el) {
				if (el) {
					Array.prototype.forEach.call(el.length ? el : [el], update);
				}
				return el;
			};
		}

		module.exports = autosize;
	});
});
System.registerDynamic("github:jackmoore/autosize@3.0.21.js", ["github:jackmoore/autosize@3.0.21/dist/autosize.js"], true, function ($__require, exports, module) {
  var global = this || self,
      GLOBAL = global;
  module.exports = $__require("github:jackmoore/autosize@3.0.21/dist/autosize.js");
});
System.registerDynamic("libs/history.js", [], true, function ($__require, exports, module) {
    var global = this || self,
        GLOBAL = global;
    /**
     * API
     * 
     * let history = new TextHistory()
     * 
     * history.write('hola mundo')
     * history.undo()
     * history.redo()
     * history.current
     * 
     */
    function TextHistory() {
        let o = Object.create(TextHistory.prototype);
        o._pointer = 0;
        o._history = [];
        return o;
    }

    TextHistory.prototype.redo = function () {
        this._pointer < this._history.length - 1 && this._pointer++;
        return this.current;
    };

    TextHistory.prototype.undo = function () {
        this._pointer > 0 && this._pointer--;
        return this.current;
    };

    TextHistory.prototype.write = function (version) {
        if (this._pointer !== this._history.length - 1) {
            this._history = this._history.slice(0, this._pointer);
            this._pointer = this._history.length;
            this._history.push(version);
        }
    };

    Object.defineProperty(TextHistory.prototype, "current", {
        enumerable: true,
        get: function () {
            return this._history[this._pointer];
        }
    });

    module.exports = TextHistory;
});
System.registerDynamic("libs/throttle.js", [], true, function ($__require, exports, module) {
  var global = this || self,
      GLOBAL = global;
  let throttlers = {};

  function throttle(cb, limit, id, immediate = true) {

    if (throttlers[id] != null) {
      throttlers[id](cb);
    } else {
      throttlers[id] = throttler(limit);
      if (immediate) {
        cb();
      }
    }
  }

  function throttler(limit) {
    let canCall = true;

    return cb => {
      if (canCall) {
        cb();
        canCall = false;
        setTimeout(() => canCall = true, limit);
      }
    };
  }

  module.exports = throttle;
});
System.registerDynamic("npm:browser-split@0.0.0/index.js", [], true, function ($__require, exports, module) {
  var global = this || self,
      GLOBAL = global;
  /*!
   * Cross-Browser Split 1.1.1
   * Copyright 2007-2012 Steven Levithan <stevenlevithan.com>
   * Available under the MIT License
   * ECMAScript compliant, uniform cross-browser split method
   */

  /**
   * Splits a string into an array of strings using a regex or string separator. Matches of the
   * separator are not included in the result array. However, if `separator` is a regex that contains
   * capturing groups, backreferences are spliced into the result each time `separator` is matched.
   * Fixes browser bugs compared to the native `String.prototype.split` and can be used reliably
   * cross-browser.
   * @param {String} str String to split.
   * @param {RegExp|String} separator Regex or string to use for separating the string.
   * @param {Number} [limit] Maximum number of items to include in the result array.
   * @returns {Array} Array of substrings.
   * @example
   *
   * // Basic use
   * split('a b c d', ' ');
   * // -> ['a', 'b', 'c', 'd']
   *
   * // With limit
   * split('a b c d', ' ', 2);
   * // -> ['a', 'b']
   *
   * // Backreferences in result array
   * split('..word1 word2..', /([a-z]+)(\d+)/i);
   * // -> ['..', 'word', '1', ' ', 'word', '2', '..']
   */
  module.exports = function split(undef) {

    var nativeSplit = String.prototype.split,
        compliantExecNpcg = /()??/.exec("")[1] === undef,

    // NPCG: nonparticipating capturing group
    self;

    self = function (str, separator, limit) {
      // If `separator` is not a regex, use `nativeSplit`
      if (Object.prototype.toString.call(separator) !== "[object RegExp]") {
        return nativeSplit.call(str, separator, limit);
      }
      var output = [],
          flags = (separator.ignoreCase ? "i" : "") + (separator.multiline ? "m" : "") + (separator.extended ? "x" : "") + ( // Proposed for ES6
      separator.sticky ? "y" : ""),

      // Firefox 3+
      lastLastIndex = 0,

      // Make `global` and avoid `lastIndex` issues by working with a copy
      separator = new RegExp(separator.source, flags + "g"),
          separator2,
          match,
          lastIndex,
          lastLength;
      str += ""; // Type-convert
      if (!compliantExecNpcg) {
        // Doesn't need flags gy, but they don't hurt
        separator2 = new RegExp("^" + separator.source + "$(?!\\s)", flags);
      }
      /* Values for `limit`, per the spec:
       * If undefined: 4294967295 // Math.pow(2, 32) - 1
       * If 0, Infinity, or NaN: 0
       * If positive number: limit = Math.floor(limit); if (limit > 4294967295) limit -= 4294967296;
       * If negative number: 4294967296 - Math.floor(Math.abs(limit))
       * If other: Type-convert, then use the above rules
       */
      limit = limit === undef ? -1 >>> 0 : // Math.pow(2, 32) - 1
      limit >>> 0; // ToUint32(limit)
      while (match = separator.exec(str)) {
        // `separator.lastIndex` is not reliable cross-browser
        lastIndex = match.index + match[0].length;
        if (lastIndex > lastLastIndex) {
          output.push(str.slice(lastLastIndex, match.index));
          // Fix browsers whose `exec` methods don't consistently return `undefined` for
          // nonparticipating capturing groups
          if (!compliantExecNpcg && match.length > 1) {
            match[0].replace(separator2, function () {
              for (var i = 1; i < arguments.length - 2; i++) {
                if (arguments[i] === undef) {
                  match[i] = undef;
                }
              }
            });
          }
          if (match.length > 1 && match.index < str.length) {
            Array.prototype.push.apply(output, match.slice(1));
          }
          lastLength = match[0].length;
          lastLastIndex = lastIndex;
          if (output.length >= limit) {
            break;
          }
        }
        if (separator.lastIndex === match.index) {
          separator.lastIndex++; // Avoid an infinite loop
        }
      }
      if (lastLastIndex === str.length) {
        if (lastLength || !separator.test("")) {
          output.push("");
        }
      } else {
        output.push(str.slice(lastLastIndex));
      }
      return output.length > limit ? output.slice(0, limit) : output;
    };

    return self;
  }();
});
System.registerDynamic("npm:browser-split@0.0.0.js", ["npm:browser-split@0.0.0/index.js"], true, function ($__require, exports, module) {
  var global = this || self,
      GLOBAL = global;
  module.exports = $__require("npm:browser-split@0.0.0/index.js");
});
System.registerDynamic("npm:indexof@0.0.1/index.js", [], true, function ($__require, exports, module) {
  var global = this || self,
      GLOBAL = global;
  /* */

  var indexOf = [].indexOf;

  module.exports = function (arr, obj) {
    if (indexOf) return arr.indexOf(obj);
    for (var i = 0; i < arr.length; ++i) {
      if (arr[i] === obj) return i;
    }
    return -1;
  };
});
System.registerDynamic("npm:indexof@0.0.1.js", ["npm:indexof@0.0.1/index.js"], true, function ($__require, exports, module) {
  var global = this || self,
      GLOBAL = global;
  module.exports = $__require("npm:indexof@0.0.1/index.js");
});
System.registerDynamic("npm:class-list@0.1.1/index.js", ["npm:indexof@0.0.1.js"], true, function ($__require, exports, module) {
    var global = this || self,
        GLOBAL = global;
    // contains, add, remove, toggle
    var indexof = $__require("npm:indexof@0.0.1.js");

    module.exports = ClassList;

    function ClassList(elem) {
        var cl = elem.classList;

        if (cl) {
            return cl;
        }

        var classList = {
            add: add,
            remove: remove,
            contains: contains,
            toggle: toggle,
            toString: $toString,
            length: 0,
            item: item
        };

        return classList;

        function add(token) {
            var list = getTokens();
            if (indexof(list, token) > -1) {
                return;
            }
            list.push(token);
            setTokens(list);
        }

        function remove(token) {
            var list = getTokens(),
                index = indexof(list, token);

            if (index === -1) {
                return;
            }

            list.splice(index, 1);
            setTokens(list);
        }

        function contains(token) {
            return indexof(getTokens(), token) > -1;
        }

        function toggle(token) {
            if (contains(token)) {
                remove(token);
                return false;
            } else {
                add(token);
                return true;
            }
        }

        function $toString() {
            return elem.className;
        }

        function item(index) {
            var tokens = getTokens();
            return tokens[index] || null;
        }

        function getTokens() {
            var className = elem.className;

            return filter(className.split(" "), isTruthy);
        }

        function setTokens(list) {
            var length = list.length;

            elem.className = list.join(" ");
            classList.length = length;

            for (var i = 0; i < list.length; i++) {
                classList[i] = list[i];
            }

            delete list[length];
        }
    }

    function filter(arr, fn) {
        var ret = [];
        for (var i = 0; i < arr.length; i++) {
            if (fn(arr[i])) ret.push(arr[i]);
        }
        return ret;
    }

    function isTruthy(value) {
        return !!value;
    }
});
System.registerDynamic("npm:class-list@0.1.1.js", ["npm:class-list@0.1.1/index.js"], true, function ($__require, exports, module) {
  var global = this || self,
      GLOBAL = global;
  module.exports = $__require("npm:class-list@0.1.1/index.js");
});
System.registerDynamic('npm:hyperscript@2.0.2/index.js', ['npm:browser-split@0.0.0.js', 'npm:class-list@0.1.1.js', '@empty'], true, function ($__require, exports, module) {
  var global = this || self,
      GLOBAL = global;
  /* */
  var split = $__require('npm:browser-split@0.0.0.js');
  var ClassList = $__require('npm:class-list@0.1.1.js');

  var w = typeof window === 'undefined' ? $__require('@empty') : window;
  var document = w.document;
  var Text = w.Text;

  function context() {

    var cleanupFuncs = [];

    function h() {
      var args = [].slice.call(arguments),
          e = null;
      function item(l) {
        var r;
        function parseClass(string) {
          // Our minimal parser doesn’t understand escaping CSS special
          // characters like `#`. Don’t use them. More reading:
          // https://mathiasbynens.be/notes/css-escapes .

          var m = split(string, /([\.#]?[^\s#.]+)/);
          if (/^\.|#/.test(m[1])) e = document.createElement('div');
          forEach(m, function (v) {
            var s = v.substring(1, v.length);
            if (!v) return;
            if (!e) e = document.createElement(v);else if (v[0] === '.') ClassList(e).add(s);else if (v[0] === '#') e.setAttribute('id', s);
          });
        }

        if (l == null) ;else if ('string' === typeof l) {
          if (!e) parseClass(l);else e.appendChild(r = document.createTextNode(l));
        } else if ('number' === typeof l || 'boolean' === typeof l || l instanceof Date || l instanceof RegExp) {
          e.appendChild(r = document.createTextNode(l.toString()));
        }
        //there might be a better way to handle this...
        else if (isArray(l)) forEach(l, item);else if (isNode(l)) e.appendChild(r = l);else if (l instanceof Text) e.appendChild(r = l);else if ('object' === typeof l) {
            for (var k in l) {
              if ('function' === typeof l[k]) {
                if (/^on\w+/.test(k)) {
                  (function (k, l) {
                    // capture k, l in the closure
                    if (e.addEventListener) {
                      e.addEventListener(k.substring(2), l[k], false);
                      cleanupFuncs.push(function () {
                        e.removeEventListener(k.substring(2), l[k], false);
                      });
                    } else {
                      e.attachEvent(k, l[k]);
                      cleanupFuncs.push(function () {
                        e.detachEvent(k, l[k]);
                      });
                    }
                  })(k, l);
                } else {
                  // observable
                  e[k] = l[k]();
                  cleanupFuncs.push(l[k](function (v) {
                    e[k] = v;
                  }));
                }
              } else if (k === 'style') {
                if ('string' === typeof l[k]) {
                  e.style.cssText = l[k];
                } else {
                  for (var s in l[k]) (function (s, v) {
                    if ('function' === typeof v) {
                      // observable
                      e.style.setProperty(s, v());
                      cleanupFuncs.push(v(function (val) {
                        e.style.setProperty(s, val);
                      }));
                    } else var match = l[k][s].match(/(.*)\W+!important\W*$/);
                    if (match) {
                      e.style.setProperty(s, match[1], 'important');
                    } else {
                      e.style.setProperty(s, l[k][s]);
                    }
                  })(s, l[k][s]);
                }
              } else if (k === 'attrs') {
                for (var v in l[k]) {
                  e.setAttribute(v, l[k][v]);
                }
              } else if (k.substr(0, 5) === "data-") {
                e.setAttribute(k, l[k]);
              } else {
                e[k] = l[k];
              }
            }
          } else if ('function' === typeof l) {
            //assume it's an observable!
            var v = l();
            e.appendChild(r = isNode(v) ? v : document.createTextNode(v));

            cleanupFuncs.push(l(function (v) {
              if (isNode(v) && r.parentElement) r.parentElement.replaceChild(v, r), r = v;else r.textContent = v;
            }));
          }

        return r;
      }
      while (args.length) item(args.shift());

      return e;
    }

    h.cleanup = function () {
      for (var i = 0; i < cleanupFuncs.length; i++) {
        cleanupFuncs[i]();
      }
      cleanupFuncs.length = 0;
    };

    return h;
  }

  var h = module.exports = context();
  h.context = context;

  function isNode(el) {
    return el && el.nodeName && el.nodeType;
  }

  function forEach(arr, fn) {
    if (arr.forEach) return arr.forEach(fn);
    for (var i = 0; i < arr.length; i++) fn(arr[i], i);
  }

  function isArray(arr) {
    return Object.prototype.toString.call(arr) == '[object Array]';
  }
});
System.registerDynamic("npm:hyperscript@2.0.2.js", ["npm:hyperscript@2.0.2/index.js"], true, function ($__require, exports, module) {
  var global = this || self,
      GLOBAL = global;
  module.exports = $__require("npm:hyperscript@2.0.2/index.js");
});