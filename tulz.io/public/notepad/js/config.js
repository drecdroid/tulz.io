System.config({
  baseURL: "js/",
  defaultJSExtensions: true,
  transpiler: false,
  paths: {
    "npm:*": "jspm_packages/npm/*",
    "github:*": "jspm_packages/github/*"
  },
  bundles: {
    "deps.js": [
      "npm:hyperscript@2.0.2.js",
      "npm:hyperscript@2.0.2/index.js",
      "npm:class-list@0.1.1.js",
      "npm:class-list@0.1.1/index.js",
      "npm:indexof@0.0.1.js",
      "npm:indexof@0.0.1/index.js",
      "npm:browser-split@0.0.0.js",
      "npm:browser-split@0.0.0/index.js",
      "libs/throttle.js",
      "libs/history.js"
    ]
  },

  map: {
    "autosize": "github:jackmoore/autosize@3.0.21",
    "file-saver": "npm:file-saver@1.3.3",
    "hyperscript": "npm:hyperscript@2.0.2",
    "npm:class-list@0.1.1": {
      "indexof": "npm:indexof@0.0.1"
    },
    "npm:hyperscript@2.0.2": {
      "browser-split": "npm:browser-split@0.0.0",
      "class-list": "npm:class-list@0.1.1"
    }
  }
});
