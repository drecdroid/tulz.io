
let h = require('hyperscript')
let _History = require('../libs/history')
//let autosize = require('autosize')
let throttle = require('../libs/throttle')

function main(){
    
    
    let fontSize = 13;
    let lineHeight = 18;

    let textarea = document.querySelector('#document-editor textarea')
    let doc_line_numbers = document.querySelector('#document-line-numbers')
    let doc_status = document.querySelector('#document-status')
    let doc_title = document.querySelector('#document-name')
    //autosize(textarea)
    
    let div = document.createElement('div')
                    
    Object.assign(div.style, {
        position: 'fixed',
        overflowWrap : 'break-word',
        bottom: '-1000px',
        right: '-1000px',
        whiteSpace : 'pre-wrap',
        margin: 0,
        width: `${textarea.clientWidth}px`,
        lineHeight: `${lineHeight}px`,
        fontSize: `${fontSize}px`,
        fontFamily: "monospace",
    })


    document.body.appendChild(div)
    let line_numbers = (e)=>{
        throttle(()=>{
            doc_line_numbers
            .innerText = 
            textarea.value.split('\n')
            .map((line, i)=>{
                let render_lines = 1
                if(line.length){
                    div.innerText = line
                    render_lines = Math.floor(div.clientHeight/lineHeight)
                }
                return `${i+1}${'\n'.repeat(render_lines)}`
            }).join('')
        }, 40, 'print', true)
    }

    textarea.addEventListener('input', line_numbers )
    textarea.addEventListener('change', line_numbers )
    textarea.addEventListener('keydown', (e)=>{
        if(e.keyCode === 9 && !e.shiftKey){
            e.preventDefault()
            let s = textarea.selectionStart
            textarea.value = textarea.value.slice(0,textarea.selectionStart)+'   '+textarea.value.slice(textarea.selectionStart)
            textarea.selectionStart = s + 3
            textarea.selectionEnd = textarea.selectionStart
        }

        if(e.keyCode === 9 && e.shiftKey){
            e.preventDefault()
        }
    })

    let caret_position = (e)=>{
        let row
        let column 
        
        throttle(()=>{
            let until_caret = textarea.value.substr(0,textarea.selectionStart).split('\n')

            column = until_caret.pop().length+1

            row = until_caret.length + 1

            doc_status
            .querySelector('span')
            .innerText = `Line ${row} / Column ${column}`
            
        }, 30, 'caret_positon', true)

        /* if(row){
            console.log(`scrollY: ${window.scrollY}, caretY: ${textarea.offsetTop + row*lineHeight}`)
            console.log(doc_status.offsetTop - (textarea.offsetTop + row*lineHeight- window.scrollY))
            let dist_bottom = doc_status.offsetTop - (textarea.offsetTop + row*lineHeight- window.scrollY)
            if(dist_bottom <= lineHeight){
                scrollTo(0, window.scrollY+lineHeight*2)
            }
            
        }*/
    }

    textarea.addEventListener('keydown', caret_position)
    textarea.addEventListener('keyup', caret_position)

    caret_position()


    document.querySelectorAll('.change-color').forEach(e=>{
        e.style.backgroundColor = e.dataset['color']
    })

    document
    .addEventListener('click', (e)=>{
        let widget = document.querySelector('#change-color-widget')
        
        if(widget.style.opacity === "100" && !e.target.classList.contains('change-color')){
            widget.style.opacity = "0"
        }
        else
        if(widget.style.opacity === "0" && e.target.id === "change-color-button"){
            widget.style.opacity = "100"
        }
    })

    let change_color = (e)=>{
        if(e.target.classList.contains('change-color')){
            let color = e.target.dataset['color']
            document.body.style.backgroundColor = color
            document.querySelector('#change-color-button').style.backgroundColor = color
            document.querySelector('#document-name').style.color = color

            /*document
            .querySelectorAll('#document-controls *')
            .forEach(el=>{
                el
                .style
                .color = color
            })*/

            document
            .querySelector('#document-controls')
            .style
            .color = color
        }
        else
        if(e.target.parentElement.id === 'document-controls'){
            if(e.target.classList.contains('button-new')){
                if(confirm('You will loose your current changes')){
                    textarea.value = ""
                    line_numbers()
                }
            }

            else
            if(e.target.classList.contains('button-undo')){
                document.execCommand('undo')
            }

            else
            if(e.target.classList.contains('button-redo')){
                document.execCommand('redo')
            }

            else
            if(e.target.classList.contains('button-open')){
                document.getElementById('file-open').click()
            }

            else
            if(e.target.classList.contains('button-save')){
                let link = document.createElement('a')
                link.download = doc_title.value
                let data = `data:text/plain;charset=utf-8,${textarea.value}`
                link.href = data
                link.click()
            }

            else
            if(e.target.classList.contains('button-print')){
                print()
            }

            else
            if(e.target.classList.contains('button-link')){
                console.log(firebase)
            }
        }
    }
    
    addEventListener('click', change_color, true)
    document.querySelector('.change-color').click()
    
    let set_textarea_size = function(){
        let textarea_height = innerHeight-document.querySelector('#document-editor').offsetTop-doc_status.clientHeight
        textarea.style.minHeight = `${textarea_height}px`;
        textarea.style.maxHeight = `${textarea_height}px`;
    }

    set_textarea_size()
    onresize = set_textarea_size

    textarea
    .addEventListener('scroll', (e)=>{
        doc_line_numbers
        .style
        .top = `-${textarea.scrollTop}px`
    })

    document
    .getElementById('file-open')
    .addEventListener('change', (e)=>{
        let fr = new FileReader()
        fr.readAsText(e.target.files[0])
        fr.addEventListener('loadend', ()=>{
            console.log(e.target.files[0])
            document
            .getElementById('document-name')
            .value = e.target.files[0].name
            textarea.value = fr.result
            line_numbers()
            //autosize.update(textarea)
        })
    })

    setInterval((e)=>{
        localStorage.setItem('text', textarea.value)
        localStorage.setItem('title', doc_title.value)
    }, 1000)

    doc_title.value = localStorage.getItem('title')
    textarea.value = localStorage.getItem('text')

    caret_position()
    line_numbers()
}

main()