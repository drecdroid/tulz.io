function format_digits(num, digits){         
    var num_str = num.toString()              
    var to_add = digits - num_str.length     
    to_add = to_add > 0 ? to_add : 0          

    return `${"0".repeat(to_add)}${num}`    

}

function format_time({h, m, s, ms}){
    return `${format_digits(h, 2)}:${format_digits(m, 2)}:${format_digits(s, 2)}.${format_digits(ms, 3)}`
}


function shallowCopy(obj){
    return Object.assign({}, obj)
}

function createTimer(h, m, s, ms){
    let obj = {}
    obj.initialTime = { h, m, s, ms }
    obj.actualTime = shallowCopy(obj.initialTime)

    obj.reset = function(){
        obj.actualTime = shallowCopy(obj.initialTime)
        obj.pause()
        obj.emit("reseted")
     
    }

    obj.tick = function(){
        let actual = obj.actualTime
        let now = Date.now()
        let elapsed = now-obj.prevTime
        obj.prevTime = now

        actual.ms += elapsed
        if(actual.ms > 999){
            actual.ms = 0
            actual.s += 1
        }

        if(actual.s > 59){
            actual.s = 0
            actual.m += 1
        }

        if(actual.m > 59){
            actual.m = 0
            actual.h += 1
        }
        obj.emit("updated")

        requestAnimationFrame(obj.tick)
    }

    obj.init = function (){
        if(obj.timer == null){
            //obj.timer = setInterval(obj.tick, 11)
            obj.prevTime = Date.now()
            obj.tick()
        }
    }

    obj.pause = function(){
        clearInterval(obj.timer)
        obj.timer = null
        obj.emit("paused")
    }

    obj.resume = function(){
        obj.init()
        obj.emit("resumed")
    }

    obj.suscribers = []

    obj.emit = function(event){
        obj
        .suscribers
        .forEach(fn=>{
            fn(event, obj)
        })
    }

    obj.subscribe = function(fn){
        obj.suscribers.push(fn)
            obj.emit('subscribed')
    }

    obj.toString = function(){
        let actual = obj.actualTime
        return format_time(actual)
    }

    obj.emit("created")

    return obj
}


function mostrar(event, obj){
    if(event === 'updated'){
        let time = obj.actualTime  
        console.log(format_time(time))
    }
    if(event === 'paused'){
        console.log('paused')
    }
    if(event === 'reseted'){
        console.log('reseted')
    }
    if(event === 'resumed'){
        console.log('resumed')
    }
    if(event === 'finished'){
        console.log('finished')
    }

}

function mostrarDOM(event, obj){
    
    if(event === 'updated' || event ==='subscribed'){
        let time = obj.actualTime
        document.getElementById('hours').innerText = format_digits(obj.actualTime.h, 2)
        document.getElementById('minutes').innerText = format_digits(obj.actualTime.m, 2)
        document.getElementById('seconds').innerText = format_digits(obj.actualTime.s, 2)
        document.getElementById('miliseconds').innerText = format_digits(obj.actualTime.ms, 3)
    }
    
    if(event === 'reseted'){
        document.getElementById('hours').innerText = "00"
        document.getElementById('minutes').innerText = "00"
        document.getElementById('seconds').innerText = "00"
        document.getElementById('miliseconds').innerText = "000"
    }

}

let timer = createTimer(0, 0, 0, 0)
timer.subscribe(mostrarDOM)




let onclick_run = function(){
    timer.init()
}

let onclick_stop = function(){
    timer.pause()
}

let onclick_reset = function(){
    timer.reset()
}


document
.getElementById('btn-run')
.addEventListener('click', onclick_run)

document
.getElementById('btn-stop')
.addEventListener('click', onclick_stop)

document
.getElementById('btn-reset')
.addEventListener('click', onclick_reset)


