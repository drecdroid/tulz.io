function count_lines(e){
    let input = document.getElementById('lines-input')
    let output = document.getElementById('line-count')

    let text = input.value
    
    let lines = text.match(/(\r\n|\n|\r)?$/gm).length
    output.innerText = `${lines} ${lines>1?"lines":"line"}`
}

document
.getElementById('count-lines-button')
.addEventListener('click', count_lines)

document
.getElementById('lines-input')
.addEventListener('input', count_lines)

count_lines()