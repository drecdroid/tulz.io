"use strict";
/**
 * TODO:
 * - Arrow keys page event
 * - Page arrow distribution
 * - Char categories filter
 * - Color scheme
 * - Black and white
 * - Search by name
 * - Show character name and category
 * - Filter by category
 * - UTF-16 BE / LE
 * - UTF-32 BE / LE
 */
class CharProperty {
    constructor(name, repr){        
        let tr = document.createElement('tr')
        tr.className="char-property"
        tr.innerHTML = 
`<td class="char-prop-name">${name}</td>
<td class="char-code"></td>`

        this.char_code = tr.querySelector(".char-code")

        this.root = tr
        this.repr = repr
    }

    set value(code){
        this.char_code.innerText = this.repr(code)
    }
}

class CharInfo {
    constructor(){
        this.root = document.createElement('div')
        this.root.className = 'popup'
        this.root.classList.toggle('hidden', true)
        this.root.innerHTML = `
            <div class="popup-content">
                <div class="bar bar-popup"><div class="close-button fa fa-times bar-button"></div></div>
                
                <div class="character-big"></div>
                <table class="char-properties">
                </table>
                <p class="copy-value">Copy by double clicking on the character or its codes</p>
            </div>
            `
        this.character = this.root.querySelector('.character-big')

        this.properties = [
            new CharProperty('Decimal code point', (c)=>c),
            new CharProperty('Hex code point', (c)=>hex_format(c)),
            new CharProperty('Octal code point', (c)=>{
                return c.toString(8).length % 2 ? `0${c.toString(8)}`: c.toString(8)
            }),
            new CharProperty('Unicode', (c)=>`U+${unicodeCode(c)}`),
            new CharProperty('Javascript Escaped', (c)=>{
                let unicodeCodepoint = unicodeCode(c)
                return `\\u${c<=0xFFFF?unicodeCodepoint: `\{${unicodeCodepoint}\}`}`
            }),
            new CharProperty('UTF-8 bytes', (c)=>{
                return codepoint2UTF8(c).map(e=>hex_format(e).toUpperCase()).join(' ')
            }),
            new CharProperty('UTF-8 bytes LE', (c)=>{
                return codepoint2UTF8(c).reverse().map(e=>hex_format(e).toUpperCase()).join(' ')
            }),
            /*new CharProperty('UTF-16 bytes', (c)=>),
            new CharProperty('UTF-16 bytes LE', (c)=>),*/
            new CharProperty('HTML Entity', (c)=>`&#${c};`),
            new CharProperty('HTML Entity Hex', (c)=>`&#x${hex_format(c)};`),
        ]
        
        let properties_el = this.root.querySelector(".char-properties")
        
        this.properties.forEach(prop=>{
            properties_el.appendChild(prop.root)
        })

        /** COPY ON CLICK */
        this.root.addEventListener('dblclick', (e)=>{
            if(e.target.classList.contains('char-code') || e.target.classList.contains('character-big')){
                let input = document.createElement('input')
                Object.assign(input.style, {
                    position: 'fixed',
                    bottom: '-100px',
                    right: '-100px'
                })
                
                document.body.appendChild(input)
                input.type = 'text'
                let copied = input.value = e.target.innerText
                input.focus()
                input.select()
                
                document.execCommand('Copy');

                document
                .querySelector('.copy-value')
                .innerText = `Just copied: ${copied}`
                input.remove()
            }
        })

        this.root
        .querySelector('.close-button')
        .addEventListener('click',(e)=>{
            window.dispatchEvent(new CustomEvent('close-menu'))
        })
    }

    set value(code){
        this.character.innerText = String.fromCodePoint(code)
        this.properties.forEach(prop=>{
          prop.value = code  
        })
    }
}

class CharBox {
    constructor(charcode){
        this.root = document.createElement('div')
        this.root.className = 'char-box'
        
        this.root.addEventListener('click', (e)=>{
            
            /*
            history.pushState(null, null, `/charmap/char/unicode/${unicodeCode(charcode)}`)
            char_popup(charcode)*/
            window.dispatchEvent(new CustomEvent('view-char', {
                detail : {
                    charcode : this.charcode
                }
            }))

            e.stopPropagation();
        })
    }

    set charcode(charcode){
        this._charcode = charcode
        this.root.dataset['charcode'] = charcode
        this.root.innerText = String.fromCodePoint(charcode)
        this.root.title = `${charcode} | 0x${charcode.toString(16)}`
        this.root.id = `char-${charcode}`
    }

    get charcode(){
        return this._charcode
    }

    set active(toggle){
        this.root.classList.toggle('active', toggle)
    }
}

class CharMap {
    /**
     * @param {number} chars_per_page 
     */
    constructor(chars_per_page){
        this.chars_per_page = chars_per_page
        this.root = document.createElement('div')
        this.root.className = 'charmap'

        this.boxes = range(chars_per_page).map(c=>new CharBox())
        this.boxes.forEach(box => this.root.appendChild(box.root))

        this.go_page(0)
        this.active_char = 0
        this.active_box = this.boxes[0]
    }

    view_active(){
        this.go_page(Math.floor(this.active_char/this.chars_per_page))
    }

    select_char(char){
        this.active_char = char
        this.view_active()
        this.update_selected()
    }

    update_selected(){
        this.deselect()
        if(Math.floor(this.active_char/this.chars_per_page) === this.page){
            this.active_box = this.boxes[this.active_char % this.chars_per_page]
            this.active_box.active = true
        }
    }
    deselect(){
        this.active_box != null && (this.active_box.active = false)
    }

    move_page_by(offset){
        if(this.page+offset<=0){
            this.go_page(0)
        }else{
            this.go_page(this.page+offset)
        }

    }

    go_page(page){
        if(page>=0){
            this.page = page
            this.boxes
            .forEach((box, i) => {
                box.charcode = this.page*this.chars_per_page + i
            })
        }

        window.dispatchEvent(new CustomEvent('page-changed', {
            detail: {
                page : this.page,
                chars_per_page : this.chars_per_page,
            }
        }))
    }
}

/**
 * Returns an array with the numbers from `m` inclusive to `n`exlusive
 * 
 * @param {number} m 
 * @param {number} n
 * 
 * @return {number[]}
 */
function range(m, n) {
    if(n==null){
        n=m
        m=0
    }
    let ret = Array(n-m)
    for(let i=n-m-1; i>=0; i--){
        ret[i] = m+i
    }
    return ret
}

function hex_format(val){
    let hex_str = val.toString(16)
    return `${"0".repeat(hex_str.length % 2)}${hex_str}`.toUpperCase()
}

/**
 * Evaluates if a number is or is between others(inclusive)
 * Must be bound to a value
 * 
 * @param {number} m 
 * @param {number} n 
 */
function between(m,n){ return (this >= m && this <= n) }

/**
 * Masks a group of bits leaving `offset` bits from the right and returning the next `q`bits
 * 
 * @param {number} q 
 * @param {number} offset 
 */
function mask_bytes(q, offset){
    return (this & ((1<<q)-1) << offset) >> offset
}

/**
 * Returns an array of numbers representing each byte of an UTF-8 big endian representation of the codepoint
 * @param {number} codepoint 
 */
function codepoint2UTF8(codepoint){
    let cp_between = between.bind(codepoint)

    function utf_8_rule_apply(codepoint, bytes){
        let ret = [ ((Math.pow(2, (bytes)-1)) << (8-bytes)) | codepoint >> 6*(bytes-1)]
        let mask = mask_bytes.bind(codepoint)

        return ret
        .concat(
            range(bytes-1)
            .reverse()
            .map(b => 1<<7 | mask(6, b*6))
        )
    }

    if(cp_between(0x00000000, 0x0000007F)){
        return [codepoint]
    }else
    if(cp_between(0x00000080, 0x000007FF)){
        return utf_8_rule_apply(codepoint, 2)
    }else
    if(cp_between(0x00000800, 0x0000FFFF)){
        return utf_8_rule_apply(codepoint, 3)
    }else
    if(cp_between(0x00010000, 0x001FFFFF)){
        return utf_8_rule_apply(codepoint, 4)
    }else
    if(cp_between(0x00200000, 0x03FFFFFF)){
        return utf_8_rule_apply(codepoint, 5)
    }else
    if(cp_between(0x04000000, 0x7FFFFFFF)){
        return utf_8_rule_apply(codepoint, 6)
    }
}

function unicodeCode(codepoint){
    if(codepoint <= 0xFFFF){
        let partial_code = codepoint.toString(16).toUpperCase()
        let zeroes = 4-partial_code.length
        return `${'0'.repeat(zeroes)}${partial_code}`
    }
    else{
        return `${codepoint.toString(16).toUpperCase()}`
    }
}

function codepoint2UTF16(codepoint){
    let bytes_str = hex_format(codepoint)
    let len_bytes_repr = bytes_str.length/2

    let res = Array(len_bytes_repr)

    for(let i=len_bytes_repr-1; i >=0 ; i--){
        res[i] = bytes_str.slice(i*2, i*2+2)
    }
    
    return codepoint > 0xFF ? res : ['00'].concat(res)
}

function searchCharacter(e){
    e.preventDefault()
    let enc = document.querySelector('.char-encoding')
    let character = document.querySelector('.char-input input').value
    let utf8_decoder = new TextDecoder('utf-8')
    let code
    if(!character) return;
    switch(enc.value){
        case 'char':
            code = character.codePointAt(0)
            break;
        case 'dec':
            code = Number.parseInt(character)
            break;
        case 'hex':
            code = Number.parseInt(character, 16)
            break;
        case 'oct':
            code = Number.parseInt(character, 8)
            break;
        case 'uni':
            if(!character.startsWith('U+')) return;
            code = Number.parseInt(character.substr(2), 16)
            break;
        case 'js':
            if(!character.startsWith('\\u')) return;
            code = character <= 0xFFFF ? Number.parseInt(character.substr(2), 16) : Number.parseInt(character.match(/{(.*)}/)[1], 16)
            //s_input.placeholder = `e.g. \\u{1F601}`
            break;
        case 'utf8be':
            var arr = new Uint8Array(character.trim().split(/\s+/).map(c=>Number.parseInt(c, 16)))
            code = utf8_decoder.decode(arr).codePointAt(0)
            break;
        case 'utf8le':
            var arr = new Uint8Array(character.trim().split(/\s+/).map(c=>Number.parseInt(c, 16)))
            code = utf8_decoder.decode(arr.reverse()).codePointAt(0)
            break;
        case 'html':
            if(!character.startsWith('&#')) return;
            code = Number.parseInt(character.substr(2), 16)
            break;
        case 'htmlhex':
            if(!character.startsWith('&#')) return;
            code = Number.parseInt(character.substr(2), 16)
            break;
    }
    
    dispatchEvent(new CustomEvent('view-char', {
        detail : {
            charcode : code
        }
    }))

    
    e.stopPropagation()
}

function main(){
    let charmap = new CharMap(500)
    let popup = new CharInfo()
    let bar = document.querySelector('.nav-bar')
    let enc = document.querySelector('.char-encoding')
    let s_input = document.querySelector('.char-input input')
    let header = document.querySelector('.header')

    document.body.appendChild(charmap.root)
    document.body.appendChild(popup.root)

    /** PAGE PREV */
    document
    .querySelector('.page-prev')
    .addEventListener('click', (e)=>{
        charmap.move_page_by(-1)
    })

    /** PAGE NEXT */
    document
    .querySelector('.page-next')
    .addEventListener('click', (e)=>{
        charmap.move_page_by(1)
    })

    /** PAGE PREV FAST */
    document
    .querySelector('.page-prev-fast')
    .addEventListener('click', (e)=>{
        charmap.move_page_by(-10)
    })

    /** PAGE NEXT FAST */
    document
    .querySelector('.page-next-fast')
    .addEventListener('click', (e)=>{
        charmap.move_page_by(10)
    })

    /** SEARCH */
    document
    .querySelector('.search-button')
    .addEventListener('click', searchCharacter)

    document
    .querySelector('.char-input')
    .addEventListener('submit', searchCharacter)

    /** BAR FOLLOW SCROLL */
    document
    .addEventListener('scroll', (e)=>{
        if(header.offsetTop + header.clientHeight <= window.scrollY){
            Object.assign(bar.style, {
                position : "fixed",
                top : "0px",
                width : `${document.body.clientWidth}px`,
            })
        }

        if(header.offsetTop + header.clientHeight - bar.clientHeight > window.scrollY)
        {
            Object.assign(bar.style, {
                position : "initial",
                top : "initial",
                width : "initial"
            })
        }
    })

    /** CLOSE MENU WITH ESC KEY */
    document.addEventListener('keyup', (e)=>{
        if(e.keyCode === 27){
            window.dispatchEvent(new CustomEvent('close-menu'))
        }
    })

    /** CLOSE WHEN CLICKING OUTSIDE MENU */
    document
    .body
    .addEventListener('click', (e)=>{
        if(e.clientX > popup.root.clientWidth && window.innerWidth <= 640){
            window.dispatchEvent(new CustomEvent('close-menu'))
        }
    }, false)

    /** */
    enc
    .addEventListener('change', (e)=>{
        switch(e.target.value){
            case 'char':
                s_input.placeholder = `e.g. 😁`
                break;
            case 'dec':
                s_input.placeholder = `e.g. 128513`
                break;
            case 'hex':
                s_input.placeholder = `e.g. 01F601`
                break;
            case 'oct':
                s_input.placeholder = `e.g. 373001`
                break;
            case 'uni':
                s_input.placeholder = `e.g. U+1F601`
                break;
            case 'js':
                s_input.placeholder = `e.g. \\u{1F601}`
                break;
            case 'utf8be':
                s_input.placeholder = `e.g. F0 9F 98 81`
                break;
            case 'utf8le':
                s_input.placeholder = `e.g. 81 98 9F F0`
                break;
            case 'html':
                s_input.placeholder = `e.g. &#128513;`
                break;
            case 'htmlhex':
                s_input.placeholder = `e.g. &#x01F601;`
                break;                
            default:
                s_input.placeholder = `e.g. 128513`
                break;
        }
    })
    enc.dispatchEvent(new Event('change'))

    window.addEventListener('view-char', (e)=>{
        let charcode = e.detail.charcode
        
        popup.root.classList.toggle('hidden', false)
        popup.root.scrollY = 0
        popup.value = charcode

        charmap.select_char(charcode)
        
        history.replaceState(null, null, `/charmap/char/unicode/U+${unicodeCode(charcode)}`)

        document.body.classList.toggle('menu-open', true)
        document.body.classList.toggle('blocked', true)
    })

    window.addEventListener('close-menu', (e)=>{
        /**
         * - deselect any charbox
         * - reset location pathname
         * - close menu
         */
        charmap.deselect()
        history.replaceState(null, null, '/')
        document.body.classList.toggle('blocked', false)
        document.body.classList.toggle('menu-open', false)
        popup.root.classList.toggle('hidden', true)
    })

    window.addEventListener('page-changed', (e)=>{
        let page = e.detail.page
        let chars_per_page = e.detail.chars_per_page
        charmap.update_selected()
        bar
        .querySelector('.page-info')
        .innerText = `${page*chars_per_page} - ${chars_per_page*(page+1)-1}`
    })

    charmap.go_page(0)
    if(window.location.pathname.startsWith('/charmap/char/unicode/')){
        let last_backslash = window.location.pathname.lastIndexOf('/')
        let charcode = Number.parseInt(window.location.pathname.slice(last_backslash+1+2), 16)
        
        window.dispatchEvent(new CustomEvent('view-char', {
            detail : {
                charcode
            }
        }))
    }
}

main()

/*if(window.location.pathname.startsWith('/charmap/char/unicode/')){
    let last_backslash = window.location.pathname.lastIndexOf('/')
    let code = Number.parseInt(window.location.pathname.slice(last_backslash+1+2), 16)
    char_popup(code)
    let chars_per_page = Number(charmap.dataset['chars'])
    draw_characters(Math.floor(code/chars_per_page), chars_per_page)
    document.getElementById(`char-${code}`).classList.toggle('active', true)
}

*/

