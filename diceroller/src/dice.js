const Random = require('random-js')

const random = new Random(Random.engines.browserCrypto)

/**
 * Generates a number for a dice with {faces} faces
 * 
 * @param {number} faces
 * @returns {number}
 */
function diceGenerate(faces){
    return random.integer(1, faces)
}

function rollDice(notation){
    let dice = parseDiceNotation(notation)

    return Array(dice.times)
    .fill(0)
    .map(()=>diceGenerate(dice.faces)+dice.modifier)
}

class ParseError extends Error {
  constructor(position) {
    super(`Error at position ${position}`);
    this.name = this.constructor.name;
    if (typeof Error.captureStackTrace === 'function') {
      Error.captureStackTrace(this, this.constructor);
    } else { 
      this.stack = (new Error(message)).stack; 
    }
  }
}    

/**
 * 
 * @param {string} notation
 */
function parseDiceNotation(notation){
    let d_i = notation.indexOf('d')
    if(d_i !== -1){
        let times = 1
        let modifier = 0

        if(d_i !== 0){
            times = Number.parseInt(notation.slice(0, d_i))

            if(isNaN(times)){
                throw new ParseError(0)
            }

            times = Math.abs(times)
        }
        
        let m_i = notation.search(/[+-]/)

        let faces = Number.parseInt(m_i>=d_i+2 ? notation.slice(d_i+1, m_i) : notation.slice(d_i+1))
        
        if(isNaN(faces)){
            throw new ParseError(d_i+1)
        }        

        if(m_i >= d_i+2){
            
            modifier = Number.parseInt(notation.slice(m_i))

            if(isNaN(modifier)){
                throw new ParseError(m_i)
            }
        }
        else if(m_i !== -1){
            throw new ParseError(m_i)
        }

        return {
            times,
            faces,
            modifier
        }
    }else{
        throw new ParseError(0)
    }
}


module.exports = {
    rollDice,
    parseDiceNotation
}