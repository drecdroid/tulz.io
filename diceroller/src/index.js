const h = require('hyperscript')
const moment = require('moment')

//named imports
let { rollDice, parseDiceNotation } = require('./dice')

function resultRow(id, date, rule, result){
    return h('tr',
        h('td', id),
        h('td', date),
        h('td', rule),
        h('td', JSON.stringify(result)),
        h('td', result.reduce((pv, cv) => pv+cv, 0)),
        h('td', Math.max(...result)),
        h('td', Math.min(...result))
    )
}

document.addEventListener('DOMContentLoaded', main)

function main(){
    let prev_rows = localStorage.getItem('rolls')
    let rolls = prev_rows ? JSON.parse(prev_rows) : []

    rolls.forEach(roll=>{
        let result_row = resultRow(roll.id, roll.date, roll.rule, roll.result)
        let tbody = document.querySelector('.rolls-table tbody')
        tbody.insertBefore(result_row, tbody.firstChild)
    })
    
    function dice_roll(e){
        e.preventDefault()
        
        let rule = document.querySelector('.dice-input-notation').value

        if(rule){
            let result = rollDice(rule)
            let tbody = document.querySelector('.rolls-table tbody')
            
            let id = tbody.children.length + 1
            let date = moment().calendar()
            let result_row = resultRow(id, date, rule, result)
            
            tbody.insertBefore(result_row, tbody.firstChild)
            
            rolls.push({
                id,
                date,
                rule,
                result
            })

            localStorage.setItem('rolls', JSON.stringify(rolls))
        }
    }

    document
    .querySelector('.dice-roll')
    .addEventListener('click', dice_roll)

    document
    .querySelector('.dice-input')
    .addEventListener('submit', dice_roll)

    document
    .querySelector('.clear-rolls')
    .addEventListener('click', (e) => {
        let tbody = document.querySelector('.rolls-table tbody')
        tbody.innerHTML = ""

        localStorage.clear()
    })
}
