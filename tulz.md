#Colores : 
https://coolors.co/ffffff-00c3c6-003c49-ffc744-111111

# Tulz

## Tools Sites
- http://www.online-toolz.com
- http://www.webtoolkitonline.com
- https://www.freeformatter.com
- https://codebeautify.org/
- https://www.tools4noobs.com
- http://www.miniwebtool.com/

## Ideas
- guitar tuner
- audio converter
- pdf converter
- video converter
- xml / html / json formatter(beautifier)
- xml / html / json minificator(uglifier)
- seconds to minutes

## Public APIs Ideas
- url multi analyzer
- dictionary with Pearson data

## To Do
- unit converter
- ideal weight calculator
- days until
- wasted on lol
- age calculator
- calories calculator
- grade calculator
- link checker
- keyword density tool
- text tools
- pomodoro
- box-shadow generator
- gradient generator
- currency converter

## Backlog

### JSON
[ ] JSON viewer
[ ] JSON validator
[ ] JSON editor
[ ] JSON to XML
[ ] JSON Minifier
[ ] JSON Path Tester
[ ] JSON to Excel
[ ] JSON to YAML
[ ] JSON to TOML

### XML

[ ] XML viewer
[ ] XML to JSON
[ ] XML validator
[ ] XML to Excel
[ ] XML formatter
[ ] XML-XSL Transformation
[ ] XML Escape Unescape
[ ] MXML viewer
[ ] XML editor

### HTML

[ ] HTML viewer
[ ] HTML formatter
[ ] HTML to PHP
[ ] HTML Escape Unescape

### CSS

[ ] CSS validator
[ ] CSS beautifier
[ ] CSS Minifier

### Conversion
[ ] Base64 Encode / Decode
[ ] Base converter
[ ] UTF-8 / UTF-16 / UTF-32 / other formats

### Others

[ ] Text reverser
[ ] Encrypt / Decrypt
[ ] Random Word generator
[ ] Password generator
[ ] Hmac generator
[ ] Share link generator
[ ] Responsive Tester

## Color
[ ] RGB / HSV / CMYK / Pantone / HEX / HTML name

##Done
- notepad
- stopwatch
- character map
- line counter
