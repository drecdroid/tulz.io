const {mount, el} = require('redom')
const TextOperation = require('../../TextOperationBase')

class TrimLines extends TextOperation {

    process(input_text){
        return input_text.split('\n').map(l=>l.trim()).join('\n')
    }
}

TrimLines.title = 'Trim Lines'
TrimLines.group = 'lines'
TrimLines.info = 
`# Trim Lines
Removes any whitespace at the start or the end of the line.
`
module.exports = TrimLines