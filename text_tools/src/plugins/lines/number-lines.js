const { setChildren, el } = require('redom')
let TextOperation = require('../../TextOperationBase')

module.exports = class extends TextOperation {

    createControls(){
        setChildren(this.controls, [
            el('label', 'Number empty lines',
                this.emptyLines = el('input', { type : 'checkbox'})            
            ),
            el('label', 'Start at number',
                this.startNumber = el('input', { type : 'number', value: 1} )
            ),
            el('label', 'Number suffix',
                this.numberSuffix = el('input', { type : 'text', value: '.- '})
            )
        ])
    }

    process(input_text){
        let lineNumber = this.startNumber.value

        return input_text
        .split('\n')
        .map(line => {
            if(!line.trim() && !this.emptyLines.checked){
                return line
            }
            
            return `${lineNumber++}${this.numberSuffix.value}${line}`
        })
        .join('\n')
    }
}

module.exports.title = 'Number Lines'
module.exports.group = 'lines'