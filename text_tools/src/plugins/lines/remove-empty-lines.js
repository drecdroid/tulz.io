let TextOperation = require('../../TextOperationBase')

module.exports = class extends TextOperation {

    process(input_text){
        return input_text.split('\n').filter(line=>line.trim()).join('\n')
    }
}

module.exports.title = 'Remove Empty Lines'
module.exports.group = 'lines'