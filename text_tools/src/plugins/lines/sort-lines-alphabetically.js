const {mount, el} = require('redom')
const TextOperation = require('../../TextOperationBase')

class SortLinesAlphabetically extends TextOperation {
    
    process(input_text){
        return input_text.split('\n').sort((l1, l2)=>l1.trim()>l2.trim()).join('\n')
    }
}

SortLinesAlphabetically.title = 'Sort Lines Alphabetically'
SortLinesAlphabetically.group = 'lines'

module.exports = SortLinesAlphabetically