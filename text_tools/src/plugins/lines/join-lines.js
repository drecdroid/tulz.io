const { mount, el } = require('redom')

const TextOperation = require('../../TextOperationBase')

class JoinLines extends TextOperation {
    constructor(initial, item, index, collection){
        super(initial, item, index, collection)

        mount(this.controls,
            el('label', 'Use as separator: ',
                this.separatorControl = el('input', {type:"text", value:","})
            )
        )
    }

    get separator(){
        return this.separatorControl.value
    }

    process(input_text){
        return input_text.split('\n').join(this.separator)
    }
}

JoinLines.title = 'Join Lines'
JoinLines.group = 'lines'

module.exports = JoinLines