const {mount, el} = require('redom')
const TextOperation = require('../../TextOperationBase')

class SplitInLines extends TextOperation {
    constructor(initial, item, index, collection){
        super(initial, item, index, collection)

        mount(this.controls,
            el('label',
                'Split by: ',
                this.separatorControl = el('input', {type:'text', value:','})
            )
        )
    }

    get separator(){
        return this.separatorControl.value
    }
    
    process(input_text){
        return input_text.split(this.separator).join('\n')
    }
}

SplitInLines.title = 'Split In Lines'
SplitInLines.group = 'lines'

module.exports = SplitInLines