let TextOperation = require('../../TextOperationBase')

class RemoveLineBreaks extends TextOperation {

    process(input_text){
        return input_text.split('\n').join('')
    }
}

RemoveLineBreaks.title = 'Remove Line Breaks'
RemoveLineBreaks.group = 'lines'

module.exports = RemoveLineBreaks