const { setChildren, el } = require('redom')
let TextOperation = require('../../TextOperationBase')

module.exports = class extends TextOperation {

    createControls(){

    }

    process(input_text){
        return Array.from(new Set(input_text.split('\n'))).join('\n')
    }
}

module.exports.title = 'Remove Duplicates'
module.exports.group = 'lines'