let TextOperation = require('../../TextOperationBase')

module.exports = class extends TextOperation {

    /**
     * @param {string} input_text 
     */
    process(input_text){
        return input_text.replace(/&.+?;/g, m=> {
            switch(m){
                case '&lt;':
                    return '<'
                case '&gt;':
                    return '>'
                case '&amp;':
                    return '&'
                case '&quot;':
                    return '"'
                default:
                    m
            }
        })
    }
}

module.exports.title = 'Unescape HTML'