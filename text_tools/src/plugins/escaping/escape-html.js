let TextOperation = require('../../TextOperationBase')

module.exports = class extends TextOperation {

    /**
     * @param {string} input_text 
     */
    process(input_text){
        return input_text.replace(/[<>"&]/g, m=> {
            switch(m){
                case '<':
                    return '&lt;'
                case '>':
                    return '&gt;'
                case '&':
                    return '&amp;'
                case '"':
                    return '&quot;'
                default:
                    m
            }
        })
    }
}

module.exports.title = 'Escape HTML'