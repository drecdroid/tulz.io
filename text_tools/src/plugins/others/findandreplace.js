const { setChildren, el } = require('redom')
let TextOperation = require('../../TextOperationBase')

module.exports = class extends TextOperation {

    createControls(){
        let checked = true

        setChildren(this.controls, [
            el('label', 'Global',
                this.global = el('input', {type:'checkbox', checked})
            ),
            el('label', 'Multiline',
                this.multiline = el('input', {type:'checkbox'})
            ),
            el('label', 'Case Insensitive',
                this.insensitive = el('input', {type:'checkbox'})
            ),
            el('label', 'Pattern',
                this.pattern = el('input', {type:'text'})
            ),
            el('label', 'Replace With',
                this.replacement = el('input', {type:'text'})
            )
        ])
    }

    process(input_text){
        let re = new RegExp(this.pattern.value, `${this.multiline.checked? 'm':''}${this.global.checked ? 'g':''}${this.insensitive.checked?'i':''}`)
        return input_text.replace(re, this.replacement.value)
    }
}

module.exports.title = 'Find & Replace'