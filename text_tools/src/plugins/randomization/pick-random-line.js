const Random = require('random-js')
const { el, setChildren } = require('redom')
let TextOperation = require('../../TextOperationBase')

let random = new Random(Random.engines.browserCrypto)

module.exports = class extends TextOperation {
    process(input_text){
        return random.pick(input_text.split('\n'))
    }
}

module.exports.title = 'Pick Random Line'