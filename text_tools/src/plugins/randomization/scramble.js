const Random = require('random-js')
const { el, setChildren } = require('redom')
let TextOperation = require('../../TextOperationBase')

let random = new Random(Random.engines.browserCrypto)

class Radio {
    constructor(name, options){
        this.el = el('',
            options.map(option => el(
                'label', option.text,
                el('input', {name, type:'radio', value: option.value, checked: option.checked || false })
            ))
        )

        this.el.addEventListener('click', e => {
            if(e.target.getAttribute('name') === name){
                this.value = e.target.value
            }
        })
    }
}

module.exports = class extends TextOperation {

    createControls(){
        setChildren(this.controls,
            this.options = new Radio('scramble',[
            {
                text : 'Everything',
                value : 'everything',
                checked : true
            },
            {
                text : 'Everythin Preservig Line Break',
                value : 'everything_break'
            },
            {
                text : 'Each Line',
                value : 'line'
            },
            {
                text : 'Each Word',
                value : 'word'
            }
        ]))
    }

    /**
     * 
     * @param {string} input_text 
     */
    process(input_text){
        function shuffle(str){
            return random.shuffle(Array.from(str)).join('')
        }

        switch(this.options.value){
            case undefined:
            case 'everything':
                return shuffle(input_text)
            case 'everything_break':
                let breaks = input_text.split('\n').map(l=>l.length)

                let shuffled = random.shuffle(Array.prototype.filter.call(input_text, c => c !== '\n'))
                
                return breaks.map(b => shuffled.splice(0, b).join('')).join('\n')
            case 'line':
                return input_text.replace(/.+/g, shuffle)
            case 'word':
                return input_text.replace(/\w\S*/g, shuffle)
        }
    }
}

module.exports.title = 'Scramble Text'