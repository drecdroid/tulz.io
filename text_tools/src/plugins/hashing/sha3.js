let TextOperation = require('../../TextOperationBase')
let hash = require('crypto-js/sha3')

module.exports = class extends TextOperation {

    process(input_text){
        return hash(input_text)
    }
}

module.exports.title = 'SHA3'
module.exports.group = 'hashing'