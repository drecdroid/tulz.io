let TextOperation = require('../../TextOperationBase')
let md5 = require('crypto-js/md5')

module.exports = class extends TextOperation {

    process(input_text){
        return md5(input_text)
    }
}

module.exports.title = 'MD5'
module.exports.group = 'hashing'