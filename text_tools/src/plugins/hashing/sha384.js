let TextOperation = require('../../TextOperationBase')
let hash = require('crypto-js/sha384')

module.exports = class extends TextOperation {

    process(input_text){
        return hash(input_text)
    }
}

module.exports.title = 'SHA384'
module.exports.group = 'hashing'