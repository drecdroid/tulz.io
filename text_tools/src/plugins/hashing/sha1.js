let TextOperation = require('../../TextOperationBase')
let sha1 = require('crypto-js/sha1')

module.exports = class extends TextOperation {

    process(input_text){
        return sha1(input_text)
    }
}

module.exports.title = 'SHA1'
module.exports.group = 'hashing'