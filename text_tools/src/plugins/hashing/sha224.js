let TextOperation = require('../../TextOperationBase')
let hash = require('crypto-js/sha224')

module.exports = class extends TextOperation {

    process(input_text){
        return hash(input_text)
    }
}

module.exports.title = 'SHA224'
module.exports.group = 'hashing'