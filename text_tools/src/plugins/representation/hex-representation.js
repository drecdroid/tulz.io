const { setChildren, el } = require('redom')
let TextOperation = require('../../TextOperationBase')

let enc = require('crypto-js').enc

module.exports = class extends TextOperation {

    createControls(){
        setChildren(this.controls, [
            el('label', 'Encoding',
                this.encoding = el('select',
                    el('option', {value:'Utf16', selected:true}, 'Utf16'),
                    el('option', {value:'Utf16LE'}, 'Utf16LE'),
                    el('option', {value:'Utf8'}, 'Utf8'),
                    el('option', {value:'Latin1'}, 'Latin1')
                )
            )
        ])
    }

    process(input_text){
        return enc[this.encoding.value].parse(input_text).toString().match(/.{2}/g).join(' ')
    }
}

module.exports.title = 'Hexadecimal Representation'