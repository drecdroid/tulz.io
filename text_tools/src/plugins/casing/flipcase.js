const { el, setChildren } = require('redom')
let TextOperation = require('../../TextOperationBase')

module.exports = class extends TextOperation {

    /**
     * 
     * @param {string} input_text 
     */
    process(input_text){
        return Array.from(input_text).map( c => c === c.toLowerCase() ? c.toUpperCase() : c.toLowerCase()).join('')
    }
}

module.exports.title = 'Flip Case'