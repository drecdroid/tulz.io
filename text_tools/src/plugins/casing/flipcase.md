# Flip Case
Uppercases lowercase characters and viceversa

## Example
If input is:

```
Sample TEXT
```

Text output will be:

```
sAMPLE text
```