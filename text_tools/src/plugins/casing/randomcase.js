const Random = require('random-js')
const TextOperation = require('../../TextOperationBase')

let random = new Random(Random.engines.browserCrypto)

module.exports = class extends TextOperation {

    /**
     * 
     * @param {string} input_text 
     */
    process(input_text){
        return Array.from(input_text).map(c => random.bool()? c.toLowerCase(): c.toUpperCase()).join('')
    }
}

module.exports.title = 'Random Case'