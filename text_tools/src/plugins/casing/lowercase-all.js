const TextOperation = require('../../TextOperationBase')

module.exports = class extends TextOperation {
    process(input_text){
        return input_text.toLowerCase()
    }
}

module.exports.title = 'Lowercase All'