const TextOperation = require('../../TextOperationBase')

module.exports = class extends TextOperation {

    /**
     * 
     * @param {string} input_text 
     */
    process(input_text){
        return input_text.replace(/\w\S*/g, txt => txt.charAt(0).toUpperCase() + txt.substr(1));
    }
}

module.exports.title = 'Title Case'