const { el, setChildren } = require('redom')
let TextOperation = require('../../TextOperationBase')

module.exports = class extends TextOperation {
    process(input_text){
        return input_text.split('\n').map(line =>{
          line = line.replace(/(.+):\/\//, "")
          line = line.split(/[/?:@]/)[0]
          return line
        }).join('\n')
    }
}

module.exports.title = 'Get Hostname'