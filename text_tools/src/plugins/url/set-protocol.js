const { el, setChildren } = require('redom')
let TextOperation = require('../../TextOperationBase')

module.exports = class extends TextOperation {
    
    createControls(){
      setChildren(this.controls, [
        el('label', 'New protocol',
          this.protocol = el('input', { type : 'text' })
        )
      ])
    }

    /**
     * @param {string} input_text 
     */
    process(input_text){
      
        return input_text.split('\n').map(line =>{
          line = line.split('://')[1] || ""
          line = this.protocol.value + line
          return line
        }).join('\n')
    }
}

module.exports.title = 'Set Scheme'