const { setChildren, el } = require('redom')
let TextOperation = require('../../TextOperationBase')

let AES = require('crypto-js').AES
let Utf8 = require('crypto-js').enc.Utf8

module.exports = class extends TextOperation {

    createControls(){
        setChildren(this.controls,[
            el('label', 'Secret key: ',
                this.secretKey = el('input', {type:"text"})
            ),
            el('label', 'Encrypt: ',
                el('input', {type:"radio", value:"encrypt", name: "method"})
            ),
            el('label', 'Decrypt: ',
                el('input', {type:"radio", value:"decrypt", name: "method"})
            )
        ])
    }

    process(input_text){
        let method = this.controls.querySelector('input[name="method"]:checked').value

        switch(method){
            case 'encrypt':
                return AES.encrypt(input_text, this.secretKey.value).toString()
            case 'decrypt':
                return AES.decrypt(input_text, this.secretKey.value).toString(Utf8)
        }
        
    }
}

module.exports.title = 'AES'
module.exports.group = 'encryption'