const { mount, el } = require('redom')

const TextOperation = require('../../TextOperationBase')

class Base64Encoder extends TextOperation {

    process(input_text){
        return btoa(input_text)
    }
}

Base64Encoder.title = 'Base64 Encoder'
Base64Encoder.group = 'encoding'

module.exports = Base64Encoder