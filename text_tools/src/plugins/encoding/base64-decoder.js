const { mount, el } = require('redom')

const TextOperation = require('../../TextOperationBase')

module.exports = class extends TextOperation {

    process(input_text){
        return atob(input_text)
    }
}

module.exports.title = 'Base64 Decoder'
module.exports.group = 'encoding'