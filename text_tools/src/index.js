const { el, mount, list } = require('redom')
const textOperations = require('./textOperations')
const { ToolBox, ToolBoxInfo } = require('./ToolBox')
const uuid = require('uuid')

const Home = require('./home')

function TextOperationFactory(initial, item, index, collection){
    return new (textOperations.operations.find(operation => operation.id === item.operation_type))(initial, item, index, collection)
}

function insertAfter(arr, origin, target){
    if(target>arr.length-1) return arr

    if(origin > target){
        arr = [...arr.slice(0,target+1), arr[origin], ...arr.slice(target+1, origin), ...arr.slice(origin+1)]
    }
    else{
        arr = [...arr.slice(0,origin), ...arr.slice(origin+1, target+1), arr[origin], ...arr.slice(target+1)]
    }
    return arr
}

function insertBefore(arr, origin, target){
    if(target<0) return arr

    if(origin > target){
        arr = [...arr.slice(0,target), arr[origin], ...arr.slice(target, origin), ...arr.slice(origin+1)]
    }else{
        arr = [...arr.slice(0,origin), ...arr.slice(origin+1, target), arr[origin], ...arr.slice(target)]
    }
    return arr
}

class TextOperationPipeline {
    constructor(){
        this.moveBelow = (origin_id, target_id) => {
            let o_index = this.operations.findIndex(v=>v.id === origin_id)
            let t_index = this.operations.findIndex(v=>v.id === target_id)
            
            this.operations = insertAfter(this.operations, o_index, t_index)
            this.update(this.operations)
        }

        this.moveOver = (origin_id, target_id) => {
            let o_index = this.operations.findIndex(v=>v.id === origin_id)
            let t_index = this.operations.findIndex(v=>v.id === target_id)
            this.operations = insertBefore(this.operations, o_index, t_index)
            this.update(this.operations)
        }

        this.moveUp = (operation_id) => {
            let id = this.operations.findIndex(v=>v.id === operation_id)
            this.operations = insertBefore(this.operations, id, id-1)
            this.update(this.operations)
        }

        this.moveDown = (operation_id) => {
            let id = this.operations.findIndex(v=>v.id === operation_id)
            this.operations = insertAfter(this.operations, id, id+1)
            this.update(this.operations)
        }

        this.deleteOperation = (operation_id) => {
            this.operations = this.operations.filter(operation=> operation.id !== operation_id)
            this.update(this.operations)
        }

        this.addOperation = (operation_type) => {
            this.operations.push({
                id: uuid(),
                operation_type,
            })
            this.update(this.operations)
        }

        this.operations = []

        this.el = el('.text-operation-list')
        this.list = list(this.el, TextOperationFactory, 'id', {
            moveBelow : this.moveBelow,
            moveOver : this.moveOver,
            moveUp : this.moveUp,
            moveDown : this.moveDown,
            deleteOperation : this.deleteOperation
        })
    }

    process(input_text){
        return this.list.views.filter(view=>view.enabled).reduce( (pv, cv) => cv.process(pv), input_text)
    }

    update(data){
        this.list.update(data)
    }
}

class ToolInfo {
    constructor(){
        this.viewInfo = this.viewInfo.bind(this)
        this.el = el('.tool-info')
        
        window.addEventListener('view-info', e => {
            if(e.detail === 'default'){
                this.viewDefault()
            }
            else{
                let md_path = `help/${e.detail.operation_type}.html`
                this.viewInfo(md_path)
            }
        })

        this.viewDefault()
    }

    viewInfo(md_path){
        let req = new XMLHttpRequest()
        req.open('GET', md_path)
        req.onreadystatechange = (ev)=>{
            if(req.readyState === 4 && req.status === 200){
                this.el.innerHTML = req.responseText || ''
            }
        }

        req.send()
    }

    viewDefault(){
        this.viewInfo('help/index.html')
    }
}

class App {
    constructor(){
        this.el = el('',
            el('.workspace',
                this.inputText = el('textarea.text-input'),
                this.textOperationsPipeline = new TextOperationPipeline(),
                this.processBtn = el('button.process-btn.control', 'Run Text Operations'),
                this.outputText = el('textarea.text-output'),
                el('.output-buttons',
                    this.copy = el('button.control.control-bar', 'Copy Output'),
                    this.download = el('button.control.control-bar', 'Download Output'),
                    this.reinput = el('button.control.control-bar', 'Use Ouput As Input')                
                )
            )
        )

        this.copy.addEventListener('click', e=>{
            this.outputText.select()
            document.execCommand('copy')
            this.outputText.selectionEnd = this.outputText.selectionStart
            this.outputText.blur()            
        })
        
        this.download.addEventListener('click', e => {
            let link = document.createElement('a')
            link.download = 'output.txt'
            console.log(this.outputText.value)
            let data = `data:text/plain;charset=UTF-8,${encodeURIComponent(this.outputText.value)}`
            link.href = data
            document.body.appendChild(link)
            link.click()
            document.body.removeChild(link)
        })
        
        this.reinput.addEventListener('click', e => {
            this.inputText.value = this.outputText.value
        })

        addEventListener('add-operation', e => {
            this.textOperationsPipeline.addOperation(e.detail.id)
        })

        this.processBtn.addEventListener('click', (e)=>{
            this.outputText.value = this.textOperationsPipeline.process(this.inputText.value)
        })
    }

    update(data){

    }
}

document.addEventListener('DOMContentLoaded', main)
function main(){
    mount(document.body, new Home(new ToolBox(),new App(), new ToolInfo()))
}