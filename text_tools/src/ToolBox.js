const { el, list, mount } = require('redom')
const textOperations = require('./textOperations')

class ToolBoxItem {
    constructor(initial, data, i, collection){
        this.el = el('.tool-box-item.control.control-bar', data.title)
        this.data = data

        this.el.onclick = e => {
            this.el.dispatchEvent(
                new CustomEvent('add-operation', { detail: this.data, bubbles: true})
            )
        }
    }
    
    update(data, i, collection){
        this.data = data
    }
}

class ToolBoxGroupItems {
    constructor(data){
        this.el = el('.tool-box-group-items')
        this.list = list(this.el, ToolBoxItem, 'id')

        this.list.update(data)
    }

    update(data){
        this.list.update(data)
    }
}

class ToolBoxGroup {
    constructor(initial, data, i, collection){
        this.el = el('.tool-box-group.collapsed',
            this.toggle_collapsed = el('.tool-box-group-title',
                el('i.group-collapsed-indicator.fa'),`${data.title} (${data.operations && data.operations.length || 0})`
            ),
            new ToolBoxGroupItems(data.operations)
        )

        this.toggle_collapsed.addEventListener('click', e => {
            this.el.classList.toggle('collapsed')
        })
    }


    update(data){
        this.data = data
    }
}


class ToolBox {
    constructor(){
        this.el = el('.tool-box')
        this.list = list(this.el, ToolBoxGroup, 'id')

        let groups = textOperations.groups        
        let operations = textOperations.operations

        groups.map(group => {
            group.operations = operations.filter(operation => operation.group === group.id)
        })
        
        this.update(groups)
    }

    update(data){
        this.list.update(data)
    }

}

module.exports = {
    ToolBox,
    ToolBoxItem
}