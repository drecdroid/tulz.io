let { el, mount, list } = require('redom')

class Template {
    constructor(left, middle, right){
        this.el = el('',
            el('.app-container.vertical-bar',
                el('.top-bar.bar',
                    el('.menu-toggle.bar-button.fa.fa-bars'),                    
                    el('h1.app-name',
                        'AIO Text Tools',
                        this.viewDefaultInfo = el('i.view-default-info.fa.fa-info-circle')),
                    el('.info-toggle.bar-button.fa.fa-info')
                ),
                el('.main-view.widget-container',
                    el('.widget',
                        middle
                    )
                )
            ),
            this.controls_sidebar = el('.controls-sidebar.vertical-bar',
                el('.mini-bar.bar',
                    el('.tulz-logo',
                        el('img', { src: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANIAAAAwCAYAAABkFSTLAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAACnpJREFUeNrsXdtu3MYZ/iX4vnyDUrkImgKJaaRJ0ORC3KCtnCCyVo0R2YojkZJ6be0TSHoCqdfVaqlIceXUiVaN0VRJEDOwjaRoCtEtWqAo0LBvwLyApzPkrLQHHmaGM+Rulj/AtWWvZoYf/+8/zYGTUEklleSWSxUEEuWvd3T8eXGh8F8DfHn48uHlRX+s8Tl1NPy5DoDm8J8GxQcoPvv4cuCqHYzirU1U2i9Bvr1j4c9Wl2JEMvAzcvFnA1551xtDEhn48wG+tHNgBvEiuNjwhj1y+ExWLJAi+sC/DJKIfJqhMv3lA22s0PnM0bHJpiRKFUK2Y/i0pVVEqiRLiJJsjNk9E2+t9VgYlGqUNioiVcIi5lh5I5b77SWWVRFpXAWlhHXxIcz4hr0sXvvTllERqZK4/IiHaJUgpFVEqqSSWMORmR91y0iVwQfnkf57T6Mx6jR0qixZQCBO4GJ/Rv3/78NPF+yRC+sq6RefGz+EAnhzRbwE/smeSfOyy6EOozCU7i67d+b2yJ9P8M8u1FddeUT67l5UfkRCcW08OiiH0v3r7j4mU3SD/7xLxnQbN2BcANPTZgecfXj+hsPcx9kRbZe0iQwKfP+YyRhO4MWbO8pJ9fWhhdtYZjY8LPgOGioPzKVGITT6leXDqeMCX4GlzU+eJnmOpNpXh+wyu9Y1nnr4e+0m0R+iN7/FpPLzEQlgWw6JePmGkhSBKJTblbSuM4Bjwt/v3oYXFq5wJMPrGd+J2v3b78l4aphQ7GEHT3709WELZFes4vE1wX3/e0ymzYKeMiHtGeN3A/p9XgJR3ITXGNBVF/g6bmJCoS2YX2Mm1GSXN9JjrYZYfCtLzIHBsPVvYDJtCmhcloU3GEgn6omMUBmKw3cDk6kYozlj4UgB2SFJ0vENQkP15gqbobq/t04JaknOz6yw3eNdi59IIOiJsvKdfKLz939ukeYUVZOWFalbvQR8rcJoO2OTsOkKDZ+CmDyKhM1TzMuD7u+1wggqO4wT1V/Sbgs+3m2JFRtUJuGFerOnquYhdI6kGYYc3+lC44sZmxDGphfAn1uG0Lq6iERWQfhamEwAv16zWT1SdvWt2PyoQBZyhq3f3jGkKH65+Wf5clWIRNvJnnRCFd6ETJusHingVgSkXBG88hUxMTEdfQ9eFJ6nLSv0flEh66LiinpCOxdIxSzNQ90Py9rrwjeaD98N+GjXhbfX3HSPNHXdg7yTYEh6UeJELD96qkjxkEql9ErAt6jtCsvUi5gJRkinodoZ/GkvLW9rqTcsqfi22EI7kfq9uqKERxNQMcVBE4IDU0S6rG5+fqudy5CJFSVOYPhkG5NJi/FGVk9+yp8fBdTrBTnw1bFXsliKDVvng83OjzTgX3zpQ9JMd283ZCLWGZIwrkgh+PNWG3UQqW4SpaotuUOIL9ErUsF0BkKrVEnMj4iBasC1rknWk3DuaRuSKqVZId7g2PqI9Mx10lmNqbn//IG46QecQO3DT97ZHAnF56nAyRrLq7d2erxwljw8IEr3nUBPxCqXu/wqHV89JjfSBXpx4NrK4H3OhaSah3ZznRKKz3B9tFvHuVI7LbSTg04ZZe/u/AiNwQ76iETRrlN+fG0wl/whvrv+srzInKAbS6Juqa/uQGflDF/+OZeVI1VSRn4kJq3E0Do9P2pgErWLxSN3C2Z6dDCRFCazSIN7/GhwBVBFpDI8aN72Hx7wxfeoK9Qxl3aGDu9sMTjb97E3Ysv/6qukqMVbvdTh3u80+URCRWhfiYUBNEQzmQ8PLBBb70dWfA/ntpQ0fKP8iD+s49MnV0CpDLUeadTyIzRCpHt0YEL/XAZbdyQfqsGoCMqN7/84e/w+r/5WoZ1qjyjLsDw6iPaK8UYH0Sa2eeyNgh/mw4gte/vKPNgFvmZFpGEKG9lIpFESiSxNIhU6b4TxFVnX6Bf90CflAFPgXqXkbRPFxO9FE+3RYafMrQv0XXyFTj6+vkCrWkEkL8gjFbttovz5IzX50TaklrkTf3ZgeggqdPklEMCX14uJ4OuVF9qN2yEhebelPD7cBO7Ndyh6yNNL9g8f4MRlQT9S58HQIMFHLkcapmVBquXxISGQyNG9JBSqjcTzY8NXIL9DJucviGxw7CHSpfzADPGyIO/IAOOGNzJEviARCTVaAvlnVKGbVlSh+9whCroM0bo3M5z4jIhLVpG3wxODZOP71koA9/cCzrzHgD829Z6FqklyvKvRe+GT678pKbQrZ1NeccfeyipKRCR6IBg2NjCJ5FfovnA0TKJjOi6rS/EiQkWnT53BqWMpQtdlXBbULazefJ0dX3QxHmnFBlQwm5BQ9WZjaIjOkh89PowO3BCrOm3B9HuOotETEmUtSYrGfurUFfR/wo0vwoQ/aZoZ3ogYrdsyxnNJknaol+dv+PCPI14Xr+PwjiiBjUO8IPE7eT2MvFxq49yL8i27iu7tq/c3swsd9AdELevry26GN7KA63BHhMnUcmGm7817efJPhNogdmLQMSaTDXOrg1MAx4RkSHRuri2ZSCwPTbKL71hG9mVB9fA6OyI3/6TLYv2YKog+FIn440OdL8wY8AYihQmS70xlkpvveXaOvN4R1qH+/mZXA/ik2Qb+48M0SiYft0l+v7MUaI6e1iuivy7Oj3w1ROIpSuR38XVBYuPfQ3UlhkOOYbktDV92qHX4ct9M9Epf7GNyI539/s7/MsdNJJbQVfw01WQjxY9v7PaMSXXapCS8c0DGWwryetCXF91cHaOESlMhhZ6B02pTwjakl4JvnMyGFbgtxvxIlbSxN3IlE6k0PjVKXhakavGnGWsB1eN7ufAnKFrhnF0lOaAnh9jc+KZuzx+9bRSRV3IT8yP11TdHUcteSfhqQ5L/MrY/UQOV705CiSSqYW8UFEekYtzWPL/iSbHwSeGFx5E/pLWdH18kdE/x8gvLFcTXV6ZKsyuRUnf6UH8acBC+AKBvAlYmkcpbmv/CAr65pzXg3QnJZHFR2v3O4/woyIUFSi2kFJ0fEXmS0YKIB95PvDcZUwWz4UvIrsQ+f7ke1KeeKHMFvTiRnn0nEJwklUSmmwG+atRDqHT1QdjHS4tX8BVPmFfe9SHv4Zqv3XJCQhabHwUMROHFtw1XbTeGjEEm0REXmQJ81SA6vCTgMyxM+DohWa+vMRnJvKFdgzmpU5Yq3yAJ6FT4wBEEEq2RF97fBG77pcVNhu/b9M1+ecROVQq5+LqhtX19OV0Jfxmun2sw4unFJuRv2J1wTL7Bu7aygwc2JcWgonMCTcHbaza+mNvLX/7694cWdGadsx/0Fjy3sKmUWN4RmSsiq3mNi2OTUiwQ6lEC7BHQk9Cq/mxRzNt+80EdolL2dF/83nlfqR+GDK/eiifdIzIxS8vOYu/eZQntvEwC9cvnDsGTPGczJj/qeLatgRUN3RIdRUzwuQw9KzhoGIXCsxbc8O9vrYjhf9Ik7c/R9g2Gip1P+/wqnLTlII9cIl0Qyhw47wudg+yFA35uofhQ8OxIo++GTVJMH168WV6IOmrymUNIrkP36u8ZhqJEWdJuxrxvmCrC/JpbPdBKKhki+b8AAwDRrD7EuOELegAAAABJRU5ErkJggg=='} )
                    ),
                    el('.menu-toggle.bar-button.fa.fa-bars')
                ),
                el('.widgets-wrapper.widget-container',
                    el('.widget',
                        left
                    )
                )
            ),
            this.info_sidebar = el('.info-sidebar.vertical-bar',
                el('.bar',
                    el('.info-toggle.bar-button.fa.fa-info')
                ),
                el('.widget-container',
                    el('.widget',
                        right
                    )
                )
            )
        
        )

        this.viewDefaultInfo.addEventListener('click', e => {
            this.el.dispatchEvent(new CustomEvent('view-info', { detail:'default', bubbles: true }))
        })

        let ontouch = (e) => {
            if(e.target.classList.contains('menu-toggle')){
                document.body.classList.toggle('controls_sidebar_opened')
                document.body.classList.toggle('info_sidebar_opened', false)
            }
            else
            if(e.target.classList.contains('info-toggle')){
                document.body.classList.toggle('controls_sidebar_opened',false)
                document.body.classList.toggle('info_sidebar_opened')
            }

            if(window.innerWidth <= 640){
                if(document.body.classList.contains('controls_sidebar_opened')){
                    if(e.clientX > this.controls_sidebar.clientWidth){
                        document.body.classList.toggle('controls_sidebar_opened', false)
                    }
                }

                if(document.body.classList.contains('info_sidebar_opened')){
                    if(e.clientX < this.info_sidebar.offsetLeft){
                        document.body.classList.toggle('info_sidebar_opened', false)
                    }
                }
            }
            console.log('touch')
            e.stopPropagation();
            return false;
        }

        if(document.ontouchstart){
            document.addEventListener('touchstart', ontouch)
        }else{
            document.addEventListener('click', ontouch)
        }


        document.addEventListener('view-info', e=>{
            setTimeout(
                ()=>{
                    document.body.classList.toggle('info_sidebar_opened', true)
                    if(window.innerWidth <= 900){
                        document.body.classList.toggle('controls_sidebar_opened',false)
                    }
                },
                30
            )
        })
    }
}

module.exports = Template