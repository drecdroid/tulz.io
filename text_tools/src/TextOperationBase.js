const { el, mount, list } = require('redom')

class TextOperationBase {

    constructor(initial, data, index, collection){

        this.title = this.constructor.title || ""
        this.data = data
        this.handleDrag = e =>{
            e.dataTransfer.setData('text/plain', this.data.id)
            e.dropEffect = "move"            
        }

        this.handleDragEnd = e =>{
            document.body.classList.toggle('dragging', false)
        }


        this.handleDragOver = e => {
            e.preventDefault()
            e.dataTransfer.dropEffect = "move"
        }

        this.handleDrop = e =>{
            console.log("dropping")
            let origin_id = e.dataTransfer.getData('text')
            if(this.data.id !== origin_id){
                if(e.target.classList.contains('drop-over')){
                    initial.moveOver(origin_id, this.data.id)
                }else{
                    initial.moveBelow(origin_id, this.data.id)
                }
            }
        }

        this.el = el(
            '.text-operation.expanded', 
            this.dropzone_top = el('.drop-zone.drop-over'),
            this.dragarea = el('.text-operation-dragarea', {
                draggable : true,
            },
                this.expandControl = el('i.expanded-icon.fa'),
                el('label.enabled-icon.fa.fa-eye',
                    this.enabledControl = el('input', {type:"checkbox", checked: true, hidden: true})
                ),
                this.titleControl = el('span.text-operation-title', this.title),
                this.infoControl = el('i.info-icon.fa.fa-info'),
                this.moveup = el('i.move-up-icon.fa.fa-arrow-up'),
                this.movedown = el('i.move-down-icon.fa.fa-arrow-down'),
                this.delete = el('i.delete-icon.fa.fa-trash')
            ),
            this.controls = el('.text-operation-controls'),
            this.dropzone_bot = el('.drop-zone.drop-below')
        )

        this.moveup.addEventListener('click', e => {
            initial.moveUp(this.data.id)
        })

        this.movedown.addEventListener('click', e => {
            initial.moveDown(this.data.id)
        })
        
        this.infoControl.addEventListener('click', e => {
            this.el.dispatchEvent(new CustomEvent('view-info', {
                detail: this.data,
                bubbles: true
            }))
        })

        this.enabledControl.addEventListener('change', e => {
            this.enabled = this.enabled
        })

        this.dragarea.ondragstart = this.handleDrag
        this.dragarea.ondragenter = (e)=>{
            document.body.classList.toggle('dragging', true)
        }
        this.dragarea.ondragend = this.handleDragEnd

        this.titleControl.onclick =
        this.expandControl.onclick =
        e =>{
            this.el.classList.toggle('expanded')
        }

        this.dropzone_top.addEventListener("dragover", this.handleDragOver)
        this.dropzone_bot.addEventListener("dragover", this.handleDragOver)

        this.dropzone_top.addEventListener("drop", this.handleDrop)
        this.dropzone_bot.addEventListener("drop", this.handleDrop)

        this.delete.addEventListener('click', e =>{
            /*if(confirm(`Are you sure you want to delete the ${name} operation?`)){
                initial.deleteOperation(this.data.id)
            }*/
            initial.deleteOperation(this.data.id)
        }, false)

        this.createControls()
    }

    createControls(){
        
    }

    set enabled(value){
        this.el.classList.toggle('disabled', !value)
    }

    get enabled(){
        return this.enabledControl.checked
    }

    update(data, index, collection){
        this.data = data
    }
    
    /**
     * 
     * @param {string} input_text 
     */
    process(input_text){
        return input_text
    }
}

module.exports = TextOperationBase