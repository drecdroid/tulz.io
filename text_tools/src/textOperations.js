const path = require('path')

function expandObj(list){
    list.forEach((obj, i) => {
        let tempObj = obj.obj
        delete obj.obj
        list[i] = Object.assign(tempObj, obj)
    })
}

const textOperations = module.exports = require('./plugins/**/*.js',
    {
        mode: (base,files, config) => {
            let result = `{
                    'groups' : [
                        ${files
                            .filter(file => file.includes('index.js'))
                            .map(file=>
                            `{
                                "id" : "${path.basename(path.dirname(file))}",
                                "obj" : require("${path.dirname(file)}")
                            }`
                        ).join(',')
                        }
                    ],
                    'operations' : [
                        ${files
                            .filter(file => !file.includes('index'))
                            .map(file=>
                                `{
                                    "id": "${path.basename(path.dirname(file))}/${path.basename(file,'.js')}",
                                    "group" : "${path.basename(path.dirname(file))}",
                                    "obj" : require("${file}")
                                }`
                            ).join(',')
                        }
                    ]
                }
                `.replace(/ /g,'')
            
            return result
        }
    }
)
expandObj(textOperations.operations)
expandObj(textOperations.groups)

console.log(module.exports)