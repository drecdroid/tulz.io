(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/*jshint eqnull:true*/
(function (root) {
  "use strict";

  var GLOBAL_KEY = "Random";

  var imul = (typeof Math.imul !== "function" || Math.imul(0xffffffff, 5) !== -5 ?
    function (a, b) {
      var ah = (a >>> 16) & 0xffff;
      var al = a & 0xffff;
      var bh = (b >>> 16) & 0xffff;
      var bl = b & 0xffff;
      // the shift by 0 fixes the sign on the high part
      // the final |0 converts the unsigned value into a signed value
      return (al * bl) + (((ah * bl + al * bh) << 16) >>> 0) | 0;
    } :
    Math.imul);

  var stringRepeat = (typeof String.prototype.repeat === "function" && "x".repeat(3) === "xxx" ?
    function (x, y) {
      return x.repeat(y);
    } : function (pattern, count) {
      var result = "";
      while (count > 0) {
        if (count & 1) {
          result += pattern;
        }
        count >>= 1;
        pattern += pattern;
      }
      return result;
    });

  function Random(engine) {
    if (!(this instanceof Random)) {
      return new Random(engine);
    }

    if (engine == null) {
      engine = Random.engines.nativeMath;
    } else if (typeof engine !== "function") {
      throw new TypeError("Expected engine to be a function, got " + typeof engine);
    }
    this.engine = engine;
  }
  var proto = Random.prototype;

  Random.engines = {
    nativeMath: function () {
      return (Math.random() * 0x100000000) | 0;
    },
    mt19937: (function (Int32Array) {
      // http://en.wikipedia.org/wiki/Mersenne_twister
      function refreshData(data) {
        var k = 0;
        var tmp = 0;
        for (;
          (k | 0) < 227; k = (k + 1) | 0) {
          tmp = (data[k] & 0x80000000) | (data[(k + 1) | 0] & 0x7fffffff);
          data[k] = data[(k + 397) | 0] ^ (tmp >>> 1) ^ ((tmp & 0x1) ? 0x9908b0df : 0);
        }

        for (;
          (k | 0) < 623; k = (k + 1) | 0) {
          tmp = (data[k] & 0x80000000) | (data[(k + 1) | 0] & 0x7fffffff);
          data[k] = data[(k - 227) | 0] ^ (tmp >>> 1) ^ ((tmp & 0x1) ? 0x9908b0df : 0);
        }

        tmp = (data[623] & 0x80000000) | (data[0] & 0x7fffffff);
        data[623] = data[396] ^ (tmp >>> 1) ^ ((tmp & 0x1) ? 0x9908b0df : 0);
      }

      function temper(value) {
        value ^= value >>> 11;
        value ^= (value << 7) & 0x9d2c5680;
        value ^= (value << 15) & 0xefc60000;
        return value ^ (value >>> 18);
      }

      function seedWithArray(data, source) {
        var i = 1;
        var j = 0;
        var sourceLength = source.length;
        var k = Math.max(sourceLength, 624) | 0;
        var previous = data[0] | 0;
        for (;
          (k | 0) > 0; --k) {
          data[i] = previous = ((data[i] ^ imul((previous ^ (previous >>> 30)), 0x0019660d)) + (source[j] | 0) + (j | 0)) | 0;
          i = (i + 1) | 0;
          ++j;
          if ((i | 0) > 623) {
            data[0] = data[623];
            i = 1;
          }
          if (j >= sourceLength) {
            j = 0;
          }
        }
        for (k = 623;
          (k | 0) > 0; --k) {
          data[i] = previous = ((data[i] ^ imul((previous ^ (previous >>> 30)), 0x5d588b65)) - i) | 0;
          i = (i + 1) | 0;
          if ((i | 0) > 623) {
            data[0] = data[623];
            i = 1;
          }
        }
        data[0] = 0x80000000;
      }

      function mt19937() {
        var data = new Int32Array(624);
        var index = 0;
        var uses = 0;

        function next() {
          if ((index | 0) >= 624) {
            refreshData(data);
            index = 0;
          }

          var value = data[index];
          index = (index + 1) | 0;
          uses += 1;
          return temper(value) | 0;
        }
        next.getUseCount = function() {
          return uses;
        };
        next.discard = function (count) {
          uses += count;
          if ((index | 0) >= 624) {
            refreshData(data);
            index = 0;
          }
          while ((count - index) > 624) {
            count -= 624 - index;
            refreshData(data);
            index = 0;
          }
          index = (index + count) | 0;
          return next;
        };
        next.seed = function (initial) {
          var previous = 0;
          data[0] = previous = initial | 0;

          for (var i = 1; i < 624; i = (i + 1) | 0) {
            data[i] = previous = (imul((previous ^ (previous >>> 30)), 0x6c078965) + i) | 0;
          }
          index = 624;
          uses = 0;
          return next;
        };
        next.seedWithArray = function (source) {
          next.seed(0x012bd6aa);
          seedWithArray(data, source);
          return next;
        };
        next.autoSeed = function () {
          return next.seedWithArray(Random.generateEntropyArray());
        };
        return next;
      }

      return mt19937;
    }(typeof Int32Array === "function" ? Int32Array : Array)),
    browserCrypto: (typeof crypto !== "undefined" && typeof crypto.getRandomValues === "function" && typeof Int32Array === "function") ? (function () {
      var data = null;
      var index = 128;

      return function () {
        if (index >= 128) {
          if (data === null) {
            data = new Int32Array(128);
          }
          crypto.getRandomValues(data);
          index = 0;
        }

        return data[index++] | 0;
      };
    }()) : null
  };

  Random.generateEntropyArray = function () {
    var array = [];
    var engine = Random.engines.nativeMath;
    for (var i = 0; i < 16; ++i) {
      array[i] = engine() | 0;
    }
    array.push(new Date().getTime() | 0);
    return array;
  };

  function returnValue(value) {
    return function () {
      return value;
    };
  }

  // [-0x80000000, 0x7fffffff]
  Random.int32 = function (engine) {
    return engine() | 0;
  };
  proto.int32 = function () {
    return Random.int32(this.engine);
  };

  // [0, 0xffffffff]
  Random.uint32 = function (engine) {
    return engine() >>> 0;
  };
  proto.uint32 = function () {
    return Random.uint32(this.engine);
  };

  // [0, 0x1fffffffffffff]
  Random.uint53 = function (engine) {
    var high = engine() & 0x1fffff;
    var low = engine() >>> 0;
    return (high * 0x100000000) + low;
  };
  proto.uint53 = function () {
    return Random.uint53(this.engine);
  };

  // [0, 0x20000000000000]
  Random.uint53Full = function (engine) {
    while (true) {
      var high = engine() | 0;
      if (high & 0x200000) {
        if ((high & 0x3fffff) === 0x200000 && (engine() | 0) === 0) {
          return 0x20000000000000;
        }
      } else {
        var low = engine() >>> 0;
        return ((high & 0x1fffff) * 0x100000000) + low;
      }
    }
  };
  proto.uint53Full = function () {
    return Random.uint53Full(this.engine);
  };

  // [-0x20000000000000, 0x1fffffffffffff]
  Random.int53 = function (engine) {
    var high = engine() | 0;
    var low = engine() >>> 0;
    return ((high & 0x1fffff) * 0x100000000) + low + (high & 0x200000 ? -0x20000000000000 : 0);
  };
  proto.int53 = function () {
    return Random.int53(this.engine);
  };

  // [-0x20000000000000, 0x20000000000000]
  Random.int53Full = function (engine) {
    while (true) {
      var high = engine() | 0;
      if (high & 0x400000) {
        if ((high & 0x7fffff) === 0x400000 && (engine() | 0) === 0) {
          return 0x20000000000000;
        }
      } else {
        var low = engine() >>> 0;
        return ((high & 0x1fffff) * 0x100000000) + low + (high & 0x200000 ? -0x20000000000000 : 0);
      }
    }
  };
  proto.int53Full = function () {
    return Random.int53Full(this.engine);
  };

  function add(generate, addend) {
    if (addend === 0) {
      return generate;
    } else {
      return function (engine) {
        return generate(engine) + addend;
      };
    }
  }

  Random.integer = (function () {
    function isPowerOfTwoMinusOne(value) {
      return ((value + 1) & value) === 0;
    }

    function bitmask(masking) {
      return function (engine) {
        return engine() & masking;
      };
    }

    function downscaleToLoopCheckedRange(range) {
      var extendedRange = range + 1;
      var maximum = extendedRange * Math.floor(0x100000000 / extendedRange);
      return function (engine) {
        var value = 0;
        do {
          value = engine() >>> 0;
        } while (value >= maximum);
        return value % extendedRange;
      };
    }

    function downscaleToRange(range) {
      if (isPowerOfTwoMinusOne(range)) {
        return bitmask(range);
      } else {
        return downscaleToLoopCheckedRange(range);
      }
    }

    function isEvenlyDivisibleByMaxInt32(value) {
      return (value | 0) === 0;
    }

    function upscaleWithHighMasking(masking) {
      return function (engine) {
        var high = engine() & masking;
        var low = engine() >>> 0;
        return (high * 0x100000000) + low;
      };
    }

    function upscaleToLoopCheckedRange(extendedRange) {
      var maximum = extendedRange * Math.floor(0x20000000000000 / extendedRange);
      return function (engine) {
        var ret = 0;
        do {
          var high = engine() & 0x1fffff;
          var low = engine() >>> 0;
          ret = (high * 0x100000000) + low;
        } while (ret >= maximum);
        return ret % extendedRange;
      };
    }

    function upscaleWithinU53(range) {
      var extendedRange = range + 1;
      if (isEvenlyDivisibleByMaxInt32(extendedRange)) {
        var highRange = ((extendedRange / 0x100000000) | 0) - 1;
        if (isPowerOfTwoMinusOne(highRange)) {
          return upscaleWithHighMasking(highRange);
        }
      }
      return upscaleToLoopCheckedRange(extendedRange);
    }

    function upscaleWithinI53AndLoopCheck(min, max) {
      return function (engine) {
        var ret = 0;
        do {
          var high = engine() | 0;
          var low = engine() >>> 0;
          ret = ((high & 0x1fffff) * 0x100000000) + low + (high & 0x200000 ? -0x20000000000000 : 0);
        } while (ret < min || ret > max);
        return ret;
      };
    }

    return function (min, max) {
      min = Math.floor(min);
      max = Math.floor(max);
      if (min < -0x20000000000000 || !isFinite(min)) {
        throw new RangeError("Expected min to be at least " + (-0x20000000000000));
      } else if (max > 0x20000000000000 || !isFinite(max)) {
        throw new RangeError("Expected max to be at most " + 0x20000000000000);
      }

      var range = max - min;
      if (range <= 0 || !isFinite(range)) {
        return returnValue(min);
      } else if (range === 0xffffffff) {
        if (min === 0) {
          return Random.uint32;
        } else {
          return add(Random.int32, min + 0x80000000);
        }
      } else if (range < 0xffffffff) {
        return add(downscaleToRange(range), min);
      } else if (range === 0x1fffffffffffff) {
        return add(Random.uint53, min);
      } else if (range < 0x1fffffffffffff) {
        return add(upscaleWithinU53(range), min);
      } else if (max - 1 - min === 0x1fffffffffffff) {
        return add(Random.uint53Full, min);
      } else if (min === -0x20000000000000 && max === 0x20000000000000) {
        return Random.int53Full;
      } else if (min === -0x20000000000000 && max === 0x1fffffffffffff) {
        return Random.int53;
      } else if (min === -0x1fffffffffffff && max === 0x20000000000000) {
        return add(Random.int53, 1);
      } else if (max === 0x20000000000000) {
        return add(upscaleWithinI53AndLoopCheck(min - 1, max - 1), 1);
      } else {
        return upscaleWithinI53AndLoopCheck(min, max);
      }
    };
  }());
  proto.integer = function (min, max) {
    return Random.integer(min, max)(this.engine);
  };

  // [0, 1] (floating point)
  Random.realZeroToOneInclusive = function (engine) {
    return Random.uint53Full(engine) / 0x20000000000000;
  };
  proto.realZeroToOneInclusive = function () {
    return Random.realZeroToOneInclusive(this.engine);
  };

  // [0, 1) (floating point)
  Random.realZeroToOneExclusive = function (engine) {
    return Random.uint53(engine) / 0x20000000000000;
  };
  proto.realZeroToOneExclusive = function () {
    return Random.realZeroToOneExclusive(this.engine);
  };

  Random.real = (function () {
    function multiply(generate, multiplier) {
      if (multiplier === 1) {
        return generate;
      } else if (multiplier === 0) {
        return function () {
          return 0;
        };
      } else {
        return function (engine) {
          return generate(engine) * multiplier;
        };
      }
    }

    return function (left, right, inclusive) {
      if (!isFinite(left)) {
        throw new RangeError("Expected left to be a finite number");
      } else if (!isFinite(right)) {
        throw new RangeError("Expected right to be a finite number");
      }
      return add(
        multiply(
          inclusive ? Random.realZeroToOneInclusive : Random.realZeroToOneExclusive,
          right - left),
        left);
    };
  }());
  proto.real = function (min, max, inclusive) {
    return Random.real(min, max, inclusive)(this.engine);
  };

  Random.bool = (function () {
    function isLeastBitTrue(engine) {
      return (engine() & 1) === 1;
    }

    function lessThan(generate, value) {
      return function (engine) {
        return generate(engine) < value;
      };
    }

    function probability(percentage) {
      if (percentage <= 0) {
        return returnValue(false);
      } else if (percentage >= 1) {
        return returnValue(true);
      } else {
        var scaled = percentage * 0x100000000;
        if (scaled % 1 === 0) {
          return lessThan(Random.int32, (scaled - 0x80000000) | 0);
        } else {
          return lessThan(Random.uint53, Math.round(percentage * 0x20000000000000));
        }
      }
    }

    return function (numerator, denominator) {
      if (denominator == null) {
        if (numerator == null) {
          return isLeastBitTrue;
        }
        return probability(numerator);
      } else {
        if (numerator <= 0) {
          return returnValue(false);
        } else if (numerator >= denominator) {
          return returnValue(true);
        }
        return lessThan(Random.integer(0, denominator - 1), numerator);
      }
    };
  }());
  proto.bool = function (numerator, denominator) {
    return Random.bool(numerator, denominator)(this.engine);
  };

  function toInteger(value) {
    var number = +value;
    if (number < 0) {
      return Math.ceil(number);
    } else {
      return Math.floor(number);
    }
  }

  function convertSliceArgument(value, length) {
    if (value < 0) {
      return Math.max(value + length, 0);
    } else {
      return Math.min(value, length);
    }
  }
  Random.pick = function (engine, array, begin, end) {
    var length = array.length;
    var start = begin == null ? 0 : convertSliceArgument(toInteger(begin), length);
    var finish = end === void 0 ? length : convertSliceArgument(toInteger(end), length);
    if (start >= finish) {
      return void 0;
    }
    var distribution = Random.integer(start, finish - 1);
    return array[distribution(engine)];
  };
  proto.pick = function (array, begin, end) {
    return Random.pick(this.engine, array, begin, end);
  };

  function returnUndefined() {
    return void 0;
  }
  var slice = Array.prototype.slice;
  Random.picker = function (array, begin, end) {
    var clone = slice.call(array, begin, end);
    if (!clone.length) {
      return returnUndefined;
    }
    var distribution = Random.integer(0, clone.length - 1);
    return function (engine) {
      return clone[distribution(engine)];
    };
  };

  Random.shuffle = function (engine, array, downTo) {
    var length = array.length;
    if (length) {
      if (downTo == null) {
        downTo = 0;
      }
      for (var i = (length - 1) >>> 0; i > downTo; --i) {
        var distribution = Random.integer(0, i);
        var j = distribution(engine);
        if (i !== j) {
          var tmp = array[i];
          array[i] = array[j];
          array[j] = tmp;
        }
      }
    }
    return array;
  };
  proto.shuffle = function (array) {
    return Random.shuffle(this.engine, array);
  };

  Random.sample = function (engine, population, sampleSize) {
    if (sampleSize < 0 || sampleSize > population.length || !isFinite(sampleSize)) {
      throw new RangeError("Expected sampleSize to be within 0 and the length of the population");
    }

    if (sampleSize === 0) {
      return [];
    }

    var clone = slice.call(population);
    var length = clone.length;
    if (length === sampleSize) {
      return Random.shuffle(engine, clone, 0);
    }
    var tailLength = length - sampleSize;
    return Random.shuffle(engine, clone, tailLength - 1).slice(tailLength);
  };
  proto.sample = function (population, sampleSize) {
    return Random.sample(this.engine, population, sampleSize);
  };

  Random.die = function (sideCount) {
    return Random.integer(1, sideCount);
  };
  proto.die = function (sideCount) {
    return Random.die(sideCount)(this.engine);
  };

  Random.dice = function (sideCount, dieCount) {
    var distribution = Random.die(sideCount);
    return function (engine) {
      var result = [];
      result.length = dieCount;
      for (var i = 0; i < dieCount; ++i) {
        result[i] = distribution(engine);
      }
      return result;
    };
  };
  proto.dice = function (sideCount, dieCount) {
    return Random.dice(sideCount, dieCount)(this.engine);
  };

  // http://en.wikipedia.org/wiki/Universally_unique_identifier
  Random.uuid4 = (function () {
    function zeroPad(string, zeroCount) {
      return stringRepeat("0", zeroCount - string.length) + string;
    }

    return function (engine) {
      var a = engine() >>> 0;
      var b = engine() | 0;
      var c = engine() | 0;
      var d = engine() >>> 0;

      return (
        zeroPad(a.toString(16), 8) +
        "-" +
        zeroPad((b & 0xffff).toString(16), 4) +
        "-" +
        zeroPad((((b >> 4) & 0x0fff) | 0x4000).toString(16), 4) +
        "-" +
        zeroPad(((c & 0x3fff) | 0x8000).toString(16), 4) +
        "-" +
        zeroPad(((c >> 4) & 0xffff).toString(16), 4) +
        zeroPad(d.toString(16), 8));
    };
  }());
  proto.uuid4 = function () {
    return Random.uuid4(this.engine);
  };

  Random.string = (function () {
    // has 2**x chars, for faster uniform distribution
    var DEFAULT_STRING_POOL = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-";

    return function (pool) {
      if (pool == null) {
        pool = DEFAULT_STRING_POOL;
      }

      var length = pool.length;
      if (!length) {
        throw new Error("Expected pool not to be an empty string");
      }

      var distribution = Random.integer(0, length - 1);
      return function (engine, length) {
        var result = "";
        for (var i = 0; i < length; ++i) {
          var j = distribution(engine);
          result += pool.charAt(j);
        }
        return result;
      };
    };
  }());
  proto.string = function (length, pool) {
    return Random.string(pool)(this.engine, length);
  };

  Random.hex = (function () {
    var LOWER_HEX_POOL = "0123456789abcdef";
    var lowerHex = Random.string(LOWER_HEX_POOL);
    var upperHex = Random.string(LOWER_HEX_POOL.toUpperCase());

    return function (upper) {
      if (upper) {
        return upperHex;
      } else {
        return lowerHex;
      }
    };
  }());
  proto.hex = function (length, upper) {
    return Random.hex(upper)(this.engine, length);
  };

  Random.date = function (start, end) {
    if (!(start instanceof Date)) {
      throw new TypeError("Expected start to be a Date, got " + typeof start);
    } else if (!(end instanceof Date)) {
      throw new TypeError("Expected end to be a Date, got " + typeof end);
    }
    var distribution = Random.integer(start.getTime(), end.getTime());
    return function (engine) {
      return new Date(distribution(engine));
    };
  };
  proto.date = function (start, end) {
    return Random.date(start, end)(this.engine);
  };

  if (typeof define === "function" && define.amd) {
    define(function () {
      return Random;
    });
  } else if (typeof module !== "undefined" && typeof require === "function") {
    module.exports = Random;
  } else {
    (function () {
      var oldGlobal = root[GLOBAL_KEY];
      Random.noConflict = function () {
        root[GLOBAL_KEY] = oldGlobal;
        return this;
      };
    }());
    root[GLOBAL_KEY] = Random;
  }
}(this));
},{}],2:[function(require,module,exports){
(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
	typeof define === 'function' && define.amd ? define(['exports'], factory) :
	(factory((global.redom = global.redom || {})));
}(this, (function (exports) { 'use strict';

var HASH = '#'.charCodeAt(0);
var DOT = '.'.charCodeAt(0);

function createElement (query, ns) {
  var tag;
  var id;
  var className;

  var mode = 0;
  var start = 0;

  for (var i = 0; i <= query.length; i++) {
    var char = query.charCodeAt(i);

    if (char === HASH || char === DOT || !char) {
      if (mode === 0) {
        if (i === 0) {
          tag = 'div';
        } else if (!char) {
          tag = query;
        } else {
          tag = query.substring(start, i);
        }
      } else {
        var slice = query.substring(start, i);

        if (mode === 1) {
          id = slice;
        } else if (className) {
          className += ' ' + slice;
        } else {
          className = slice;
        }
      }

      start = i + 1;

      if (char === HASH) {
        mode = 1;
      } else {
        mode = 2;
      }
    }
  }

  var element = ns ? document.createElementNS(ns, tag) : document.createElement(tag);

  if (id) {
    element.id = id;
  }

  if (className) {
    if (ns) {
      element.setAttribute('class', className);
    } else {
      element.className = className;
    }
  }

  return element;
}

var hookNames = ['onmount', 'onunmount'];

function mount (parent, child, before) {
  var parentEl = getEl(parent);
  var childEl = getEl(child);

  if (child === childEl && childEl.__redom_view) {
    // try to look up the view if not provided
    child = childEl.__redom_view;
  }

  if (child !== childEl) {
    childEl.__redom_view = child;
  }

  var wasMounted = childEl.__redom_mounted;
  var oldParent = childEl.parentNode;

  if (wasMounted && (oldParent !== parentEl)) {
    doUnmount(child, childEl, oldParent);
  }

  if (before != null) {
    parentEl.insertBefore(childEl, getEl(before));
  } else {
    parentEl.appendChild(childEl);
  }

  doMount(child, childEl, parentEl, oldParent);

  return child;
}

function unmount (parent, child) {
  var parentEl = getEl(parent);
  var childEl = getEl(child);

  if (child === childEl && childEl.__redom_view) {
    // try to look up the view if not provided
    child = childEl.__redom_view;
  }

  doUnmount(child, childEl, parentEl);

  parentEl.removeChild(childEl);

  return child;
}

function doMount (child, childEl, parentEl, oldParent) {
  var hooks = childEl.__redom_lifecycle || (childEl.__redom_lifecycle = {});
  var remount = (parentEl === oldParent);
  var hooksFound = false;

  for (var i = 0; i < hookNames.length; i++) {
    var hookName = hookNames[i];

    if (!remount && (child !== childEl) && (hookName in child)) {
      hooks[hookName] = (hooks[hookName] || 0) + 1;
    }
    if (hooks[hookName]) {
      hooksFound = true;
    }
  }

  if (!hooksFound) {
    childEl.__redom_mounted = true;
    return;
  }

  var traverse = parentEl;
  var triggered = false;

  if (remount || (!triggered && (traverse && traverse.__redom_mounted))) {
    trigger(childEl, remount ? 'onremount' : 'onmount');
    triggered = true;
  }

  if (remount) {
    return;
  }

  while (traverse) {
    var parent = traverse.parentNode;
    var parentHooks = traverse.__redom_lifecycle || (traverse.__redom_lifecycle = {});

    for (var hook in hooks) {
      parentHooks[hook] = (parentHooks[hook] || 0) + hooks[hook];
    }

    if (!triggered && (traverse === document || (parent && parent.__redom_mounted))) {
      trigger(traverse, remount ? 'onremount' : 'onmount');
      triggered = true;
    }

    traverse = parent;
  }
}

function doUnmount (child, childEl, parentEl) {
  var hooks = childEl.__redom_lifecycle;

  if (!hooks) {
    childEl.__redom_mounted = false;
    return;
  }

  var traverse = parentEl;

  if (childEl.__redom_mounted) {
    trigger(childEl, 'onunmount');
  }

  while (traverse) {
    var parentHooks = traverse.__redom_lifecycle || (traverse.__redom_lifecycle = {});
    var hooksFound = false;

    for (var hook in hooks) {
      if (parentHooks[hook]) {
        parentHooks[hook] -= hooks[hook];
      }
      if (parentHooks[hook]) {
        hooksFound = true;
      }
    }

    if (!hooksFound) {
      traverse.__redom_lifecycle = null;
    }

    traverse = traverse.parentNode;
  }
}

function trigger (el, eventName) {
  if (eventName === 'onmount') {
    el.__redom_mounted = true;
  } else if (eventName === 'onunmount') {
    el.__redom_mounted = false;
  }

  var hooks = el.__redom_lifecycle;

  if (!hooks) {
    return;
  }

  var view = el.__redom_view;
  var hookCount = 0;

  view && view[eventName] && view[eventName]();

  for (var hook in hooks) {
    if (hook) {
      hookCount++;
    }
  }

  if (hookCount) {
    var traverse = el.firstChild;

    while (traverse) {
      var next = traverse.nextSibling;

      trigger(traverse, eventName);

      traverse = next;
    }
  }
}

function setStyle (view, arg1, arg2) {
  var el = getEl(view);

  if (arg2 !== undefined) {
    el.style[arg1] = arg2;
  } else if (isString(arg1)) {
    el.setAttribute('style', arg1);
  } else {
    for (var key in arg1) {
      setStyle(el, key, arg1[key]);
    }
  }
}

function setAttr (view, arg1, arg2) {
  var el = getEl(view);
  var isSVG = el instanceof window.SVGElement;

  if (arg2 !== undefined) {
    if (arg1 === 'style') {
      setStyle(el, arg2);
    } else if (isSVG && isFunction(arg2)) {
      el[arg1] = arg2;
    } else if (!isSVG && (arg1 in el || isFunction(arg2))) {
      el[arg1] = arg2;
    } else {
      el.setAttribute(arg1, arg2);
    }
  } else {
    for (var key in arg1) {
      setAttr(el, key, arg1[key]);
    }
  }
}

var text = function (str) { return document.createTextNode(str); };

function parseArguments (element, args) {
  for (var i = 0; i < args.length; i++) {
    var arg = args[i];

    if (arg !== 0 && !arg) {
      continue;
    }

    // support middleware
    if (typeof arg === 'function') {
      arg(element);
    } else if (isString(arg) || isNumber(arg)) {
      element.appendChild(text(arg));
    } else if (isNode(getEl(arg))) {
      mount(element, arg);
    } else if (arg.length) {
      parseArguments(element, arg);
    } else if (typeof arg === 'object') {
      setAttr(element, arg);
    }
  }
}

var ensureEl = function (parent) { return isString(parent) ? html(parent) : getEl(parent); };
var getEl = function (parent) { return (parent.nodeType && parent) || (!parent.el && parent) || getEl(parent.el); };

var isString = function (a) { return typeof a === 'string'; };
var isNumber = function (a) { return typeof a === 'number'; };
var isFunction = function (a) { return typeof a === 'function'; };

var isNode = function (a) { return a && a.nodeType; };

var htmlCache = {};

var memoizeHTML = function (query) { return htmlCache[query] || (htmlCache[query] = createElement(query)); };

function html (query) {
  var args = [], len = arguments.length - 1;
  while ( len-- > 0 ) args[ len ] = arguments[ len + 1 ];

  var element;

  if (isString(query)) {
    element = memoizeHTML(query).cloneNode(false);
  } else if (isNode(query)) {
    element = query.cloneNode(false);
  } else {
    throw new Error('At least one argument required');
  }

  parseArguments(element, args);

  return element;
}

html.extend = function (query) {
  var clone = memoizeHTML(query);

  return html.bind(this, clone);
};

var el = html;

function setChildren (parent, children) {
  if (children.length === undefined) {
    return setChildren(parent, [children]);
  }

  var parentEl = getEl(parent);
  var traverse = parentEl.firstChild;

  for (var i = 0; i < children.length; i++) {
    var child = children[i];

    if (!child) {
      continue;
    }

    var childEl = getEl(child);

    if (childEl === traverse) {
      traverse = traverse.nextSibling;
      continue;
    }

    mount(parent, child, traverse);
  }

  while (traverse) {
    var next = traverse.nextSibling;

    unmount(parent, traverse);

    traverse = next;
  }
}

var propKey = function (key) { return function (item) { return item[key]; }; };

function list (parent, View, key, initData) {
  return new List(parent, View, key, initData);
}

function List (parent, View, key, initData) {
  this.__redom_list = true;
  this.View = View;
  this.initData = initData;
  this.views = [];
  this.el = ensureEl(parent);

  if (key != null) {
    this.lookup = {};
    this.key = isFunction(key) ? key : propKey(key);
  }
}

List.extend = function (parent, View, key, initData) {
  return List.bind(List, parent, View, key, initData);
};

list.extend = List.extend;

List.prototype.update = function (data) {
  var this$1 = this;
  if ( data === void 0 ) data = [];

  var View = this.View;
  var key = this.key;
  var keySet = key != null;
  var initData = this.initData;
  var newViews = new Array(data.length);
  var oldViews = this.views;
  var newLookup = key && {};
  var oldLookup = key && this.lookup;

  for (var i = 0; i < data.length; i++) {
    var item = data[i];
    var view = (void 0);

    if (keySet) {
      var id = key(item);
      view = newViews[i] = oldLookup[id] || new View(initData, item, i, data);
      newLookup[id] = view;
      view.__id = id;
    } else {
      view = newViews[i] = oldViews[i] || new View(initData, item, i, data);
    }
    var el = view.el;
    if (el.__redom_list) {
      el = el.el;
    }
    el.__redom_view = view;
    view.update && view.update(item, i, data);
  }

  if (keySet) {
    for (var i$1 = 0; i$1 < oldViews.length; i$1++) {
      var id$1 = oldViews[i$1].__id;

      if (!(id$1 in newLookup)) {
        unmount(this$1, oldLookup[id$1]);
      }
    }
  }

  setChildren(this, newViews);

  if (keySet) {
    this.lookup = newLookup;
  }
  this.views = newViews;
};

function router (parent, Views, initData) {
  return new Router(parent, Views, initData);
}

var Router = function Router (parent, Views, initData) {
  this.el = ensureEl(parent);
  this.Views = Views;
  this.initData = initData;
};
Router.prototype.update = function update (route, data) {
  if (route !== this.route) {
    var Views = this.Views;
    var View = Views[route];

    this.view = View && new View(this.initData, data);
    this.route = route;

    setChildren(this.el, [ this.view ]);
  }
  this.view && this.view.update && this.view.update(data, route);
};

var SVG = 'http://www.w3.org/2000/svg';

var svgCache = {};

var memoizeSVG = function (query) { return svgCache[query] || (svgCache[query] = createElement(query, SVG)); };

function svg (query) {
  var args = [], len = arguments.length - 1;
  while ( len-- > 0 ) args[ len ] = arguments[ len + 1 ];

  var element;

  if (isString(query)) {
    element = memoizeSVG(query).cloneNode(false);
  } else if (isNode(query)) {
    element = query.cloneNode(false);
  } else {
    throw new Error('At least one argument required');
  }

  parseArguments(element, args);

  return element;
}

svg.extend = function (query) {
  var clone = memoizeSVG(query);

  return svg.bind(this, clone);
};

exports.html = html;
exports.el = el;
exports.list = list;
exports.List = List;
exports.mount = mount;
exports.unmount = unmount;
exports.router = router;
exports.Router = Router;
exports.setAttr = setAttr;
exports.setStyle = setStyle;
exports.setChildren = setChildren;
exports.svg = svg;
exports.text = text;

Object.defineProperty(exports, '__esModule', { value: true });

})));

},{}],3:[function(require,module,exports){
const { el, mount, list, unmount}  = require('redom')
const Random = require('random-js')

let random = new Random(Random.engines.browserCrypto)

class Item {
    constructor(a,b,c,d){
        
        this.remove = this.remove.bind(this)
        this.el = el('.item',
            this.itemName = el('.item-name'),
            this.itemDelete = el('.item-delete', 'x')
        )

        this.itemDelete.onclick = this.remove
    }

    remove(){
        const { id } = this.data;
        const event = new CustomEvent('delete_item', { detail: id, bubbles: true })
        this.el.dispatchEvent(event)
    }

    update(data){
        this.itemName.textContent = this.name = data.name
        this.data = data
    }
}

class ItemsList{
    constructor(){
        this.el = el('.item-container')
        this.list = list(this.el, Item, 'id')
        this.id = 0
        this.items = []

        this.el.addEventListener('delete_item', e => {
            this.items = this.items.filter(el=>el.id !== e.detail)
            this.list.update(this.items)
        })
    }

    update(data){
        if(data.action === 'add_item' && data.name){
            this.items.push({
                id : this.id++,
                name : data.name
            })
            this.list.update(this.items)
        }
    }
}

class ItemInput {
    constructor(addItem){
        this.pop = this.pop.bind(this)

        this.el = el('form.item-input',
            this.newItemName = el('input', {type:'text', autofocus: true}),
            this.addItemBtn = el('input', {type:'submit', value:'Add Item'}),
            el('label', 'Add one or many items separated by commas')
        )

        this.addItemBtn.onclick = addItem
        this.el.onsubmit = addItem
    }

    pop(){
        let value = this.newItemName.value
        this.newItemName.value = ""
        this.newItemName.focus()
        return value
    }
}

class ItemPicker {
    constructor(){  
        this.addItem = this.addItem.bind(this)

        this.el = el('div.global-container',
            el('div.header',
                this.newItemForm = new ItemInput(this.addItem)
            ),
            this.items = new ItemsList(),
            el('label', this.removePicked = el('input', {type:"checkbox", checked: false}), 'Remove picked'),
            this.pickRandom = el('button', 'Pick Random'),
            this.pickedItem = el('.picked-item')
        )

        this.pickRandom.onclick = (e)=>{
            let pickedItem = random.pick(this.items.list.views)
            let name = (pickedItem || {}).name
            if(pickedItem && this.removePicked.checked){
                pickedItem.remove()
            }
            this.pickedItem.textContent = name
        }
    }

    addItem(e){
        e.preventDefault()
        this
        .newItemForm
        .pop()
        .split(',')
        .forEach(item=>{
            this.items.update({
                action : 'add_item',
                name : item.trim()
            })
        })
        
    }
}

document.addEventListener('DOMContentLoaded', main)
function main(){
    mount(document.body, new ItemPicker())
}
},{"random-js":1,"redom":2}]},{},[3])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy5udm0vdmVyc2lvbnMvbm9kZS92Ny44LjAvbGliL25vZGVfbW9kdWxlcy93YXRjaGlmeS9ub2RlX21vZHVsZXMvYnJvd3Nlci1wYWNrL19wcmVsdWRlLmpzIiwibm9kZV9tb2R1bGVzL3JhbmRvbS1qcy9saWIvcmFuZG9tLmpzIiwibm9kZV9tb2R1bGVzL3JlZG9tL2Rpc3QvcmVkb20uanMiLCJzcmMvaW5kZXguanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzNzQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDdmdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsIi8qanNoaW50IGVxbnVsbDp0cnVlKi9cbihmdW5jdGlvbiAocm9vdCkge1xuICBcInVzZSBzdHJpY3RcIjtcblxuICB2YXIgR0xPQkFMX0tFWSA9IFwiUmFuZG9tXCI7XG5cbiAgdmFyIGltdWwgPSAodHlwZW9mIE1hdGguaW11bCAhPT0gXCJmdW5jdGlvblwiIHx8IE1hdGguaW11bCgweGZmZmZmZmZmLCA1KSAhPT0gLTUgP1xuICAgIGZ1bmN0aW9uIChhLCBiKSB7XG4gICAgICB2YXIgYWggPSAoYSA+Pj4gMTYpICYgMHhmZmZmO1xuICAgICAgdmFyIGFsID0gYSAmIDB4ZmZmZjtcbiAgICAgIHZhciBiaCA9IChiID4+PiAxNikgJiAweGZmZmY7XG4gICAgICB2YXIgYmwgPSBiICYgMHhmZmZmO1xuICAgICAgLy8gdGhlIHNoaWZ0IGJ5IDAgZml4ZXMgdGhlIHNpZ24gb24gdGhlIGhpZ2ggcGFydFxuICAgICAgLy8gdGhlIGZpbmFsIHwwIGNvbnZlcnRzIHRoZSB1bnNpZ25lZCB2YWx1ZSBpbnRvIGEgc2lnbmVkIHZhbHVlXG4gICAgICByZXR1cm4gKGFsICogYmwpICsgKCgoYWggKiBibCArIGFsICogYmgpIDw8IDE2KSA+Pj4gMCkgfCAwO1xuICAgIH0gOlxuICAgIE1hdGguaW11bCk7XG5cbiAgdmFyIHN0cmluZ1JlcGVhdCA9ICh0eXBlb2YgU3RyaW5nLnByb3RvdHlwZS5yZXBlYXQgPT09IFwiZnVuY3Rpb25cIiAmJiBcInhcIi5yZXBlYXQoMykgPT09IFwieHh4XCIgP1xuICAgIGZ1bmN0aW9uICh4LCB5KSB7XG4gICAgICByZXR1cm4geC5yZXBlYXQoeSk7XG4gICAgfSA6IGZ1bmN0aW9uIChwYXR0ZXJuLCBjb3VudCkge1xuICAgICAgdmFyIHJlc3VsdCA9IFwiXCI7XG4gICAgICB3aGlsZSAoY291bnQgPiAwKSB7XG4gICAgICAgIGlmIChjb3VudCAmIDEpIHtcbiAgICAgICAgICByZXN1bHQgKz0gcGF0dGVybjtcbiAgICAgICAgfVxuICAgICAgICBjb3VudCA+Pj0gMTtcbiAgICAgICAgcGF0dGVybiArPSBwYXR0ZXJuO1xuICAgICAgfVxuICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICB9KTtcblxuICBmdW5jdGlvbiBSYW5kb20oZW5naW5lKSB7XG4gICAgaWYgKCEodGhpcyBpbnN0YW5jZW9mIFJhbmRvbSkpIHtcbiAgICAgIHJldHVybiBuZXcgUmFuZG9tKGVuZ2luZSk7XG4gICAgfVxuXG4gICAgaWYgKGVuZ2luZSA9PSBudWxsKSB7XG4gICAgICBlbmdpbmUgPSBSYW5kb20uZW5naW5lcy5uYXRpdmVNYXRoO1xuICAgIH0gZWxzZSBpZiAodHlwZW9mIGVuZ2luZSAhPT0gXCJmdW5jdGlvblwiKSB7XG4gICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKFwiRXhwZWN0ZWQgZW5naW5lIHRvIGJlIGEgZnVuY3Rpb24sIGdvdCBcIiArIHR5cGVvZiBlbmdpbmUpO1xuICAgIH1cbiAgICB0aGlzLmVuZ2luZSA9IGVuZ2luZTtcbiAgfVxuICB2YXIgcHJvdG8gPSBSYW5kb20ucHJvdG90eXBlO1xuXG4gIFJhbmRvbS5lbmdpbmVzID0ge1xuICAgIG5hdGl2ZU1hdGg6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHJldHVybiAoTWF0aC5yYW5kb20oKSAqIDB4MTAwMDAwMDAwKSB8IDA7XG4gICAgfSxcbiAgICBtdDE5OTM3OiAoZnVuY3Rpb24gKEludDMyQXJyYXkpIHtcbiAgICAgIC8vIGh0dHA6Ly9lbi53aWtpcGVkaWEub3JnL3dpa2kvTWVyc2VubmVfdHdpc3RlclxuICAgICAgZnVuY3Rpb24gcmVmcmVzaERhdGEoZGF0YSkge1xuICAgICAgICB2YXIgayA9IDA7XG4gICAgICAgIHZhciB0bXAgPSAwO1xuICAgICAgICBmb3IgKDtcbiAgICAgICAgICAoayB8IDApIDwgMjI3OyBrID0gKGsgKyAxKSB8IDApIHtcbiAgICAgICAgICB0bXAgPSAoZGF0YVtrXSAmIDB4ODAwMDAwMDApIHwgKGRhdGFbKGsgKyAxKSB8IDBdICYgMHg3ZmZmZmZmZik7XG4gICAgICAgICAgZGF0YVtrXSA9IGRhdGFbKGsgKyAzOTcpIHwgMF0gXiAodG1wID4+PiAxKSBeICgodG1wICYgMHgxKSA/IDB4OTkwOGIwZGYgOiAwKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGZvciAoO1xuICAgICAgICAgIChrIHwgMCkgPCA2MjM7IGsgPSAoayArIDEpIHwgMCkge1xuICAgICAgICAgIHRtcCA9IChkYXRhW2tdICYgMHg4MDAwMDAwMCkgfCAoZGF0YVsoayArIDEpIHwgMF0gJiAweDdmZmZmZmZmKTtcbiAgICAgICAgICBkYXRhW2tdID0gZGF0YVsoayAtIDIyNykgfCAwXSBeICh0bXAgPj4+IDEpIF4gKCh0bXAgJiAweDEpID8gMHg5OTA4YjBkZiA6IDApO1xuICAgICAgICB9XG5cbiAgICAgICAgdG1wID0gKGRhdGFbNjIzXSAmIDB4ODAwMDAwMDApIHwgKGRhdGFbMF0gJiAweDdmZmZmZmZmKTtcbiAgICAgICAgZGF0YVs2MjNdID0gZGF0YVszOTZdIF4gKHRtcCA+Pj4gMSkgXiAoKHRtcCAmIDB4MSkgPyAweDk5MDhiMGRmIDogMCk7XG4gICAgICB9XG5cbiAgICAgIGZ1bmN0aW9uIHRlbXBlcih2YWx1ZSkge1xuICAgICAgICB2YWx1ZSBePSB2YWx1ZSA+Pj4gMTE7XG4gICAgICAgIHZhbHVlIF49ICh2YWx1ZSA8PCA3KSAmIDB4OWQyYzU2ODA7XG4gICAgICAgIHZhbHVlIF49ICh2YWx1ZSA8PCAxNSkgJiAweGVmYzYwMDAwO1xuICAgICAgICByZXR1cm4gdmFsdWUgXiAodmFsdWUgPj4+IDE4KTtcbiAgICAgIH1cblxuICAgICAgZnVuY3Rpb24gc2VlZFdpdGhBcnJheShkYXRhLCBzb3VyY2UpIHtcbiAgICAgICAgdmFyIGkgPSAxO1xuICAgICAgICB2YXIgaiA9IDA7XG4gICAgICAgIHZhciBzb3VyY2VMZW5ndGggPSBzb3VyY2UubGVuZ3RoO1xuICAgICAgICB2YXIgayA9IE1hdGgubWF4KHNvdXJjZUxlbmd0aCwgNjI0KSB8IDA7XG4gICAgICAgIHZhciBwcmV2aW91cyA9IGRhdGFbMF0gfCAwO1xuICAgICAgICBmb3IgKDtcbiAgICAgICAgICAoayB8IDApID4gMDsgLS1rKSB7XG4gICAgICAgICAgZGF0YVtpXSA9IHByZXZpb3VzID0gKChkYXRhW2ldIF4gaW11bCgocHJldmlvdXMgXiAocHJldmlvdXMgPj4+IDMwKSksIDB4MDAxOTY2MGQpKSArIChzb3VyY2Vbal0gfCAwKSArIChqIHwgMCkpIHwgMDtcbiAgICAgICAgICBpID0gKGkgKyAxKSB8IDA7XG4gICAgICAgICAgKytqO1xuICAgICAgICAgIGlmICgoaSB8IDApID4gNjIzKSB7XG4gICAgICAgICAgICBkYXRhWzBdID0gZGF0YVs2MjNdO1xuICAgICAgICAgICAgaSA9IDE7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmIChqID49IHNvdXJjZUxlbmd0aCkge1xuICAgICAgICAgICAgaiA9IDA7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGZvciAoayA9IDYyMztcbiAgICAgICAgICAoayB8IDApID4gMDsgLS1rKSB7XG4gICAgICAgICAgZGF0YVtpXSA9IHByZXZpb3VzID0gKChkYXRhW2ldIF4gaW11bCgocHJldmlvdXMgXiAocHJldmlvdXMgPj4+IDMwKSksIDB4NWQ1ODhiNjUpKSAtIGkpIHwgMDtcbiAgICAgICAgICBpID0gKGkgKyAxKSB8IDA7XG4gICAgICAgICAgaWYgKChpIHwgMCkgPiA2MjMpIHtcbiAgICAgICAgICAgIGRhdGFbMF0gPSBkYXRhWzYyM107XG4gICAgICAgICAgICBpID0gMTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgZGF0YVswXSA9IDB4ODAwMDAwMDA7XG4gICAgICB9XG5cbiAgICAgIGZ1bmN0aW9uIG10MTk5MzcoKSB7XG4gICAgICAgIHZhciBkYXRhID0gbmV3IEludDMyQXJyYXkoNjI0KTtcbiAgICAgICAgdmFyIGluZGV4ID0gMDtcbiAgICAgICAgdmFyIHVzZXMgPSAwO1xuXG4gICAgICAgIGZ1bmN0aW9uIG5leHQoKSB7XG4gICAgICAgICAgaWYgKChpbmRleCB8IDApID49IDYyNCkge1xuICAgICAgICAgICAgcmVmcmVzaERhdGEoZGF0YSk7XG4gICAgICAgICAgICBpbmRleCA9IDA7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgdmFyIHZhbHVlID0gZGF0YVtpbmRleF07XG4gICAgICAgICAgaW5kZXggPSAoaW5kZXggKyAxKSB8IDA7XG4gICAgICAgICAgdXNlcyArPSAxO1xuICAgICAgICAgIHJldHVybiB0ZW1wZXIodmFsdWUpIHwgMDtcbiAgICAgICAgfVxuICAgICAgICBuZXh0LmdldFVzZUNvdW50ID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgcmV0dXJuIHVzZXM7XG4gICAgICAgIH07XG4gICAgICAgIG5leHQuZGlzY2FyZCA9IGZ1bmN0aW9uIChjb3VudCkge1xuICAgICAgICAgIHVzZXMgKz0gY291bnQ7XG4gICAgICAgICAgaWYgKChpbmRleCB8IDApID49IDYyNCkge1xuICAgICAgICAgICAgcmVmcmVzaERhdGEoZGF0YSk7XG4gICAgICAgICAgICBpbmRleCA9IDA7XG4gICAgICAgICAgfVxuICAgICAgICAgIHdoaWxlICgoY291bnQgLSBpbmRleCkgPiA2MjQpIHtcbiAgICAgICAgICAgIGNvdW50IC09IDYyNCAtIGluZGV4O1xuICAgICAgICAgICAgcmVmcmVzaERhdGEoZGF0YSk7XG4gICAgICAgICAgICBpbmRleCA9IDA7XG4gICAgICAgICAgfVxuICAgICAgICAgIGluZGV4ID0gKGluZGV4ICsgY291bnQpIHwgMDtcbiAgICAgICAgICByZXR1cm4gbmV4dDtcbiAgICAgICAgfTtcbiAgICAgICAgbmV4dC5zZWVkID0gZnVuY3Rpb24gKGluaXRpYWwpIHtcbiAgICAgICAgICB2YXIgcHJldmlvdXMgPSAwO1xuICAgICAgICAgIGRhdGFbMF0gPSBwcmV2aW91cyA9IGluaXRpYWwgfCAwO1xuXG4gICAgICAgICAgZm9yICh2YXIgaSA9IDE7IGkgPCA2MjQ7IGkgPSAoaSArIDEpIHwgMCkge1xuICAgICAgICAgICAgZGF0YVtpXSA9IHByZXZpb3VzID0gKGltdWwoKHByZXZpb3VzIF4gKHByZXZpb3VzID4+PiAzMCkpLCAweDZjMDc4OTY1KSArIGkpIHwgMDtcbiAgICAgICAgICB9XG4gICAgICAgICAgaW5kZXggPSA2MjQ7XG4gICAgICAgICAgdXNlcyA9IDA7XG4gICAgICAgICAgcmV0dXJuIG5leHQ7XG4gICAgICAgIH07XG4gICAgICAgIG5leHQuc2VlZFdpdGhBcnJheSA9IGZ1bmN0aW9uIChzb3VyY2UpIHtcbiAgICAgICAgICBuZXh0LnNlZWQoMHgwMTJiZDZhYSk7XG4gICAgICAgICAgc2VlZFdpdGhBcnJheShkYXRhLCBzb3VyY2UpO1xuICAgICAgICAgIHJldHVybiBuZXh0O1xuICAgICAgICB9O1xuICAgICAgICBuZXh0LmF1dG9TZWVkID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHJldHVybiBuZXh0LnNlZWRXaXRoQXJyYXkoUmFuZG9tLmdlbmVyYXRlRW50cm9weUFycmF5KCkpO1xuICAgICAgICB9O1xuICAgICAgICByZXR1cm4gbmV4dDtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIG10MTk5Mzc7XG4gICAgfSh0eXBlb2YgSW50MzJBcnJheSA9PT0gXCJmdW5jdGlvblwiID8gSW50MzJBcnJheSA6IEFycmF5KSksXG4gICAgYnJvd3NlckNyeXB0bzogKHR5cGVvZiBjcnlwdG8gIT09IFwidW5kZWZpbmVkXCIgJiYgdHlwZW9mIGNyeXB0by5nZXRSYW5kb21WYWx1ZXMgPT09IFwiZnVuY3Rpb25cIiAmJiB0eXBlb2YgSW50MzJBcnJheSA9PT0gXCJmdW5jdGlvblwiKSA/IChmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgZGF0YSA9IG51bGw7XG4gICAgICB2YXIgaW5kZXggPSAxMjg7XG5cbiAgICAgIHJldHVybiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmIChpbmRleCA+PSAxMjgpIHtcbiAgICAgICAgICBpZiAoZGF0YSA9PT0gbnVsbCkge1xuICAgICAgICAgICAgZGF0YSA9IG5ldyBJbnQzMkFycmF5KDEyOCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGNyeXB0by5nZXRSYW5kb21WYWx1ZXMoZGF0YSk7XG4gICAgICAgICAgaW5kZXggPSAwO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIGRhdGFbaW5kZXgrK10gfCAwO1xuICAgICAgfTtcbiAgICB9KCkpIDogbnVsbFxuICB9O1xuXG4gIFJhbmRvbS5nZW5lcmF0ZUVudHJvcHlBcnJheSA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgYXJyYXkgPSBbXTtcbiAgICB2YXIgZW5naW5lID0gUmFuZG9tLmVuZ2luZXMubmF0aXZlTWF0aDtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IDE2OyArK2kpIHtcbiAgICAgIGFycmF5W2ldID0gZW5naW5lKCkgfCAwO1xuICAgIH1cbiAgICBhcnJheS5wdXNoKG5ldyBEYXRlKCkuZ2V0VGltZSgpIHwgMCk7XG4gICAgcmV0dXJuIGFycmF5O1xuICB9O1xuXG4gIGZ1bmN0aW9uIHJldHVyblZhbHVlKHZhbHVlKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcbiAgICAgIHJldHVybiB2YWx1ZTtcbiAgICB9O1xuICB9XG5cbiAgLy8gWy0weDgwMDAwMDAwLCAweDdmZmZmZmZmXVxuICBSYW5kb20uaW50MzIgPSBmdW5jdGlvbiAoZW5naW5lKSB7XG4gICAgcmV0dXJuIGVuZ2luZSgpIHwgMDtcbiAgfTtcbiAgcHJvdG8uaW50MzIgPSBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIFJhbmRvbS5pbnQzMih0aGlzLmVuZ2luZSk7XG4gIH07XG5cbiAgLy8gWzAsIDB4ZmZmZmZmZmZdXG4gIFJhbmRvbS51aW50MzIgPSBmdW5jdGlvbiAoZW5naW5lKSB7XG4gICAgcmV0dXJuIGVuZ2luZSgpID4+PiAwO1xuICB9O1xuICBwcm90by51aW50MzIgPSBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIFJhbmRvbS51aW50MzIodGhpcy5lbmdpbmUpO1xuICB9O1xuXG4gIC8vIFswLCAweDFmZmZmZmZmZmZmZmZmXVxuICBSYW5kb20udWludDUzID0gZnVuY3Rpb24gKGVuZ2luZSkge1xuICAgIHZhciBoaWdoID0gZW5naW5lKCkgJiAweDFmZmZmZjtcbiAgICB2YXIgbG93ID0gZW5naW5lKCkgPj4+IDA7XG4gICAgcmV0dXJuIChoaWdoICogMHgxMDAwMDAwMDApICsgbG93O1xuICB9O1xuICBwcm90by51aW50NTMgPSBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIFJhbmRvbS51aW50NTModGhpcy5lbmdpbmUpO1xuICB9O1xuXG4gIC8vIFswLCAweDIwMDAwMDAwMDAwMDAwXVxuICBSYW5kb20udWludDUzRnVsbCA9IGZ1bmN0aW9uIChlbmdpbmUpIHtcbiAgICB3aGlsZSAodHJ1ZSkge1xuICAgICAgdmFyIGhpZ2ggPSBlbmdpbmUoKSB8IDA7XG4gICAgICBpZiAoaGlnaCAmIDB4MjAwMDAwKSB7XG4gICAgICAgIGlmICgoaGlnaCAmIDB4M2ZmZmZmKSA9PT0gMHgyMDAwMDAgJiYgKGVuZ2luZSgpIHwgMCkgPT09IDApIHtcbiAgICAgICAgICByZXR1cm4gMHgyMDAwMDAwMDAwMDAwMDtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdmFyIGxvdyA9IGVuZ2luZSgpID4+PiAwO1xuICAgICAgICByZXR1cm4gKChoaWdoICYgMHgxZmZmZmYpICogMHgxMDAwMDAwMDApICsgbG93O1xuICAgICAgfVxuICAgIH1cbiAgfTtcbiAgcHJvdG8udWludDUzRnVsbCA9IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gUmFuZG9tLnVpbnQ1M0Z1bGwodGhpcy5lbmdpbmUpO1xuICB9O1xuXG4gIC8vIFstMHgyMDAwMDAwMDAwMDAwMCwgMHgxZmZmZmZmZmZmZmZmZl1cbiAgUmFuZG9tLmludDUzID0gZnVuY3Rpb24gKGVuZ2luZSkge1xuICAgIHZhciBoaWdoID0gZW5naW5lKCkgfCAwO1xuICAgIHZhciBsb3cgPSBlbmdpbmUoKSA+Pj4gMDtcbiAgICByZXR1cm4gKChoaWdoICYgMHgxZmZmZmYpICogMHgxMDAwMDAwMDApICsgbG93ICsgKGhpZ2ggJiAweDIwMDAwMCA/IC0weDIwMDAwMDAwMDAwMDAwIDogMCk7XG4gIH07XG4gIHByb3RvLmludDUzID0gZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiBSYW5kb20uaW50NTModGhpcy5lbmdpbmUpO1xuICB9O1xuXG4gIC8vIFstMHgyMDAwMDAwMDAwMDAwMCwgMHgyMDAwMDAwMDAwMDAwMF1cbiAgUmFuZG9tLmludDUzRnVsbCA9IGZ1bmN0aW9uIChlbmdpbmUpIHtcbiAgICB3aGlsZSAodHJ1ZSkge1xuICAgICAgdmFyIGhpZ2ggPSBlbmdpbmUoKSB8IDA7XG4gICAgICBpZiAoaGlnaCAmIDB4NDAwMDAwKSB7XG4gICAgICAgIGlmICgoaGlnaCAmIDB4N2ZmZmZmKSA9PT0gMHg0MDAwMDAgJiYgKGVuZ2luZSgpIHwgMCkgPT09IDApIHtcbiAgICAgICAgICByZXR1cm4gMHgyMDAwMDAwMDAwMDAwMDtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdmFyIGxvdyA9IGVuZ2luZSgpID4+PiAwO1xuICAgICAgICByZXR1cm4gKChoaWdoICYgMHgxZmZmZmYpICogMHgxMDAwMDAwMDApICsgbG93ICsgKGhpZ2ggJiAweDIwMDAwMCA/IC0weDIwMDAwMDAwMDAwMDAwIDogMCk7XG4gICAgICB9XG4gICAgfVxuICB9O1xuICBwcm90by5pbnQ1M0Z1bGwgPSBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIFJhbmRvbS5pbnQ1M0Z1bGwodGhpcy5lbmdpbmUpO1xuICB9O1xuXG4gIGZ1bmN0aW9uIGFkZChnZW5lcmF0ZSwgYWRkZW5kKSB7XG4gICAgaWYgKGFkZGVuZCA9PT0gMCkge1xuICAgICAgcmV0dXJuIGdlbmVyYXRlO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gZnVuY3Rpb24gKGVuZ2luZSkge1xuICAgICAgICByZXR1cm4gZ2VuZXJhdGUoZW5naW5lKSArIGFkZGVuZDtcbiAgICAgIH07XG4gICAgfVxuICB9XG5cbiAgUmFuZG9tLmludGVnZXIgPSAoZnVuY3Rpb24gKCkge1xuICAgIGZ1bmN0aW9uIGlzUG93ZXJPZlR3b01pbnVzT25lKHZhbHVlKSB7XG4gICAgICByZXR1cm4gKCh2YWx1ZSArIDEpICYgdmFsdWUpID09PSAwO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGJpdG1hc2sobWFza2luZykge1xuICAgICAgcmV0dXJuIGZ1bmN0aW9uIChlbmdpbmUpIHtcbiAgICAgICAgcmV0dXJuIGVuZ2luZSgpICYgbWFza2luZztcbiAgICAgIH07XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZG93bnNjYWxlVG9Mb29wQ2hlY2tlZFJhbmdlKHJhbmdlKSB7XG4gICAgICB2YXIgZXh0ZW5kZWRSYW5nZSA9IHJhbmdlICsgMTtcbiAgICAgIHZhciBtYXhpbXVtID0gZXh0ZW5kZWRSYW5nZSAqIE1hdGguZmxvb3IoMHgxMDAwMDAwMDAgLyBleHRlbmRlZFJhbmdlKTtcbiAgICAgIHJldHVybiBmdW5jdGlvbiAoZW5naW5lKSB7XG4gICAgICAgIHZhciB2YWx1ZSA9IDA7XG4gICAgICAgIGRvIHtcbiAgICAgICAgICB2YWx1ZSA9IGVuZ2luZSgpID4+PiAwO1xuICAgICAgICB9IHdoaWxlICh2YWx1ZSA+PSBtYXhpbXVtKTtcbiAgICAgICAgcmV0dXJuIHZhbHVlICUgZXh0ZW5kZWRSYW5nZTtcbiAgICAgIH07XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZG93bnNjYWxlVG9SYW5nZShyYW5nZSkge1xuICAgICAgaWYgKGlzUG93ZXJPZlR3b01pbnVzT25lKHJhbmdlKSkge1xuICAgICAgICByZXR1cm4gYml0bWFzayhyYW5nZSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gZG93bnNjYWxlVG9Mb29wQ2hlY2tlZFJhbmdlKHJhbmdlKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBmdW5jdGlvbiBpc0V2ZW5seURpdmlzaWJsZUJ5TWF4SW50MzIodmFsdWUpIHtcbiAgICAgIHJldHVybiAodmFsdWUgfCAwKSA9PT0gMDtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiB1cHNjYWxlV2l0aEhpZ2hNYXNraW5nKG1hc2tpbmcpIHtcbiAgICAgIHJldHVybiBmdW5jdGlvbiAoZW5naW5lKSB7XG4gICAgICAgIHZhciBoaWdoID0gZW5naW5lKCkgJiBtYXNraW5nO1xuICAgICAgICB2YXIgbG93ID0gZW5naW5lKCkgPj4+IDA7XG4gICAgICAgIHJldHVybiAoaGlnaCAqIDB4MTAwMDAwMDAwKSArIGxvdztcbiAgICAgIH07XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gdXBzY2FsZVRvTG9vcENoZWNrZWRSYW5nZShleHRlbmRlZFJhbmdlKSB7XG4gICAgICB2YXIgbWF4aW11bSA9IGV4dGVuZGVkUmFuZ2UgKiBNYXRoLmZsb29yKDB4MjAwMDAwMDAwMDAwMDAgLyBleHRlbmRlZFJhbmdlKTtcbiAgICAgIHJldHVybiBmdW5jdGlvbiAoZW5naW5lKSB7XG4gICAgICAgIHZhciByZXQgPSAwO1xuICAgICAgICBkbyB7XG4gICAgICAgICAgdmFyIGhpZ2ggPSBlbmdpbmUoKSAmIDB4MWZmZmZmO1xuICAgICAgICAgIHZhciBsb3cgPSBlbmdpbmUoKSA+Pj4gMDtcbiAgICAgICAgICByZXQgPSAoaGlnaCAqIDB4MTAwMDAwMDAwKSArIGxvdztcbiAgICAgICAgfSB3aGlsZSAocmV0ID49IG1heGltdW0pO1xuICAgICAgICByZXR1cm4gcmV0ICUgZXh0ZW5kZWRSYW5nZTtcbiAgICAgIH07XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gdXBzY2FsZVdpdGhpblU1MyhyYW5nZSkge1xuICAgICAgdmFyIGV4dGVuZGVkUmFuZ2UgPSByYW5nZSArIDE7XG4gICAgICBpZiAoaXNFdmVubHlEaXZpc2libGVCeU1heEludDMyKGV4dGVuZGVkUmFuZ2UpKSB7XG4gICAgICAgIHZhciBoaWdoUmFuZ2UgPSAoKGV4dGVuZGVkUmFuZ2UgLyAweDEwMDAwMDAwMCkgfCAwKSAtIDE7XG4gICAgICAgIGlmIChpc1Bvd2VyT2ZUd29NaW51c09uZShoaWdoUmFuZ2UpKSB7XG4gICAgICAgICAgcmV0dXJuIHVwc2NhbGVXaXRoSGlnaE1hc2tpbmcoaGlnaFJhbmdlKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgcmV0dXJuIHVwc2NhbGVUb0xvb3BDaGVja2VkUmFuZ2UoZXh0ZW5kZWRSYW5nZSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gdXBzY2FsZVdpdGhpbkk1M0FuZExvb3BDaGVjayhtaW4sIG1heCkge1xuICAgICAgcmV0dXJuIGZ1bmN0aW9uIChlbmdpbmUpIHtcbiAgICAgICAgdmFyIHJldCA9IDA7XG4gICAgICAgIGRvIHtcbiAgICAgICAgICB2YXIgaGlnaCA9IGVuZ2luZSgpIHwgMDtcbiAgICAgICAgICB2YXIgbG93ID0gZW5naW5lKCkgPj4+IDA7XG4gICAgICAgICAgcmV0ID0gKChoaWdoICYgMHgxZmZmZmYpICogMHgxMDAwMDAwMDApICsgbG93ICsgKGhpZ2ggJiAweDIwMDAwMCA/IC0weDIwMDAwMDAwMDAwMDAwIDogMCk7XG4gICAgICAgIH0gd2hpbGUgKHJldCA8IG1pbiB8fCByZXQgPiBtYXgpO1xuICAgICAgICByZXR1cm4gcmV0O1xuICAgICAgfTtcbiAgICB9XG5cbiAgICByZXR1cm4gZnVuY3Rpb24gKG1pbiwgbWF4KSB7XG4gICAgICBtaW4gPSBNYXRoLmZsb29yKG1pbik7XG4gICAgICBtYXggPSBNYXRoLmZsb29yKG1heCk7XG4gICAgICBpZiAobWluIDwgLTB4MjAwMDAwMDAwMDAwMDAgfHwgIWlzRmluaXRlKG1pbikpIHtcbiAgICAgICAgdGhyb3cgbmV3IFJhbmdlRXJyb3IoXCJFeHBlY3RlZCBtaW4gdG8gYmUgYXQgbGVhc3QgXCIgKyAoLTB4MjAwMDAwMDAwMDAwMDApKTtcbiAgICAgIH0gZWxzZSBpZiAobWF4ID4gMHgyMDAwMDAwMDAwMDAwMCB8fCAhaXNGaW5pdGUobWF4KSkge1xuICAgICAgICB0aHJvdyBuZXcgUmFuZ2VFcnJvcihcIkV4cGVjdGVkIG1heCB0byBiZSBhdCBtb3N0IFwiICsgMHgyMDAwMDAwMDAwMDAwMCk7XG4gICAgICB9XG5cbiAgICAgIHZhciByYW5nZSA9IG1heCAtIG1pbjtcbiAgICAgIGlmIChyYW5nZSA8PSAwIHx8ICFpc0Zpbml0ZShyYW5nZSkpIHtcbiAgICAgICAgcmV0dXJuIHJldHVyblZhbHVlKG1pbik7XG4gICAgICB9IGVsc2UgaWYgKHJhbmdlID09PSAweGZmZmZmZmZmKSB7XG4gICAgICAgIGlmIChtaW4gPT09IDApIHtcbiAgICAgICAgICByZXR1cm4gUmFuZG9tLnVpbnQzMjtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICByZXR1cm4gYWRkKFJhbmRvbS5pbnQzMiwgbWluICsgMHg4MDAwMDAwMCk7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSBpZiAocmFuZ2UgPCAweGZmZmZmZmZmKSB7XG4gICAgICAgIHJldHVybiBhZGQoZG93bnNjYWxlVG9SYW5nZShyYW5nZSksIG1pbik7XG4gICAgICB9IGVsc2UgaWYgKHJhbmdlID09PSAweDFmZmZmZmZmZmZmZmZmKSB7XG4gICAgICAgIHJldHVybiBhZGQoUmFuZG9tLnVpbnQ1MywgbWluKTtcbiAgICAgIH0gZWxzZSBpZiAocmFuZ2UgPCAweDFmZmZmZmZmZmZmZmZmKSB7XG4gICAgICAgIHJldHVybiBhZGQodXBzY2FsZVdpdGhpblU1MyhyYW5nZSksIG1pbik7XG4gICAgICB9IGVsc2UgaWYgKG1heCAtIDEgLSBtaW4gPT09IDB4MWZmZmZmZmZmZmZmZmYpIHtcbiAgICAgICAgcmV0dXJuIGFkZChSYW5kb20udWludDUzRnVsbCwgbWluKTtcbiAgICAgIH0gZWxzZSBpZiAobWluID09PSAtMHgyMDAwMDAwMDAwMDAwMCAmJiBtYXggPT09IDB4MjAwMDAwMDAwMDAwMDApIHtcbiAgICAgICAgcmV0dXJuIFJhbmRvbS5pbnQ1M0Z1bGw7XG4gICAgICB9IGVsc2UgaWYgKG1pbiA9PT0gLTB4MjAwMDAwMDAwMDAwMDAgJiYgbWF4ID09PSAweDFmZmZmZmZmZmZmZmZmKSB7XG4gICAgICAgIHJldHVybiBSYW5kb20uaW50NTM7XG4gICAgICB9IGVsc2UgaWYgKG1pbiA9PT0gLTB4MWZmZmZmZmZmZmZmZmYgJiYgbWF4ID09PSAweDIwMDAwMDAwMDAwMDAwKSB7XG4gICAgICAgIHJldHVybiBhZGQoUmFuZG9tLmludDUzLCAxKTtcbiAgICAgIH0gZWxzZSBpZiAobWF4ID09PSAweDIwMDAwMDAwMDAwMDAwKSB7XG4gICAgICAgIHJldHVybiBhZGQodXBzY2FsZVdpdGhpbkk1M0FuZExvb3BDaGVjayhtaW4gLSAxLCBtYXggLSAxKSwgMSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gdXBzY2FsZVdpdGhpbkk1M0FuZExvb3BDaGVjayhtaW4sIG1heCk7XG4gICAgICB9XG4gICAgfTtcbiAgfSgpKTtcbiAgcHJvdG8uaW50ZWdlciA9IGZ1bmN0aW9uIChtaW4sIG1heCkge1xuICAgIHJldHVybiBSYW5kb20uaW50ZWdlcihtaW4sIG1heCkodGhpcy5lbmdpbmUpO1xuICB9O1xuXG4gIC8vIFswLCAxXSAoZmxvYXRpbmcgcG9pbnQpXG4gIFJhbmRvbS5yZWFsWmVyb1RvT25lSW5jbHVzaXZlID0gZnVuY3Rpb24gKGVuZ2luZSkge1xuICAgIHJldHVybiBSYW5kb20udWludDUzRnVsbChlbmdpbmUpIC8gMHgyMDAwMDAwMDAwMDAwMDtcbiAgfTtcbiAgcHJvdG8ucmVhbFplcm9Ub09uZUluY2x1c2l2ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gUmFuZG9tLnJlYWxaZXJvVG9PbmVJbmNsdXNpdmUodGhpcy5lbmdpbmUpO1xuICB9O1xuXG4gIC8vIFswLCAxKSAoZmxvYXRpbmcgcG9pbnQpXG4gIFJhbmRvbS5yZWFsWmVyb1RvT25lRXhjbHVzaXZlID0gZnVuY3Rpb24gKGVuZ2luZSkge1xuICAgIHJldHVybiBSYW5kb20udWludDUzKGVuZ2luZSkgLyAweDIwMDAwMDAwMDAwMDAwO1xuICB9O1xuICBwcm90by5yZWFsWmVyb1RvT25lRXhjbHVzaXZlID0gZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiBSYW5kb20ucmVhbFplcm9Ub09uZUV4Y2x1c2l2ZSh0aGlzLmVuZ2luZSk7XG4gIH07XG5cbiAgUmFuZG9tLnJlYWwgPSAoZnVuY3Rpb24gKCkge1xuICAgIGZ1bmN0aW9uIG11bHRpcGx5KGdlbmVyYXRlLCBtdWx0aXBsaWVyKSB7XG4gICAgICBpZiAobXVsdGlwbGllciA9PT0gMSkge1xuICAgICAgICByZXR1cm4gZ2VuZXJhdGU7XG4gICAgICB9IGVsc2UgaWYgKG11bHRpcGxpZXIgPT09IDApIHtcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICByZXR1cm4gMDtcbiAgICAgICAgfTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiBmdW5jdGlvbiAoZW5naW5lKSB7XG4gICAgICAgICAgcmV0dXJuIGdlbmVyYXRlKGVuZ2luZSkgKiBtdWx0aXBsaWVyO1xuICAgICAgICB9O1xuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiBmdW5jdGlvbiAobGVmdCwgcmlnaHQsIGluY2x1c2l2ZSkge1xuICAgICAgaWYgKCFpc0Zpbml0ZShsZWZ0KSkge1xuICAgICAgICB0aHJvdyBuZXcgUmFuZ2VFcnJvcihcIkV4cGVjdGVkIGxlZnQgdG8gYmUgYSBmaW5pdGUgbnVtYmVyXCIpO1xuICAgICAgfSBlbHNlIGlmICghaXNGaW5pdGUocmlnaHQpKSB7XG4gICAgICAgIHRocm93IG5ldyBSYW5nZUVycm9yKFwiRXhwZWN0ZWQgcmlnaHQgdG8gYmUgYSBmaW5pdGUgbnVtYmVyXCIpO1xuICAgICAgfVxuICAgICAgcmV0dXJuIGFkZChcbiAgICAgICAgbXVsdGlwbHkoXG4gICAgICAgICAgaW5jbHVzaXZlID8gUmFuZG9tLnJlYWxaZXJvVG9PbmVJbmNsdXNpdmUgOiBSYW5kb20ucmVhbFplcm9Ub09uZUV4Y2x1c2l2ZSxcbiAgICAgICAgICByaWdodCAtIGxlZnQpLFxuICAgICAgICBsZWZ0KTtcbiAgICB9O1xuICB9KCkpO1xuICBwcm90by5yZWFsID0gZnVuY3Rpb24gKG1pbiwgbWF4LCBpbmNsdXNpdmUpIHtcbiAgICByZXR1cm4gUmFuZG9tLnJlYWwobWluLCBtYXgsIGluY2x1c2l2ZSkodGhpcy5lbmdpbmUpO1xuICB9O1xuXG4gIFJhbmRvbS5ib29sID0gKGZ1bmN0aW9uICgpIHtcbiAgICBmdW5jdGlvbiBpc0xlYXN0Qml0VHJ1ZShlbmdpbmUpIHtcbiAgICAgIHJldHVybiAoZW5naW5lKCkgJiAxKSA9PT0gMTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBsZXNzVGhhbihnZW5lcmF0ZSwgdmFsdWUpIHtcbiAgICAgIHJldHVybiBmdW5jdGlvbiAoZW5naW5lKSB7XG4gICAgICAgIHJldHVybiBnZW5lcmF0ZShlbmdpbmUpIDwgdmFsdWU7XG4gICAgICB9O1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHByb2JhYmlsaXR5KHBlcmNlbnRhZ2UpIHtcbiAgICAgIGlmIChwZXJjZW50YWdlIDw9IDApIHtcbiAgICAgICAgcmV0dXJuIHJldHVyblZhbHVlKGZhbHNlKTtcbiAgICAgIH0gZWxzZSBpZiAocGVyY2VudGFnZSA+PSAxKSB7XG4gICAgICAgIHJldHVybiByZXR1cm5WYWx1ZSh0cnVlKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHZhciBzY2FsZWQgPSBwZXJjZW50YWdlICogMHgxMDAwMDAwMDA7XG4gICAgICAgIGlmIChzY2FsZWQgJSAxID09PSAwKSB7XG4gICAgICAgICAgcmV0dXJuIGxlc3NUaGFuKFJhbmRvbS5pbnQzMiwgKHNjYWxlZCAtIDB4ODAwMDAwMDApIHwgMCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgcmV0dXJuIGxlc3NUaGFuKFJhbmRvbS51aW50NTMsIE1hdGgucm91bmQocGVyY2VudGFnZSAqIDB4MjAwMDAwMDAwMDAwMDApKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiBmdW5jdGlvbiAobnVtZXJhdG9yLCBkZW5vbWluYXRvcikge1xuICAgICAgaWYgKGRlbm9taW5hdG9yID09IG51bGwpIHtcbiAgICAgICAgaWYgKG51bWVyYXRvciA9PSBudWxsKSB7XG4gICAgICAgICAgcmV0dXJuIGlzTGVhc3RCaXRUcnVlO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBwcm9iYWJpbGl0eShudW1lcmF0b3IpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgaWYgKG51bWVyYXRvciA8PSAwKSB7XG4gICAgICAgICAgcmV0dXJuIHJldHVyblZhbHVlKGZhbHNlKTtcbiAgICAgICAgfSBlbHNlIGlmIChudW1lcmF0b3IgPj0gZGVub21pbmF0b3IpIHtcbiAgICAgICAgICByZXR1cm4gcmV0dXJuVmFsdWUodHJ1ZSk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGxlc3NUaGFuKFJhbmRvbS5pbnRlZ2VyKDAsIGRlbm9taW5hdG9yIC0gMSksIG51bWVyYXRvcik7XG4gICAgICB9XG4gICAgfTtcbiAgfSgpKTtcbiAgcHJvdG8uYm9vbCA9IGZ1bmN0aW9uIChudW1lcmF0b3IsIGRlbm9taW5hdG9yKSB7XG4gICAgcmV0dXJuIFJhbmRvbS5ib29sKG51bWVyYXRvciwgZGVub21pbmF0b3IpKHRoaXMuZW5naW5lKTtcbiAgfTtcblxuICBmdW5jdGlvbiB0b0ludGVnZXIodmFsdWUpIHtcbiAgICB2YXIgbnVtYmVyID0gK3ZhbHVlO1xuICAgIGlmIChudW1iZXIgPCAwKSB7XG4gICAgICByZXR1cm4gTWF0aC5jZWlsKG51bWJlcik7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiBNYXRoLmZsb29yKG51bWJlcik7XG4gICAgfVxuICB9XG5cbiAgZnVuY3Rpb24gY29udmVydFNsaWNlQXJndW1lbnQodmFsdWUsIGxlbmd0aCkge1xuICAgIGlmICh2YWx1ZSA8IDApIHtcbiAgICAgIHJldHVybiBNYXRoLm1heCh2YWx1ZSArIGxlbmd0aCwgMCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiBNYXRoLm1pbih2YWx1ZSwgbGVuZ3RoKTtcbiAgICB9XG4gIH1cbiAgUmFuZG9tLnBpY2sgPSBmdW5jdGlvbiAoZW5naW5lLCBhcnJheSwgYmVnaW4sIGVuZCkge1xuICAgIHZhciBsZW5ndGggPSBhcnJheS5sZW5ndGg7XG4gICAgdmFyIHN0YXJ0ID0gYmVnaW4gPT0gbnVsbCA/IDAgOiBjb252ZXJ0U2xpY2VBcmd1bWVudCh0b0ludGVnZXIoYmVnaW4pLCBsZW5ndGgpO1xuICAgIHZhciBmaW5pc2ggPSBlbmQgPT09IHZvaWQgMCA/IGxlbmd0aCA6IGNvbnZlcnRTbGljZUFyZ3VtZW50KHRvSW50ZWdlcihlbmQpLCBsZW5ndGgpO1xuICAgIGlmIChzdGFydCA+PSBmaW5pc2gpIHtcbiAgICAgIHJldHVybiB2b2lkIDA7XG4gICAgfVxuICAgIHZhciBkaXN0cmlidXRpb24gPSBSYW5kb20uaW50ZWdlcihzdGFydCwgZmluaXNoIC0gMSk7XG4gICAgcmV0dXJuIGFycmF5W2Rpc3RyaWJ1dGlvbihlbmdpbmUpXTtcbiAgfTtcbiAgcHJvdG8ucGljayA9IGZ1bmN0aW9uIChhcnJheSwgYmVnaW4sIGVuZCkge1xuICAgIHJldHVybiBSYW5kb20ucGljayh0aGlzLmVuZ2luZSwgYXJyYXksIGJlZ2luLCBlbmQpO1xuICB9O1xuXG4gIGZ1bmN0aW9uIHJldHVyblVuZGVmaW5lZCgpIHtcbiAgICByZXR1cm4gdm9pZCAwO1xuICB9XG4gIHZhciBzbGljZSA9IEFycmF5LnByb3RvdHlwZS5zbGljZTtcbiAgUmFuZG9tLnBpY2tlciA9IGZ1bmN0aW9uIChhcnJheSwgYmVnaW4sIGVuZCkge1xuICAgIHZhciBjbG9uZSA9IHNsaWNlLmNhbGwoYXJyYXksIGJlZ2luLCBlbmQpO1xuICAgIGlmICghY2xvbmUubGVuZ3RoKSB7XG4gICAgICByZXR1cm4gcmV0dXJuVW5kZWZpbmVkO1xuICAgIH1cbiAgICB2YXIgZGlzdHJpYnV0aW9uID0gUmFuZG9tLmludGVnZXIoMCwgY2xvbmUubGVuZ3RoIC0gMSk7XG4gICAgcmV0dXJuIGZ1bmN0aW9uIChlbmdpbmUpIHtcbiAgICAgIHJldHVybiBjbG9uZVtkaXN0cmlidXRpb24oZW5naW5lKV07XG4gICAgfTtcbiAgfTtcblxuICBSYW5kb20uc2h1ZmZsZSA9IGZ1bmN0aW9uIChlbmdpbmUsIGFycmF5LCBkb3duVG8pIHtcbiAgICB2YXIgbGVuZ3RoID0gYXJyYXkubGVuZ3RoO1xuICAgIGlmIChsZW5ndGgpIHtcbiAgICAgIGlmIChkb3duVG8gPT0gbnVsbCkge1xuICAgICAgICBkb3duVG8gPSAwO1xuICAgICAgfVxuICAgICAgZm9yICh2YXIgaSA9IChsZW5ndGggLSAxKSA+Pj4gMDsgaSA+IGRvd25UbzsgLS1pKSB7XG4gICAgICAgIHZhciBkaXN0cmlidXRpb24gPSBSYW5kb20uaW50ZWdlcigwLCBpKTtcbiAgICAgICAgdmFyIGogPSBkaXN0cmlidXRpb24oZW5naW5lKTtcbiAgICAgICAgaWYgKGkgIT09IGopIHtcbiAgICAgICAgICB2YXIgdG1wID0gYXJyYXlbaV07XG4gICAgICAgICAgYXJyYXlbaV0gPSBhcnJheVtqXTtcbiAgICAgICAgICBhcnJheVtqXSA9IHRtcDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gYXJyYXk7XG4gIH07XG4gIHByb3RvLnNodWZmbGUgPSBmdW5jdGlvbiAoYXJyYXkpIHtcbiAgICByZXR1cm4gUmFuZG9tLnNodWZmbGUodGhpcy5lbmdpbmUsIGFycmF5KTtcbiAgfTtcblxuICBSYW5kb20uc2FtcGxlID0gZnVuY3Rpb24gKGVuZ2luZSwgcG9wdWxhdGlvbiwgc2FtcGxlU2l6ZSkge1xuICAgIGlmIChzYW1wbGVTaXplIDwgMCB8fCBzYW1wbGVTaXplID4gcG9wdWxhdGlvbi5sZW5ndGggfHwgIWlzRmluaXRlKHNhbXBsZVNpemUpKSB7XG4gICAgICB0aHJvdyBuZXcgUmFuZ2VFcnJvcihcIkV4cGVjdGVkIHNhbXBsZVNpemUgdG8gYmUgd2l0aGluIDAgYW5kIHRoZSBsZW5ndGggb2YgdGhlIHBvcHVsYXRpb25cIik7XG4gICAgfVxuXG4gICAgaWYgKHNhbXBsZVNpemUgPT09IDApIHtcbiAgICAgIHJldHVybiBbXTtcbiAgICB9XG5cbiAgICB2YXIgY2xvbmUgPSBzbGljZS5jYWxsKHBvcHVsYXRpb24pO1xuICAgIHZhciBsZW5ndGggPSBjbG9uZS5sZW5ndGg7XG4gICAgaWYgKGxlbmd0aCA9PT0gc2FtcGxlU2l6ZSkge1xuICAgICAgcmV0dXJuIFJhbmRvbS5zaHVmZmxlKGVuZ2luZSwgY2xvbmUsIDApO1xuICAgIH1cbiAgICB2YXIgdGFpbExlbmd0aCA9IGxlbmd0aCAtIHNhbXBsZVNpemU7XG4gICAgcmV0dXJuIFJhbmRvbS5zaHVmZmxlKGVuZ2luZSwgY2xvbmUsIHRhaWxMZW5ndGggLSAxKS5zbGljZSh0YWlsTGVuZ3RoKTtcbiAgfTtcbiAgcHJvdG8uc2FtcGxlID0gZnVuY3Rpb24gKHBvcHVsYXRpb24sIHNhbXBsZVNpemUpIHtcbiAgICByZXR1cm4gUmFuZG9tLnNhbXBsZSh0aGlzLmVuZ2luZSwgcG9wdWxhdGlvbiwgc2FtcGxlU2l6ZSk7XG4gIH07XG5cbiAgUmFuZG9tLmRpZSA9IGZ1bmN0aW9uIChzaWRlQ291bnQpIHtcbiAgICByZXR1cm4gUmFuZG9tLmludGVnZXIoMSwgc2lkZUNvdW50KTtcbiAgfTtcbiAgcHJvdG8uZGllID0gZnVuY3Rpb24gKHNpZGVDb3VudCkge1xuICAgIHJldHVybiBSYW5kb20uZGllKHNpZGVDb3VudCkodGhpcy5lbmdpbmUpO1xuICB9O1xuXG4gIFJhbmRvbS5kaWNlID0gZnVuY3Rpb24gKHNpZGVDb3VudCwgZGllQ291bnQpIHtcbiAgICB2YXIgZGlzdHJpYnV0aW9uID0gUmFuZG9tLmRpZShzaWRlQ291bnQpO1xuICAgIHJldHVybiBmdW5jdGlvbiAoZW5naW5lKSB7XG4gICAgICB2YXIgcmVzdWx0ID0gW107XG4gICAgICByZXN1bHQubGVuZ3RoID0gZGllQ291bnQ7XG4gICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGRpZUNvdW50OyArK2kpIHtcbiAgICAgICAgcmVzdWx0W2ldID0gZGlzdHJpYnV0aW9uKGVuZ2luZSk7XG4gICAgICB9XG4gICAgICByZXR1cm4gcmVzdWx0O1xuICAgIH07XG4gIH07XG4gIHByb3RvLmRpY2UgPSBmdW5jdGlvbiAoc2lkZUNvdW50LCBkaWVDb3VudCkge1xuICAgIHJldHVybiBSYW5kb20uZGljZShzaWRlQ291bnQsIGRpZUNvdW50KSh0aGlzLmVuZ2luZSk7XG4gIH07XG5cbiAgLy8gaHR0cDovL2VuLndpa2lwZWRpYS5vcmcvd2lraS9Vbml2ZXJzYWxseV91bmlxdWVfaWRlbnRpZmllclxuICBSYW5kb20udXVpZDQgPSAoZnVuY3Rpb24gKCkge1xuICAgIGZ1bmN0aW9uIHplcm9QYWQoc3RyaW5nLCB6ZXJvQ291bnQpIHtcbiAgICAgIHJldHVybiBzdHJpbmdSZXBlYXQoXCIwXCIsIHplcm9Db3VudCAtIHN0cmluZy5sZW5ndGgpICsgc3RyaW5nO1xuICAgIH1cblxuICAgIHJldHVybiBmdW5jdGlvbiAoZW5naW5lKSB7XG4gICAgICB2YXIgYSA9IGVuZ2luZSgpID4+PiAwO1xuICAgICAgdmFyIGIgPSBlbmdpbmUoKSB8IDA7XG4gICAgICB2YXIgYyA9IGVuZ2luZSgpIHwgMDtcbiAgICAgIHZhciBkID0gZW5naW5lKCkgPj4+IDA7XG5cbiAgICAgIHJldHVybiAoXG4gICAgICAgIHplcm9QYWQoYS50b1N0cmluZygxNiksIDgpICtcbiAgICAgICAgXCItXCIgK1xuICAgICAgICB6ZXJvUGFkKChiICYgMHhmZmZmKS50b1N0cmluZygxNiksIDQpICtcbiAgICAgICAgXCItXCIgK1xuICAgICAgICB6ZXJvUGFkKCgoKGIgPj4gNCkgJiAweDBmZmYpIHwgMHg0MDAwKS50b1N0cmluZygxNiksIDQpICtcbiAgICAgICAgXCItXCIgK1xuICAgICAgICB6ZXJvUGFkKCgoYyAmIDB4M2ZmZikgfCAweDgwMDApLnRvU3RyaW5nKDE2KSwgNCkgK1xuICAgICAgICBcIi1cIiArXG4gICAgICAgIHplcm9QYWQoKChjID4+IDQpICYgMHhmZmZmKS50b1N0cmluZygxNiksIDQpICtcbiAgICAgICAgemVyb1BhZChkLnRvU3RyaW5nKDE2KSwgOCkpO1xuICAgIH07XG4gIH0oKSk7XG4gIHByb3RvLnV1aWQ0ID0gZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiBSYW5kb20udXVpZDQodGhpcy5lbmdpbmUpO1xuICB9O1xuXG4gIFJhbmRvbS5zdHJpbmcgPSAoZnVuY3Rpb24gKCkge1xuICAgIC8vIGhhcyAyKip4IGNoYXJzLCBmb3IgZmFzdGVyIHVuaWZvcm0gZGlzdHJpYnV0aW9uXG4gICAgdmFyIERFRkFVTFRfU1RSSU5HX1BPT0wgPSBcImFiY2RlZmdoaWprbG1ub3BxcnN0dXZ3eHl6QUJDREVGR0hJSktMTU5PUFFSU1RVVldYWVowMTIzNDU2Nzg5Xy1cIjtcblxuICAgIHJldHVybiBmdW5jdGlvbiAocG9vbCkge1xuICAgICAgaWYgKHBvb2wgPT0gbnVsbCkge1xuICAgICAgICBwb29sID0gREVGQVVMVF9TVFJJTkdfUE9PTDtcbiAgICAgIH1cblxuICAgICAgdmFyIGxlbmd0aCA9IHBvb2wubGVuZ3RoO1xuICAgICAgaWYgKCFsZW5ndGgpIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiRXhwZWN0ZWQgcG9vbCBub3QgdG8gYmUgYW4gZW1wdHkgc3RyaW5nXCIpO1xuICAgICAgfVxuXG4gICAgICB2YXIgZGlzdHJpYnV0aW9uID0gUmFuZG9tLmludGVnZXIoMCwgbGVuZ3RoIC0gMSk7XG4gICAgICByZXR1cm4gZnVuY3Rpb24gKGVuZ2luZSwgbGVuZ3RoKSB7XG4gICAgICAgIHZhciByZXN1bHQgPSBcIlwiO1xuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGxlbmd0aDsgKytpKSB7XG4gICAgICAgICAgdmFyIGogPSBkaXN0cmlidXRpb24oZW5naW5lKTtcbiAgICAgICAgICByZXN1bHQgKz0gcG9vbC5jaGFyQXQoaik7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICAgIH07XG4gICAgfTtcbiAgfSgpKTtcbiAgcHJvdG8uc3RyaW5nID0gZnVuY3Rpb24gKGxlbmd0aCwgcG9vbCkge1xuICAgIHJldHVybiBSYW5kb20uc3RyaW5nKHBvb2wpKHRoaXMuZW5naW5lLCBsZW5ndGgpO1xuICB9O1xuXG4gIFJhbmRvbS5oZXggPSAoZnVuY3Rpb24gKCkge1xuICAgIHZhciBMT1dFUl9IRVhfUE9PTCA9IFwiMDEyMzQ1Njc4OWFiY2RlZlwiO1xuICAgIHZhciBsb3dlckhleCA9IFJhbmRvbS5zdHJpbmcoTE9XRVJfSEVYX1BPT0wpO1xuICAgIHZhciB1cHBlckhleCA9IFJhbmRvbS5zdHJpbmcoTE9XRVJfSEVYX1BPT0wudG9VcHBlckNhc2UoKSk7XG5cbiAgICByZXR1cm4gZnVuY3Rpb24gKHVwcGVyKSB7XG4gICAgICBpZiAodXBwZXIpIHtcbiAgICAgICAgcmV0dXJuIHVwcGVySGV4O1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIGxvd2VySGV4O1xuICAgICAgfVxuICAgIH07XG4gIH0oKSk7XG4gIHByb3RvLmhleCA9IGZ1bmN0aW9uIChsZW5ndGgsIHVwcGVyKSB7XG4gICAgcmV0dXJuIFJhbmRvbS5oZXgodXBwZXIpKHRoaXMuZW5naW5lLCBsZW5ndGgpO1xuICB9O1xuXG4gIFJhbmRvbS5kYXRlID0gZnVuY3Rpb24gKHN0YXJ0LCBlbmQpIHtcbiAgICBpZiAoIShzdGFydCBpbnN0YW5jZW9mIERhdGUpKSB7XG4gICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKFwiRXhwZWN0ZWQgc3RhcnQgdG8gYmUgYSBEYXRlLCBnb3QgXCIgKyB0eXBlb2Ygc3RhcnQpO1xuICAgIH0gZWxzZSBpZiAoIShlbmQgaW5zdGFuY2VvZiBEYXRlKSkge1xuICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkV4cGVjdGVkIGVuZCB0byBiZSBhIERhdGUsIGdvdCBcIiArIHR5cGVvZiBlbmQpO1xuICAgIH1cbiAgICB2YXIgZGlzdHJpYnV0aW9uID0gUmFuZG9tLmludGVnZXIoc3RhcnQuZ2V0VGltZSgpLCBlbmQuZ2V0VGltZSgpKTtcbiAgICByZXR1cm4gZnVuY3Rpb24gKGVuZ2luZSkge1xuICAgICAgcmV0dXJuIG5ldyBEYXRlKGRpc3RyaWJ1dGlvbihlbmdpbmUpKTtcbiAgICB9O1xuICB9O1xuICBwcm90by5kYXRlID0gZnVuY3Rpb24gKHN0YXJ0LCBlbmQpIHtcbiAgICByZXR1cm4gUmFuZG9tLmRhdGUoc3RhcnQsIGVuZCkodGhpcy5lbmdpbmUpO1xuICB9O1xuXG4gIGlmICh0eXBlb2YgZGVmaW5lID09PSBcImZ1bmN0aW9uXCIgJiYgZGVmaW5lLmFtZCkge1xuICAgIGRlZmluZShmdW5jdGlvbiAoKSB7XG4gICAgICByZXR1cm4gUmFuZG9tO1xuICAgIH0pO1xuICB9IGVsc2UgaWYgKHR5cGVvZiBtb2R1bGUgIT09IFwidW5kZWZpbmVkXCIgJiYgdHlwZW9mIHJlcXVpcmUgPT09IFwiZnVuY3Rpb25cIikge1xuICAgIG1vZHVsZS5leHBvcnRzID0gUmFuZG9tO1xuICB9IGVsc2Uge1xuICAgIChmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgb2xkR2xvYmFsID0gcm9vdFtHTE9CQUxfS0VZXTtcbiAgICAgIFJhbmRvbS5ub0NvbmZsaWN0ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICByb290W0dMT0JBTF9LRVldID0gb2xkR2xvYmFsO1xuICAgICAgICByZXR1cm4gdGhpcztcbiAgICAgIH07XG4gICAgfSgpKTtcbiAgICByb290W0dMT0JBTF9LRVldID0gUmFuZG9tO1xuICB9XG59KHRoaXMpKTsiLCIoZnVuY3Rpb24gKGdsb2JhbCwgZmFjdG9yeSkge1xuXHR0eXBlb2YgZXhwb3J0cyA9PT0gJ29iamVjdCcgJiYgdHlwZW9mIG1vZHVsZSAhPT0gJ3VuZGVmaW5lZCcgPyBmYWN0b3J5KGV4cG9ydHMpIDpcblx0dHlwZW9mIGRlZmluZSA9PT0gJ2Z1bmN0aW9uJyAmJiBkZWZpbmUuYW1kID8gZGVmaW5lKFsnZXhwb3J0cyddLCBmYWN0b3J5KSA6XG5cdChmYWN0b3J5KChnbG9iYWwucmVkb20gPSBnbG9iYWwucmVkb20gfHwge30pKSk7XG59KHRoaXMsIChmdW5jdGlvbiAoZXhwb3J0cykgeyAndXNlIHN0cmljdCc7XG5cbnZhciBIQVNIID0gJyMnLmNoYXJDb2RlQXQoMCk7XG52YXIgRE9UID0gJy4nLmNoYXJDb2RlQXQoMCk7XG5cbmZ1bmN0aW9uIGNyZWF0ZUVsZW1lbnQgKHF1ZXJ5LCBucykge1xuICB2YXIgdGFnO1xuICB2YXIgaWQ7XG4gIHZhciBjbGFzc05hbWU7XG5cbiAgdmFyIG1vZGUgPSAwO1xuICB2YXIgc3RhcnQgPSAwO1xuXG4gIGZvciAodmFyIGkgPSAwOyBpIDw9IHF1ZXJ5Lmxlbmd0aDsgaSsrKSB7XG4gICAgdmFyIGNoYXIgPSBxdWVyeS5jaGFyQ29kZUF0KGkpO1xuXG4gICAgaWYgKGNoYXIgPT09IEhBU0ggfHwgY2hhciA9PT0gRE9UIHx8ICFjaGFyKSB7XG4gICAgICBpZiAobW9kZSA9PT0gMCkge1xuICAgICAgICBpZiAoaSA9PT0gMCkge1xuICAgICAgICAgIHRhZyA9ICdkaXYnO1xuICAgICAgICB9IGVsc2UgaWYgKCFjaGFyKSB7XG4gICAgICAgICAgdGFnID0gcXVlcnk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdGFnID0gcXVlcnkuc3Vic3RyaW5nKHN0YXJ0LCBpKTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdmFyIHNsaWNlID0gcXVlcnkuc3Vic3RyaW5nKHN0YXJ0LCBpKTtcblxuICAgICAgICBpZiAobW9kZSA9PT0gMSkge1xuICAgICAgICAgIGlkID0gc2xpY2U7XG4gICAgICAgIH0gZWxzZSBpZiAoY2xhc3NOYW1lKSB7XG4gICAgICAgICAgY2xhc3NOYW1lICs9ICcgJyArIHNsaWNlO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGNsYXNzTmFtZSA9IHNsaWNlO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHN0YXJ0ID0gaSArIDE7XG5cbiAgICAgIGlmIChjaGFyID09PSBIQVNIKSB7XG4gICAgICAgIG1vZGUgPSAxO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgbW9kZSA9IDI7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgdmFyIGVsZW1lbnQgPSBucyA/IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnROUyhucywgdGFnKSA6IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQodGFnKTtcblxuICBpZiAoaWQpIHtcbiAgICBlbGVtZW50LmlkID0gaWQ7XG4gIH1cblxuICBpZiAoY2xhc3NOYW1lKSB7XG4gICAgaWYgKG5zKSB7XG4gICAgICBlbGVtZW50LnNldEF0dHJpYnV0ZSgnY2xhc3MnLCBjbGFzc05hbWUpO1xuICAgIH0gZWxzZSB7XG4gICAgICBlbGVtZW50LmNsYXNzTmFtZSA9IGNsYXNzTmFtZTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gZWxlbWVudDtcbn1cblxudmFyIGhvb2tOYW1lcyA9IFsnb25tb3VudCcsICdvbnVubW91bnQnXTtcblxuZnVuY3Rpb24gbW91bnQgKHBhcmVudCwgY2hpbGQsIGJlZm9yZSkge1xuICB2YXIgcGFyZW50RWwgPSBnZXRFbChwYXJlbnQpO1xuICB2YXIgY2hpbGRFbCA9IGdldEVsKGNoaWxkKTtcblxuICBpZiAoY2hpbGQgPT09IGNoaWxkRWwgJiYgY2hpbGRFbC5fX3JlZG9tX3ZpZXcpIHtcbiAgICAvLyB0cnkgdG8gbG9vayB1cCB0aGUgdmlldyBpZiBub3QgcHJvdmlkZWRcbiAgICBjaGlsZCA9IGNoaWxkRWwuX19yZWRvbV92aWV3O1xuICB9XG5cbiAgaWYgKGNoaWxkICE9PSBjaGlsZEVsKSB7XG4gICAgY2hpbGRFbC5fX3JlZG9tX3ZpZXcgPSBjaGlsZDtcbiAgfVxuXG4gIHZhciB3YXNNb3VudGVkID0gY2hpbGRFbC5fX3JlZG9tX21vdW50ZWQ7XG4gIHZhciBvbGRQYXJlbnQgPSBjaGlsZEVsLnBhcmVudE5vZGU7XG5cbiAgaWYgKHdhc01vdW50ZWQgJiYgKG9sZFBhcmVudCAhPT0gcGFyZW50RWwpKSB7XG4gICAgZG9Vbm1vdW50KGNoaWxkLCBjaGlsZEVsLCBvbGRQYXJlbnQpO1xuICB9XG5cbiAgaWYgKGJlZm9yZSAhPSBudWxsKSB7XG4gICAgcGFyZW50RWwuaW5zZXJ0QmVmb3JlKGNoaWxkRWwsIGdldEVsKGJlZm9yZSkpO1xuICB9IGVsc2Uge1xuICAgIHBhcmVudEVsLmFwcGVuZENoaWxkKGNoaWxkRWwpO1xuICB9XG5cbiAgZG9Nb3VudChjaGlsZCwgY2hpbGRFbCwgcGFyZW50RWwsIG9sZFBhcmVudCk7XG5cbiAgcmV0dXJuIGNoaWxkO1xufVxuXG5mdW5jdGlvbiB1bm1vdW50IChwYXJlbnQsIGNoaWxkKSB7XG4gIHZhciBwYXJlbnRFbCA9IGdldEVsKHBhcmVudCk7XG4gIHZhciBjaGlsZEVsID0gZ2V0RWwoY2hpbGQpO1xuXG4gIGlmIChjaGlsZCA9PT0gY2hpbGRFbCAmJiBjaGlsZEVsLl9fcmVkb21fdmlldykge1xuICAgIC8vIHRyeSB0byBsb29rIHVwIHRoZSB2aWV3IGlmIG5vdCBwcm92aWRlZFxuICAgIGNoaWxkID0gY2hpbGRFbC5fX3JlZG9tX3ZpZXc7XG4gIH1cblxuICBkb1VubW91bnQoY2hpbGQsIGNoaWxkRWwsIHBhcmVudEVsKTtcblxuICBwYXJlbnRFbC5yZW1vdmVDaGlsZChjaGlsZEVsKTtcblxuICByZXR1cm4gY2hpbGQ7XG59XG5cbmZ1bmN0aW9uIGRvTW91bnQgKGNoaWxkLCBjaGlsZEVsLCBwYXJlbnRFbCwgb2xkUGFyZW50KSB7XG4gIHZhciBob29rcyA9IGNoaWxkRWwuX19yZWRvbV9saWZlY3ljbGUgfHwgKGNoaWxkRWwuX19yZWRvbV9saWZlY3ljbGUgPSB7fSk7XG4gIHZhciByZW1vdW50ID0gKHBhcmVudEVsID09PSBvbGRQYXJlbnQpO1xuICB2YXIgaG9va3NGb3VuZCA9IGZhbHNlO1xuXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgaG9va05hbWVzLmxlbmd0aDsgaSsrKSB7XG4gICAgdmFyIGhvb2tOYW1lID0gaG9va05hbWVzW2ldO1xuXG4gICAgaWYgKCFyZW1vdW50ICYmIChjaGlsZCAhPT0gY2hpbGRFbCkgJiYgKGhvb2tOYW1lIGluIGNoaWxkKSkge1xuICAgICAgaG9va3NbaG9va05hbWVdID0gKGhvb2tzW2hvb2tOYW1lXSB8fCAwKSArIDE7XG4gICAgfVxuICAgIGlmIChob29rc1tob29rTmFtZV0pIHtcbiAgICAgIGhvb2tzRm91bmQgPSB0cnVlO1xuICAgIH1cbiAgfVxuXG4gIGlmICghaG9va3NGb3VuZCkge1xuICAgIGNoaWxkRWwuX19yZWRvbV9tb3VudGVkID0gdHJ1ZTtcbiAgICByZXR1cm47XG4gIH1cblxuICB2YXIgdHJhdmVyc2UgPSBwYXJlbnRFbDtcbiAgdmFyIHRyaWdnZXJlZCA9IGZhbHNlO1xuXG4gIGlmIChyZW1vdW50IHx8ICghdHJpZ2dlcmVkICYmICh0cmF2ZXJzZSAmJiB0cmF2ZXJzZS5fX3JlZG9tX21vdW50ZWQpKSkge1xuICAgIHRyaWdnZXIoY2hpbGRFbCwgcmVtb3VudCA/ICdvbnJlbW91bnQnIDogJ29ubW91bnQnKTtcbiAgICB0cmlnZ2VyZWQgPSB0cnVlO1xuICB9XG5cbiAgaWYgKHJlbW91bnQpIHtcbiAgICByZXR1cm47XG4gIH1cblxuICB3aGlsZSAodHJhdmVyc2UpIHtcbiAgICB2YXIgcGFyZW50ID0gdHJhdmVyc2UucGFyZW50Tm9kZTtcbiAgICB2YXIgcGFyZW50SG9va3MgPSB0cmF2ZXJzZS5fX3JlZG9tX2xpZmVjeWNsZSB8fCAodHJhdmVyc2UuX19yZWRvbV9saWZlY3ljbGUgPSB7fSk7XG5cbiAgICBmb3IgKHZhciBob29rIGluIGhvb2tzKSB7XG4gICAgICBwYXJlbnRIb29rc1tob29rXSA9IChwYXJlbnRIb29rc1tob29rXSB8fCAwKSArIGhvb2tzW2hvb2tdO1xuICAgIH1cblxuICAgIGlmICghdHJpZ2dlcmVkICYmICh0cmF2ZXJzZSA9PT0gZG9jdW1lbnQgfHwgKHBhcmVudCAmJiBwYXJlbnQuX19yZWRvbV9tb3VudGVkKSkpIHtcbiAgICAgIHRyaWdnZXIodHJhdmVyc2UsIHJlbW91bnQgPyAnb25yZW1vdW50JyA6ICdvbm1vdW50Jyk7XG4gICAgICB0cmlnZ2VyZWQgPSB0cnVlO1xuICAgIH1cblxuICAgIHRyYXZlcnNlID0gcGFyZW50O1xuICB9XG59XG5cbmZ1bmN0aW9uIGRvVW5tb3VudCAoY2hpbGQsIGNoaWxkRWwsIHBhcmVudEVsKSB7XG4gIHZhciBob29rcyA9IGNoaWxkRWwuX19yZWRvbV9saWZlY3ljbGU7XG5cbiAgaWYgKCFob29rcykge1xuICAgIGNoaWxkRWwuX19yZWRvbV9tb3VudGVkID0gZmFsc2U7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgdmFyIHRyYXZlcnNlID0gcGFyZW50RWw7XG5cbiAgaWYgKGNoaWxkRWwuX19yZWRvbV9tb3VudGVkKSB7XG4gICAgdHJpZ2dlcihjaGlsZEVsLCAnb251bm1vdW50Jyk7XG4gIH1cblxuICB3aGlsZSAodHJhdmVyc2UpIHtcbiAgICB2YXIgcGFyZW50SG9va3MgPSB0cmF2ZXJzZS5fX3JlZG9tX2xpZmVjeWNsZSB8fCAodHJhdmVyc2UuX19yZWRvbV9saWZlY3ljbGUgPSB7fSk7XG4gICAgdmFyIGhvb2tzRm91bmQgPSBmYWxzZTtcblxuICAgIGZvciAodmFyIGhvb2sgaW4gaG9va3MpIHtcbiAgICAgIGlmIChwYXJlbnRIb29rc1tob29rXSkge1xuICAgICAgICBwYXJlbnRIb29rc1tob29rXSAtPSBob29rc1tob29rXTtcbiAgICAgIH1cbiAgICAgIGlmIChwYXJlbnRIb29rc1tob29rXSkge1xuICAgICAgICBob29rc0ZvdW5kID0gdHJ1ZTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAoIWhvb2tzRm91bmQpIHtcbiAgICAgIHRyYXZlcnNlLl9fcmVkb21fbGlmZWN5Y2xlID0gbnVsbDtcbiAgICB9XG5cbiAgICB0cmF2ZXJzZSA9IHRyYXZlcnNlLnBhcmVudE5vZGU7XG4gIH1cbn1cblxuZnVuY3Rpb24gdHJpZ2dlciAoZWwsIGV2ZW50TmFtZSkge1xuICBpZiAoZXZlbnROYW1lID09PSAnb25tb3VudCcpIHtcbiAgICBlbC5fX3JlZG9tX21vdW50ZWQgPSB0cnVlO1xuICB9IGVsc2UgaWYgKGV2ZW50TmFtZSA9PT0gJ29udW5tb3VudCcpIHtcbiAgICBlbC5fX3JlZG9tX21vdW50ZWQgPSBmYWxzZTtcbiAgfVxuXG4gIHZhciBob29rcyA9IGVsLl9fcmVkb21fbGlmZWN5Y2xlO1xuXG4gIGlmICghaG9va3MpIHtcbiAgICByZXR1cm47XG4gIH1cblxuICB2YXIgdmlldyA9IGVsLl9fcmVkb21fdmlldztcbiAgdmFyIGhvb2tDb3VudCA9IDA7XG5cbiAgdmlldyAmJiB2aWV3W2V2ZW50TmFtZV0gJiYgdmlld1tldmVudE5hbWVdKCk7XG5cbiAgZm9yICh2YXIgaG9vayBpbiBob29rcykge1xuICAgIGlmIChob29rKSB7XG4gICAgICBob29rQ291bnQrKztcbiAgICB9XG4gIH1cblxuICBpZiAoaG9va0NvdW50KSB7XG4gICAgdmFyIHRyYXZlcnNlID0gZWwuZmlyc3RDaGlsZDtcblxuICAgIHdoaWxlICh0cmF2ZXJzZSkge1xuICAgICAgdmFyIG5leHQgPSB0cmF2ZXJzZS5uZXh0U2libGluZztcblxuICAgICAgdHJpZ2dlcih0cmF2ZXJzZSwgZXZlbnROYW1lKTtcblxuICAgICAgdHJhdmVyc2UgPSBuZXh0O1xuICAgIH1cbiAgfVxufVxuXG5mdW5jdGlvbiBzZXRTdHlsZSAodmlldywgYXJnMSwgYXJnMikge1xuICB2YXIgZWwgPSBnZXRFbCh2aWV3KTtcblxuICBpZiAoYXJnMiAhPT0gdW5kZWZpbmVkKSB7XG4gICAgZWwuc3R5bGVbYXJnMV0gPSBhcmcyO1xuICB9IGVsc2UgaWYgKGlzU3RyaW5nKGFyZzEpKSB7XG4gICAgZWwuc2V0QXR0cmlidXRlKCdzdHlsZScsIGFyZzEpO1xuICB9IGVsc2Uge1xuICAgIGZvciAodmFyIGtleSBpbiBhcmcxKSB7XG4gICAgICBzZXRTdHlsZShlbCwga2V5LCBhcmcxW2tleV0pO1xuICAgIH1cbiAgfVxufVxuXG5mdW5jdGlvbiBzZXRBdHRyICh2aWV3LCBhcmcxLCBhcmcyKSB7XG4gIHZhciBlbCA9IGdldEVsKHZpZXcpO1xuICB2YXIgaXNTVkcgPSBlbCBpbnN0YW5jZW9mIHdpbmRvdy5TVkdFbGVtZW50O1xuXG4gIGlmIChhcmcyICE9PSB1bmRlZmluZWQpIHtcbiAgICBpZiAoYXJnMSA9PT0gJ3N0eWxlJykge1xuICAgICAgc2V0U3R5bGUoZWwsIGFyZzIpO1xuICAgIH0gZWxzZSBpZiAoaXNTVkcgJiYgaXNGdW5jdGlvbihhcmcyKSkge1xuICAgICAgZWxbYXJnMV0gPSBhcmcyO1xuICAgIH0gZWxzZSBpZiAoIWlzU1ZHICYmIChhcmcxIGluIGVsIHx8IGlzRnVuY3Rpb24oYXJnMikpKSB7XG4gICAgICBlbFthcmcxXSA9IGFyZzI7XG4gICAgfSBlbHNlIHtcbiAgICAgIGVsLnNldEF0dHJpYnV0ZShhcmcxLCBhcmcyKTtcbiAgICB9XG4gIH0gZWxzZSB7XG4gICAgZm9yICh2YXIga2V5IGluIGFyZzEpIHtcbiAgICAgIHNldEF0dHIoZWwsIGtleSwgYXJnMVtrZXldKTtcbiAgICB9XG4gIH1cbn1cblxudmFyIHRleHQgPSBmdW5jdGlvbiAoc3RyKSB7IHJldHVybiBkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZShzdHIpOyB9O1xuXG5mdW5jdGlvbiBwYXJzZUFyZ3VtZW50cyAoZWxlbWVudCwgYXJncykge1xuICBmb3IgKHZhciBpID0gMDsgaSA8IGFyZ3MubGVuZ3RoOyBpKyspIHtcbiAgICB2YXIgYXJnID0gYXJnc1tpXTtcblxuICAgIGlmIChhcmcgIT09IDAgJiYgIWFyZykge1xuICAgICAgY29udGludWU7XG4gICAgfVxuXG4gICAgLy8gc3VwcG9ydCBtaWRkbGV3YXJlXG4gICAgaWYgKHR5cGVvZiBhcmcgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgIGFyZyhlbGVtZW50KTtcbiAgICB9IGVsc2UgaWYgKGlzU3RyaW5nKGFyZykgfHwgaXNOdW1iZXIoYXJnKSkge1xuICAgICAgZWxlbWVudC5hcHBlbmRDaGlsZCh0ZXh0KGFyZykpO1xuICAgIH0gZWxzZSBpZiAoaXNOb2RlKGdldEVsKGFyZykpKSB7XG4gICAgICBtb3VudChlbGVtZW50LCBhcmcpO1xuICAgIH0gZWxzZSBpZiAoYXJnLmxlbmd0aCkge1xuICAgICAgcGFyc2VBcmd1bWVudHMoZWxlbWVudCwgYXJnKTtcbiAgICB9IGVsc2UgaWYgKHR5cGVvZiBhcmcgPT09ICdvYmplY3QnKSB7XG4gICAgICBzZXRBdHRyKGVsZW1lbnQsIGFyZyk7XG4gICAgfVxuICB9XG59XG5cbnZhciBlbnN1cmVFbCA9IGZ1bmN0aW9uIChwYXJlbnQpIHsgcmV0dXJuIGlzU3RyaW5nKHBhcmVudCkgPyBodG1sKHBhcmVudCkgOiBnZXRFbChwYXJlbnQpOyB9O1xudmFyIGdldEVsID0gZnVuY3Rpb24gKHBhcmVudCkgeyByZXR1cm4gKHBhcmVudC5ub2RlVHlwZSAmJiBwYXJlbnQpIHx8ICghcGFyZW50LmVsICYmIHBhcmVudCkgfHwgZ2V0RWwocGFyZW50LmVsKTsgfTtcblxudmFyIGlzU3RyaW5nID0gZnVuY3Rpb24gKGEpIHsgcmV0dXJuIHR5cGVvZiBhID09PSAnc3RyaW5nJzsgfTtcbnZhciBpc051bWJlciA9IGZ1bmN0aW9uIChhKSB7IHJldHVybiB0eXBlb2YgYSA9PT0gJ251bWJlcic7IH07XG52YXIgaXNGdW5jdGlvbiA9IGZ1bmN0aW9uIChhKSB7IHJldHVybiB0eXBlb2YgYSA9PT0gJ2Z1bmN0aW9uJzsgfTtcblxudmFyIGlzTm9kZSA9IGZ1bmN0aW9uIChhKSB7IHJldHVybiBhICYmIGEubm9kZVR5cGU7IH07XG5cbnZhciBodG1sQ2FjaGUgPSB7fTtcblxudmFyIG1lbW9pemVIVE1MID0gZnVuY3Rpb24gKHF1ZXJ5KSB7IHJldHVybiBodG1sQ2FjaGVbcXVlcnldIHx8IChodG1sQ2FjaGVbcXVlcnldID0gY3JlYXRlRWxlbWVudChxdWVyeSkpOyB9O1xuXG5mdW5jdGlvbiBodG1sIChxdWVyeSkge1xuICB2YXIgYXJncyA9IFtdLCBsZW4gPSBhcmd1bWVudHMubGVuZ3RoIC0gMTtcbiAgd2hpbGUgKCBsZW4tLSA+IDAgKSBhcmdzWyBsZW4gXSA9IGFyZ3VtZW50c1sgbGVuICsgMSBdO1xuXG4gIHZhciBlbGVtZW50O1xuXG4gIGlmIChpc1N0cmluZyhxdWVyeSkpIHtcbiAgICBlbGVtZW50ID0gbWVtb2l6ZUhUTUwocXVlcnkpLmNsb25lTm9kZShmYWxzZSk7XG4gIH0gZWxzZSBpZiAoaXNOb2RlKHF1ZXJ5KSkge1xuICAgIGVsZW1lbnQgPSBxdWVyeS5jbG9uZU5vZGUoZmFsc2UpO1xuICB9IGVsc2Uge1xuICAgIHRocm93IG5ldyBFcnJvcignQXQgbGVhc3Qgb25lIGFyZ3VtZW50IHJlcXVpcmVkJyk7XG4gIH1cblxuICBwYXJzZUFyZ3VtZW50cyhlbGVtZW50LCBhcmdzKTtcblxuICByZXR1cm4gZWxlbWVudDtcbn1cblxuaHRtbC5leHRlbmQgPSBmdW5jdGlvbiAocXVlcnkpIHtcbiAgdmFyIGNsb25lID0gbWVtb2l6ZUhUTUwocXVlcnkpO1xuXG4gIHJldHVybiBodG1sLmJpbmQodGhpcywgY2xvbmUpO1xufTtcblxudmFyIGVsID0gaHRtbDtcblxuZnVuY3Rpb24gc2V0Q2hpbGRyZW4gKHBhcmVudCwgY2hpbGRyZW4pIHtcbiAgaWYgKGNoaWxkcmVuLmxlbmd0aCA9PT0gdW5kZWZpbmVkKSB7XG4gICAgcmV0dXJuIHNldENoaWxkcmVuKHBhcmVudCwgW2NoaWxkcmVuXSk7XG4gIH1cblxuICB2YXIgcGFyZW50RWwgPSBnZXRFbChwYXJlbnQpO1xuICB2YXIgdHJhdmVyc2UgPSBwYXJlbnRFbC5maXJzdENoaWxkO1xuXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgY2hpbGRyZW4ubGVuZ3RoOyBpKyspIHtcbiAgICB2YXIgY2hpbGQgPSBjaGlsZHJlbltpXTtcblxuICAgIGlmICghY2hpbGQpIHtcbiAgICAgIGNvbnRpbnVlO1xuICAgIH1cblxuICAgIHZhciBjaGlsZEVsID0gZ2V0RWwoY2hpbGQpO1xuXG4gICAgaWYgKGNoaWxkRWwgPT09IHRyYXZlcnNlKSB7XG4gICAgICB0cmF2ZXJzZSA9IHRyYXZlcnNlLm5leHRTaWJsaW5nO1xuICAgICAgY29udGludWU7XG4gICAgfVxuXG4gICAgbW91bnQocGFyZW50LCBjaGlsZCwgdHJhdmVyc2UpO1xuICB9XG5cbiAgd2hpbGUgKHRyYXZlcnNlKSB7XG4gICAgdmFyIG5leHQgPSB0cmF2ZXJzZS5uZXh0U2libGluZztcblxuICAgIHVubW91bnQocGFyZW50LCB0cmF2ZXJzZSk7XG5cbiAgICB0cmF2ZXJzZSA9IG5leHQ7XG4gIH1cbn1cblxudmFyIHByb3BLZXkgPSBmdW5jdGlvbiAoa2V5KSB7IHJldHVybiBmdW5jdGlvbiAoaXRlbSkgeyByZXR1cm4gaXRlbVtrZXldOyB9OyB9O1xuXG5mdW5jdGlvbiBsaXN0IChwYXJlbnQsIFZpZXcsIGtleSwgaW5pdERhdGEpIHtcbiAgcmV0dXJuIG5ldyBMaXN0KHBhcmVudCwgVmlldywga2V5LCBpbml0RGF0YSk7XG59XG5cbmZ1bmN0aW9uIExpc3QgKHBhcmVudCwgVmlldywga2V5LCBpbml0RGF0YSkge1xuICB0aGlzLl9fcmVkb21fbGlzdCA9IHRydWU7XG4gIHRoaXMuVmlldyA9IFZpZXc7XG4gIHRoaXMuaW5pdERhdGEgPSBpbml0RGF0YTtcbiAgdGhpcy52aWV3cyA9IFtdO1xuICB0aGlzLmVsID0gZW5zdXJlRWwocGFyZW50KTtcblxuICBpZiAoa2V5ICE9IG51bGwpIHtcbiAgICB0aGlzLmxvb2t1cCA9IHt9O1xuICAgIHRoaXMua2V5ID0gaXNGdW5jdGlvbihrZXkpID8ga2V5IDogcHJvcEtleShrZXkpO1xuICB9XG59XG5cbkxpc3QuZXh0ZW5kID0gZnVuY3Rpb24gKHBhcmVudCwgVmlldywga2V5LCBpbml0RGF0YSkge1xuICByZXR1cm4gTGlzdC5iaW5kKExpc3QsIHBhcmVudCwgVmlldywga2V5LCBpbml0RGF0YSk7XG59O1xuXG5saXN0LmV4dGVuZCA9IExpc3QuZXh0ZW5kO1xuXG5MaXN0LnByb3RvdHlwZS51cGRhdGUgPSBmdW5jdGlvbiAoZGF0YSkge1xuICB2YXIgdGhpcyQxID0gdGhpcztcbiAgaWYgKCBkYXRhID09PSB2b2lkIDAgKSBkYXRhID0gW107XG5cbiAgdmFyIFZpZXcgPSB0aGlzLlZpZXc7XG4gIHZhciBrZXkgPSB0aGlzLmtleTtcbiAgdmFyIGtleVNldCA9IGtleSAhPSBudWxsO1xuICB2YXIgaW5pdERhdGEgPSB0aGlzLmluaXREYXRhO1xuICB2YXIgbmV3Vmlld3MgPSBuZXcgQXJyYXkoZGF0YS5sZW5ndGgpO1xuICB2YXIgb2xkVmlld3MgPSB0aGlzLnZpZXdzO1xuICB2YXIgbmV3TG9va3VwID0ga2V5ICYmIHt9O1xuICB2YXIgb2xkTG9va3VwID0ga2V5ICYmIHRoaXMubG9va3VwO1xuXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgZGF0YS5sZW5ndGg7IGkrKykge1xuICAgIHZhciBpdGVtID0gZGF0YVtpXTtcbiAgICB2YXIgdmlldyA9ICh2b2lkIDApO1xuXG4gICAgaWYgKGtleVNldCkge1xuICAgICAgdmFyIGlkID0ga2V5KGl0ZW0pO1xuICAgICAgdmlldyA9IG5ld1ZpZXdzW2ldID0gb2xkTG9va3VwW2lkXSB8fCBuZXcgVmlldyhpbml0RGF0YSwgaXRlbSwgaSwgZGF0YSk7XG4gICAgICBuZXdMb29rdXBbaWRdID0gdmlldztcbiAgICAgIHZpZXcuX19pZCA9IGlkO1xuICAgIH0gZWxzZSB7XG4gICAgICB2aWV3ID0gbmV3Vmlld3NbaV0gPSBvbGRWaWV3c1tpXSB8fCBuZXcgVmlldyhpbml0RGF0YSwgaXRlbSwgaSwgZGF0YSk7XG4gICAgfVxuICAgIHZhciBlbCA9IHZpZXcuZWw7XG4gICAgaWYgKGVsLl9fcmVkb21fbGlzdCkge1xuICAgICAgZWwgPSBlbC5lbDtcbiAgICB9XG4gICAgZWwuX19yZWRvbV92aWV3ID0gdmlldztcbiAgICB2aWV3LnVwZGF0ZSAmJiB2aWV3LnVwZGF0ZShpdGVtLCBpLCBkYXRhKTtcbiAgfVxuXG4gIGlmIChrZXlTZXQpIHtcbiAgICBmb3IgKHZhciBpJDEgPSAwOyBpJDEgPCBvbGRWaWV3cy5sZW5ndGg7IGkkMSsrKSB7XG4gICAgICB2YXIgaWQkMSA9IG9sZFZpZXdzW2kkMV0uX19pZDtcblxuICAgICAgaWYgKCEoaWQkMSBpbiBuZXdMb29rdXApKSB7XG4gICAgICAgIHVubW91bnQodGhpcyQxLCBvbGRMb29rdXBbaWQkMV0pO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHNldENoaWxkcmVuKHRoaXMsIG5ld1ZpZXdzKTtcblxuICBpZiAoa2V5U2V0KSB7XG4gICAgdGhpcy5sb29rdXAgPSBuZXdMb29rdXA7XG4gIH1cbiAgdGhpcy52aWV3cyA9IG5ld1ZpZXdzO1xufTtcblxuZnVuY3Rpb24gcm91dGVyIChwYXJlbnQsIFZpZXdzLCBpbml0RGF0YSkge1xuICByZXR1cm4gbmV3IFJvdXRlcihwYXJlbnQsIFZpZXdzLCBpbml0RGF0YSk7XG59XG5cbnZhciBSb3V0ZXIgPSBmdW5jdGlvbiBSb3V0ZXIgKHBhcmVudCwgVmlld3MsIGluaXREYXRhKSB7XG4gIHRoaXMuZWwgPSBlbnN1cmVFbChwYXJlbnQpO1xuICB0aGlzLlZpZXdzID0gVmlld3M7XG4gIHRoaXMuaW5pdERhdGEgPSBpbml0RGF0YTtcbn07XG5Sb3V0ZXIucHJvdG90eXBlLnVwZGF0ZSA9IGZ1bmN0aW9uIHVwZGF0ZSAocm91dGUsIGRhdGEpIHtcbiAgaWYgKHJvdXRlICE9PSB0aGlzLnJvdXRlKSB7XG4gICAgdmFyIFZpZXdzID0gdGhpcy5WaWV3cztcbiAgICB2YXIgVmlldyA9IFZpZXdzW3JvdXRlXTtcblxuICAgIHRoaXMudmlldyA9IFZpZXcgJiYgbmV3IFZpZXcodGhpcy5pbml0RGF0YSwgZGF0YSk7XG4gICAgdGhpcy5yb3V0ZSA9IHJvdXRlO1xuXG4gICAgc2V0Q2hpbGRyZW4odGhpcy5lbCwgWyB0aGlzLnZpZXcgXSk7XG4gIH1cbiAgdGhpcy52aWV3ICYmIHRoaXMudmlldy51cGRhdGUgJiYgdGhpcy52aWV3LnVwZGF0ZShkYXRhLCByb3V0ZSk7XG59O1xuXG52YXIgU1ZHID0gJ2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJztcblxudmFyIHN2Z0NhY2hlID0ge307XG5cbnZhciBtZW1vaXplU1ZHID0gZnVuY3Rpb24gKHF1ZXJ5KSB7IHJldHVybiBzdmdDYWNoZVtxdWVyeV0gfHwgKHN2Z0NhY2hlW3F1ZXJ5XSA9IGNyZWF0ZUVsZW1lbnQocXVlcnksIFNWRykpOyB9O1xuXG5mdW5jdGlvbiBzdmcgKHF1ZXJ5KSB7XG4gIHZhciBhcmdzID0gW10sIGxlbiA9IGFyZ3VtZW50cy5sZW5ndGggLSAxO1xuICB3aGlsZSAoIGxlbi0tID4gMCApIGFyZ3NbIGxlbiBdID0gYXJndW1lbnRzWyBsZW4gKyAxIF07XG5cbiAgdmFyIGVsZW1lbnQ7XG5cbiAgaWYgKGlzU3RyaW5nKHF1ZXJ5KSkge1xuICAgIGVsZW1lbnQgPSBtZW1vaXplU1ZHKHF1ZXJ5KS5jbG9uZU5vZGUoZmFsc2UpO1xuICB9IGVsc2UgaWYgKGlzTm9kZShxdWVyeSkpIHtcbiAgICBlbGVtZW50ID0gcXVlcnkuY2xvbmVOb2RlKGZhbHNlKTtcbiAgfSBlbHNlIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ0F0IGxlYXN0IG9uZSBhcmd1bWVudCByZXF1aXJlZCcpO1xuICB9XG5cbiAgcGFyc2VBcmd1bWVudHMoZWxlbWVudCwgYXJncyk7XG5cbiAgcmV0dXJuIGVsZW1lbnQ7XG59XG5cbnN2Zy5leHRlbmQgPSBmdW5jdGlvbiAocXVlcnkpIHtcbiAgdmFyIGNsb25lID0gbWVtb2l6ZVNWRyhxdWVyeSk7XG5cbiAgcmV0dXJuIHN2Zy5iaW5kKHRoaXMsIGNsb25lKTtcbn07XG5cbmV4cG9ydHMuaHRtbCA9IGh0bWw7XG5leHBvcnRzLmVsID0gZWw7XG5leHBvcnRzLmxpc3QgPSBsaXN0O1xuZXhwb3J0cy5MaXN0ID0gTGlzdDtcbmV4cG9ydHMubW91bnQgPSBtb3VudDtcbmV4cG9ydHMudW5tb3VudCA9IHVubW91bnQ7XG5leHBvcnRzLnJvdXRlciA9IHJvdXRlcjtcbmV4cG9ydHMuUm91dGVyID0gUm91dGVyO1xuZXhwb3J0cy5zZXRBdHRyID0gc2V0QXR0cjtcbmV4cG9ydHMuc2V0U3R5bGUgPSBzZXRTdHlsZTtcbmV4cG9ydHMuc2V0Q2hpbGRyZW4gPSBzZXRDaGlsZHJlbjtcbmV4cG9ydHMuc3ZnID0gc3ZnO1xuZXhwb3J0cy50ZXh0ID0gdGV4dDtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcblxufSkpKTtcbiIsImNvbnN0IHsgZWwsIG1vdW50LCBsaXN0LCB1bm1vdW50fSAgPSByZXF1aXJlKCdyZWRvbScpXG5jb25zdCBSYW5kb20gPSByZXF1aXJlKCdyYW5kb20tanMnKVxuXG5sZXQgcmFuZG9tID0gbmV3IFJhbmRvbShSYW5kb20uZW5naW5lcy5icm93c2VyQ3J5cHRvKVxuXG5jbGFzcyBJdGVtIHtcbiAgICBjb25zdHJ1Y3RvcihhLGIsYyxkKXtcbiAgICAgICAgXG4gICAgICAgIHRoaXMucmVtb3ZlID0gdGhpcy5yZW1vdmUuYmluZCh0aGlzKVxuICAgICAgICB0aGlzLmVsID0gZWwoJy5pdGVtJyxcbiAgICAgICAgICAgIHRoaXMuaXRlbU5hbWUgPSBlbCgnLml0ZW0tbmFtZScpLFxuICAgICAgICAgICAgdGhpcy5pdGVtRGVsZXRlID0gZWwoJy5pdGVtLWRlbGV0ZScsICd4JylcbiAgICAgICAgKVxuXG4gICAgICAgIHRoaXMuaXRlbURlbGV0ZS5vbmNsaWNrID0gdGhpcy5yZW1vdmVcbiAgICB9XG5cbiAgICByZW1vdmUoKXtcbiAgICAgICAgY29uc3QgeyBpZCB9ID0gdGhpcy5kYXRhO1xuICAgICAgICBjb25zdCBldmVudCA9IG5ldyBDdXN0b21FdmVudCgnZGVsZXRlX2l0ZW0nLCB7IGRldGFpbDogaWQsIGJ1YmJsZXM6IHRydWUgfSlcbiAgICAgICAgdGhpcy5lbC5kaXNwYXRjaEV2ZW50KGV2ZW50KVxuICAgIH1cblxuICAgIHVwZGF0ZShkYXRhKXtcbiAgICAgICAgdGhpcy5pdGVtTmFtZS50ZXh0Q29udGVudCA9IHRoaXMubmFtZSA9IGRhdGEubmFtZVxuICAgICAgICB0aGlzLmRhdGEgPSBkYXRhXG4gICAgfVxufVxuXG5jbGFzcyBJdGVtc0xpc3R7XG4gICAgY29uc3RydWN0b3IoKXtcbiAgICAgICAgdGhpcy5lbCA9IGVsKCcuaXRlbS1jb250YWluZXInKVxuICAgICAgICB0aGlzLmxpc3QgPSBsaXN0KHRoaXMuZWwsIEl0ZW0sICdpZCcpXG4gICAgICAgIHRoaXMuaWQgPSAwXG4gICAgICAgIHRoaXMuaXRlbXMgPSBbXVxuXG4gICAgICAgIHRoaXMuZWwuYWRkRXZlbnRMaXN0ZW5lcignZGVsZXRlX2l0ZW0nLCBlID0+IHtcbiAgICAgICAgICAgIHRoaXMuaXRlbXMgPSB0aGlzLml0ZW1zLmZpbHRlcihlbD0+ZWwuaWQgIT09IGUuZGV0YWlsKVxuICAgICAgICAgICAgdGhpcy5saXN0LnVwZGF0ZSh0aGlzLml0ZW1zKVxuICAgICAgICB9KVxuICAgIH1cblxuICAgIHVwZGF0ZShkYXRhKXtcbiAgICAgICAgaWYoZGF0YS5hY3Rpb24gPT09ICdhZGRfaXRlbScgJiYgZGF0YS5uYW1lKXtcbiAgICAgICAgICAgIHRoaXMuaXRlbXMucHVzaCh7XG4gICAgICAgICAgICAgICAgaWQgOiB0aGlzLmlkKyssXG4gICAgICAgICAgICAgICAgbmFtZSA6IGRhdGEubmFtZVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIHRoaXMubGlzdC51cGRhdGUodGhpcy5pdGVtcylcbiAgICAgICAgfVxuICAgIH1cbn1cblxuY2xhc3MgSXRlbUlucHV0IHtcbiAgICBjb25zdHJ1Y3RvcihhZGRJdGVtKXtcbiAgICAgICAgdGhpcy5wb3AgPSB0aGlzLnBvcC5iaW5kKHRoaXMpXG5cbiAgICAgICAgdGhpcy5lbCA9IGVsKCdmb3JtLml0ZW0taW5wdXQnLFxuICAgICAgICAgICAgdGhpcy5uZXdJdGVtTmFtZSA9IGVsKCdpbnB1dCcsIHt0eXBlOid0ZXh0JywgYXV0b2ZvY3VzOiB0cnVlfSksXG4gICAgICAgICAgICB0aGlzLmFkZEl0ZW1CdG4gPSBlbCgnaW5wdXQnLCB7dHlwZTonc3VibWl0JywgdmFsdWU6J0FkZCBJdGVtJ30pLFxuICAgICAgICAgICAgZWwoJ2xhYmVsJywgJ0FkZCBvbmUgb3IgbWFueSBpdGVtcyBzZXBhcmF0ZWQgYnkgY29tbWFzJylcbiAgICAgICAgKVxuXG4gICAgICAgIHRoaXMuYWRkSXRlbUJ0bi5vbmNsaWNrID0gYWRkSXRlbVxuICAgICAgICB0aGlzLmVsLm9uc3VibWl0ID0gYWRkSXRlbVxuICAgIH1cblxuICAgIHBvcCgpe1xuICAgICAgICBsZXQgdmFsdWUgPSB0aGlzLm5ld0l0ZW1OYW1lLnZhbHVlXG4gICAgICAgIHRoaXMubmV3SXRlbU5hbWUudmFsdWUgPSBcIlwiXG4gICAgICAgIHRoaXMubmV3SXRlbU5hbWUuZm9jdXMoKVxuICAgICAgICByZXR1cm4gdmFsdWVcbiAgICB9XG59XG5cbmNsYXNzIEl0ZW1QaWNrZXIge1xuICAgIGNvbnN0cnVjdG9yKCl7ICBcbiAgICAgICAgdGhpcy5hZGRJdGVtID0gdGhpcy5hZGRJdGVtLmJpbmQodGhpcylcblxuICAgICAgICB0aGlzLmVsID0gZWwoJ2Rpdi5nbG9iYWwtY29udGFpbmVyJyxcbiAgICAgICAgICAgIGVsKCdkaXYuaGVhZGVyJyxcbiAgICAgICAgICAgICAgICB0aGlzLm5ld0l0ZW1Gb3JtID0gbmV3IEl0ZW1JbnB1dCh0aGlzLmFkZEl0ZW0pXG4gICAgICAgICAgICApLFxuICAgICAgICAgICAgdGhpcy5pdGVtcyA9IG5ldyBJdGVtc0xpc3QoKSxcbiAgICAgICAgICAgIGVsKCdsYWJlbCcsIHRoaXMucmVtb3ZlUGlja2VkID0gZWwoJ2lucHV0Jywge3R5cGU6XCJjaGVja2JveFwiLCBjaGVja2VkOiBmYWxzZX0pLCAnUmVtb3ZlIHBpY2tlZCcpLFxuICAgICAgICAgICAgdGhpcy5waWNrUmFuZG9tID0gZWwoJ2J1dHRvbicsICdQaWNrIFJhbmRvbScpLFxuICAgICAgICAgICAgdGhpcy5waWNrZWRJdGVtID0gZWwoJy5waWNrZWQtaXRlbScpXG4gICAgICAgIClcblxuICAgICAgICB0aGlzLnBpY2tSYW5kb20ub25jbGljayA9IChlKT0+e1xuICAgICAgICAgICAgbGV0IHBpY2tlZEl0ZW0gPSByYW5kb20ucGljayh0aGlzLml0ZW1zLmxpc3Qudmlld3MpXG4gICAgICAgICAgICBsZXQgbmFtZSA9IChwaWNrZWRJdGVtIHx8IHt9KS5uYW1lXG4gICAgICAgICAgICBpZihwaWNrZWRJdGVtICYmIHRoaXMucmVtb3ZlUGlja2VkLmNoZWNrZWQpe1xuICAgICAgICAgICAgICAgIHBpY2tlZEl0ZW0ucmVtb3ZlKClcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoaXMucGlja2VkSXRlbS50ZXh0Q29udGVudCA9IG5hbWVcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGFkZEl0ZW0oZSl7XG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKVxuICAgICAgICB0aGlzXG4gICAgICAgIC5uZXdJdGVtRm9ybVxuICAgICAgICAucG9wKClcbiAgICAgICAgLnNwbGl0KCcsJylcbiAgICAgICAgLmZvckVhY2goaXRlbT0+e1xuICAgICAgICAgICAgdGhpcy5pdGVtcy51cGRhdGUoe1xuICAgICAgICAgICAgICAgIGFjdGlvbiA6ICdhZGRfaXRlbScsXG4gICAgICAgICAgICAgICAgbmFtZSA6IGl0ZW0udHJpbSgpXG4gICAgICAgICAgICB9KVxuICAgICAgICB9KVxuICAgICAgICBcbiAgICB9XG59XG5cbmRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ0RPTUNvbnRlbnRMb2FkZWQnLCBtYWluKVxuZnVuY3Rpb24gbWFpbigpe1xuICAgIG1vdW50KGRvY3VtZW50LmJvZHksIG5ldyBJdGVtUGlja2VyKCkpXG59Il19
