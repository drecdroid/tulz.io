const { el, mount, list, unmount}  = require('redom')
const Random = require('random-js')

let random = new Random(Random.engines.browserCrypto)

class Item {
    constructor(a,b,c,d){
        
        this.remove = this.remove.bind(this)
        this.el = el('.item',
            this.itemName = el('.item-name'),
            this.itemDelete = el('.item-delete', 'x')
        )

        this.itemDelete.onclick = this.remove
    }

    remove(){
        const { id } = this.data;
        const event = new CustomEvent('delete_item', { detail: id, bubbles: true })
        this.el.dispatchEvent(event)
    }

    update(data){
        this.itemName.textContent = this.name = data.name
        this.data = data
    }
}

class ItemsList{
    constructor(){
        this.el = el('.item-container')
        this.list = list(this.el, Item, 'id')
        this.id = 0
        this.items = []

        this.el.addEventListener('delete_item', e => {
            this.items = this.items.filter(el=>el.id !== e.detail)
            this.list.update(this.items)
        })
    }

    update(data){
        if(data.action === 'add_item' && data.name){
            this.items.push({
                id : this.id++,
                name : data.name
            })
            this.list.update(this.items)
        }
    }
}

class ItemInput {
    constructor(addItem){
        this.pop = this.pop.bind(this)

        this.el = el('form.item-input',
            this.newItemName = el('input', {type:'text', autofocus: true}),
            this.addItemBtn = el('input', {type:'submit', value:'Add Item'}),
            el('label', 'Add one or many items separated by commas')
        )

        this.addItemBtn.onclick = addItem
        this.el.onsubmit = addItem
    }

    pop(){
        let value = this.newItemName.value
        this.newItemName.value = ""
        this.newItemName.focus()
        return value
    }
}

class ItemPicker {
    constructor(){  
        this.addItem = this.addItem.bind(this)

        this.el = el('div.global-container',
            el('div.header',
                this.newItemForm = new ItemInput(this.addItem)
            ),
            this.items = new ItemsList(),
            el('label', this.removePicked = el('input', {type:"checkbox", checked: false}), 'Remove picked'),
            this.pickRandom = el('button', 'Pick Random'),
            this.pickedItem = el('.picked-item')
        )

        this.pickRandom.onclick = (e)=>{
            let pickedItem = random.pick(this.items.list.views)
            let name = (pickedItem || {}).name
            if(pickedItem && this.removePicked.checked){
                pickedItem.remove()
            }
            this.pickedItem.textContent = name
        }
    }

    addItem(e){
        e.preventDefault()
        this
        .newItemForm
        .pop()
        .split(',')
        .forEach(item=>{
            this.items.update({
                action : 'add_item',
                name : item.trim()
            })
        })
        
    }
}

document.addEventListener('DOMContentLoaded', main)
function main(){
    mount(document.body, new ItemPicker())
}