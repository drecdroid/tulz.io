#!/usr/bin/env node
const fse = require('fs-extra')

let from = process.argv[2]
let to = process.argv[3] || from

fse.copy(`${from}/public/`, `tulz.io/public/${to}/`,
    {
        recursive: true,
        overwrite: true,
    },(e)=>{
    console.log(e)
})