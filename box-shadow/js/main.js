function preventGlobalMouseEvents(){
  document.body.style.pointerEvents = 'none';
}

function restoreGlobalMouseEvents(){
  document.body.style.pointerEvents = 'auto';
}

function captureMouseEvents(e, mouseMoveListener) {
  preventGlobalMouseEvents();
  
  mouseMoveListener(e)
  function mouseupListener(e) {
    restoreGlobalMouseEvents();
    document.removeEventListener('mouseup',   mouseupListener,   true);
    document.removeEventListener('mousemove', mouseMoveListener, true);
    e.stopPropagation();
  }

  document.addEventListener('mouseup',   mouseupListener,   true);
  document.addEventListener('mousemove', mouseMoveListener, true);
  e.preventDefault();
  e.stopPropagation();
}

let throttlers = {};

function throttle( cb , limit , id , immediate = true){

  if(throttlers[id] != null){
    throttlers[id](cb)
  }
  else{
    throttlers[id] = throttler(limit)
    if(immediate){
      cb()
    }
  }  
}

function throttler(limit){
  let canCall = true

  return (cb) => {
    if(canCall){
      cb()
      canCall = false
      setTimeout(()=>canCall = true, limit)
    }
  }  
}

class EventEmitter {
    constructor(){
        this.listeners = {}
        this.subscribe = this.subscribe.bind(this)
        this.emit = this.emit.bind(this)
    }

    subscribe(event, handler){
        if(this.listeners[event] == null){
            this.listeners[event] = [handler]
        }else{
            this.listeners[event].push(handler)
        }
    }

    emit(event, changes={} ){
        let listeners = this.listeners[event]
        if(listeners){
            listeners.forEach(listener=>{
                listener(changes, this)
            })
        }
    }
}

class BoxShadow extends EventEmitter{
    constructor(x, y, blur, color, opacity, inset){
        super()
        this.update = this.update.bind(this)
        this._x = x
        this._y = y
        this._blur = blur
        this._color = color
        this._inset = inset
        this._opacity = opacity

        //this.light.style.left = `${(this.box.clientWidth-this.light.clientWidth)/2}px`
        //this.light.style.top = `${(this.box.clientHeight-this.light.clientHeight)/2}px`
    }

    update(){
        this.emit('updated')
    }

    set x(v){
        this._x = v
        this.emit('updated')
    }

    get x(){ return this._x }

    set y(v){
        this._y = v
        this.emit('updated')
    }
    
    get y(){ return this._y }

    set blur(v){
        this._blur = v
        this.emit('updated')
    }

    get blur(){ return this._blur}

    set color(v){
        this._color = v
        this.emit('updated')
    }

    get color(){ return this._color}

    set inset(v){
        this._inset = v
        this.emit('updated')
    }

    get inset(){ return this._inset }

    set opacity(v){
        this._opacity = v
        this.emit('updated')
    }

    get opacity(){ return this._opacity }
}

class BoxShadowGUI {
    constructor(){
        this.move_light = this.move_light.bind(this)
        this.redraw = this.redraw.bind(this)
        this.generate = this.generate.bind(this)

        this.box = document.getElementById('box')
        this.light = document.getElementById('light')
        this.first_half = document.getElementById('first-half')
        this.x_control = document.getElementById('x')
        this.y_control = document.getElementById('y')
        this.blur_control = document.getElementById('blur')
        this.inset_control = document.getElementById('inset')
        this.opacity_control = document.getElementById('opacity')
        this.color_control = document.getElementById('color')
        this.result = document.getElementById('result')

        this.box_data = new BoxShadow(0, 0, 10, '#000000', 100, true)
    }

    init(){
        this.box_data.subscribe('updated', this.redraw)
        this.box_data.subscribe('updated', this.generate)
        this.bind_events()
        this.box_data.update()
    }

    bind_events(){
        this.first_half.addEventListener('mousedown',(e)=>{
            captureMouseEvents(e, this.move_light, true )
        })

        this.blur_control.addEventListener('input',(e)=>{
            this.box_data.blur = e.target.value
        })

        this.inset_control.addEventListener('change', (e)=>{
            this.box_data.inset = e.target.checked
        })

        this.y_control.addEventListener('input', (e)=>{
            this.box_data.y = e.target.value
        })

        this.x_control.addEventListener('input', (e)=>{
            this.box_data.x = e.target.value
        })

        this.opacity_control.addEventListener('input', (e)=>{
            this.box_data.opacity = e.target.value
        })

        this.color_control.addEventListener('input', (e)=>{
            this.box_data.color = e.target.value
        })
    }

    move_light(e){
        let midx = this.box.clientWidth/2
        let midy = this.box.clientHeight/2

        let mx = e.clientX - this.box.offsetLeft - this.light.clientWidth/2
        let my = e.clientY - this.box.offsetTop - this.light.clientHeight/2
        
        this.light.style.left = `${mx}px`
        this.light.style.top = `${my}px`

        this.box_data.x = (this.light.offsetLeft + this.light.clientWidth/2) - midx
        this.box_data.y = (this.light.offsetTop + this.light.clientHeight/2) - midy
        
        this.x_control.value = this.box_data.x;
        this.y_control.value = this.box_data.y;
    }

    redraw(){
        throttle(()=>{
            let rgb = this.box_data.color.slice(1).match(/.{2}/g).map(c=>parseInt(c, 16)).join(',')
            //this.color.slice(1)
            //let rgb = '0,0,0'
            this.box.style.boxShadow = `rgba(${rgb},${this.box_data.opacity}) ${-this.box_data.x}px ${-this.box_data.y}px ${this.box_data.blur}px ${this.box_data.inset?"inset":""}`
        },30, 'redraw')    
    }

    generate(){
            throttle(()=>{
            let rgb = this.box_data.color.slice(1).match(/.{2}/g).map(c=>parseInt(c, 16)).join(',')
            //this.color.slice(1)
            //let rgb = '0,0,0'
            this.result.innerText = `box-shadow: rgba(${rgb},${this.box_data.opacity}) ${-this.box_data.x}px ${-this.box_data.y}px ${this.box_data.blur}px ${this.box_data.inset?"inset":""};`
        },30, 'generate')        
    }
}
let boxShadow = new BoxShadowGUI()
boxShadow.init()