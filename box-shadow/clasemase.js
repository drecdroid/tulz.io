class Peso {
    constructor(){
        this._lb = 0
        this._kg = 0

        //Para que no se desvincule el método al pasarlo a otra función
        this.clone = this.clone.bind(this)
    }

    get libras(){
        return this._lb
    }

    get kilos(){
        return this._kg
    }

    set libras(q){
        this._lb = q
        this._kg = q * 0.453592
    }

    set kilos(q){
        this._lb = q * 2.20462
        this._kg = q
    }

    clone(){
        let p = new Peso()
        peso.kilos = p.kilos
        return p
    }

}


Peso.libras = function(q){
    let peso = new Peso()
    peso.libras = q
    return peso
}

Peso.kilos = function(q){
    let peso = new Peso()
    peso.kilos = q
    return peso
}

Peso.suma = function(p1, p2){
    let res = new Peso()
    res.kg = p1.kg + p2.kg
    return res
}

let p = new Peso()
p.kilos = 5

let peso = new Peso.libras(10)
console.log(peso.libras)
console.log(peso.kilos)

let p3 = Peso.suma(p, peso)