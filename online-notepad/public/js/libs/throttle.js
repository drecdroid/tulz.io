let throttlers = {};

function throttle( cb , limit , id , immediate = true){

  if(throttlers[id] != null){
    throttlers[id](cb)
  }
  else{
    throttlers[id] = throttler(limit)
    if(immediate){
      cb()
    }
  }  
}

function throttler(limit){
  let canCall = true

  return (cb) => {
    if(canCall){
      cb()
      canCall = false
      setTimeout(()=>canCall = true, limit)
    }
  }  
}

module.exports = throttle