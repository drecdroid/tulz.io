

/**
 * JSON.parse es un método estático(es solo una función dentro de un espacio de nombres(namespace))
 * que sirve para procesar un cadena con formato JSON en un objeto javascript
 * 
 *      "objeto en formato JSON" -> { objeto de javascript }
 *
 */
// function createStopWatchEngine(){

// }

// function stopWatchTimer(){
//     console.log("mulo")
// }

// let timers = {
//     stopwatch : stopWatchTimer,
// }
let config = JSON.parse(document.getElementById('config').innerText)

let timers_data = config.timers
let menu = document.getElementById('menu')

config
.menu
.forEach(timer_name => {
    let timer_item = document.createElement('div')
    timer_item.classList.add('timer_item', 'menu-item')
    
    let timer_data

    timers_data
    .some(_timer_data=>{
        if(_timer_data.name === timer_name){
            timer_data = _timer_data
            return true
        }
    })

    if(timer_data != null){
        timer_item.dataset['name'] = timer_data.name
        timer_item.dataset['title'] = timer_data.title
        timer_item
        .style
        .background = `url("${timer_data.img}")`

        menu.appendChild(timer_item)
    }
})

function resize_wrapper(wrapper){
    let wrapper_width = innerWidth > 960 ? 960: innerWidth
    wrapper.style.width = `${wrapper_width}px`
    
    if(wrapper.id === 'wrapper-menu'){
        document
        .querySelectorAll('#menu .menu-item')
        .forEach(el=>{
            el.style.height = `${el.clientWidth*3/4}px`
        })
    }
}

function center_wrapper(wrapper){
    let wrapper_width = innerWidth > 960 ? 960: innerWidth
    wrapper.style.left = `${(innerWidth - wrapper_width)/2}px`
}

function resize_handler(){
    let wrapper = document.querySelector('.wrapper-active')
    wrapper.classList.contains('transition-wrapper') && wrapper.classList.remove('transition-wrapper')
    let wrapper_timer = document.getElementById('wrapper-timer')
    
    if(!wrapper_timer.classList.contains('wrapper-active')){
        wrapper_timer.style.left = `${innerWidth}px`
    }
    resize_wrapper(wrapper)
    center_wrapper(wrapper)
}

function view_timer(){
    let wrapper_menu = document.getElementById('wrapper-menu')
    let wrapper_timer = document.getElementById('wrapper-timer')

    wrapper_menu.classList.remove('wrapper-active')
    wrapper_menu.classList.add('transition-wrapper')
    wrapper_menu.style.left = `-${wrapper_menu.clientWidth}px`
    wrapper_menu.style.opacity = "0"

    wrapper_timer.classList.add('wrapper-active')
    resize_wrapper(wrapper_timer)
    wrapper_timer.classList.add('transition-wrapper')
    wrapper_timer.style.opacity = "100"
    center_wrapper(wrapper_timer)
}

function view_menu(){
    let wrapper_menu = document.getElementById('wrapper-menu')
    let wrapper_timer = document.getElementById('wrapper-timer')

    wrapper_menu.classList.add('wrapper-active')
    resize_wrapper(wrapper_menu)
    wrapper_menu.classList.add('transition-wrapper')
    wrapper_menu.style.opacity = "100"
    center_wrapper(wrapper_menu)

    wrapper_timer.classList.remove('wrapper-active')
    wrapper_timer.classList.add('transition-wrapper')
    wrapper_timer.style.left = `${innerWidth}px`
    wrapper_timer.style.opacity = "0"

    // hacer que desaparezca la otra pantalla
}

document
.getElementById('menu')
.addEventListener('click', (e)=>{
    if(e.target.classList.contains('menu-item')){
        view_timer()
        let timer = timers[e.target.dataset.name]
        timer != null && timer()
        //inicializar el reloj usando e.target.dataset
    }
})

document
.getElementById('wrapper-timer')
.addEventListener('click', (e)=>{
    if(e.target.classList.contains('go-menu')){
        view_menu()
    }
})


resize_handler()
window.addEventListener('resize', resize_handler)

//move_left()
//setTimeout(move_right, 500)