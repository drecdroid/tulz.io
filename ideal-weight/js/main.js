class Peso {
    constructor(){
        this._lb = 0
        this._kg = 0
    }

    get libras(){
        return this._lb
    }

    get kilos(){
        return this._kg
    }

    set libras(q){
        this._lb = q
        this._kg = q * 0.453592
    }

    set kilos(q){
        this._lb = q * 2.20462
        this._kg = q
    }

}


Peso.libras = function(q){
    let peso = new Peso()
    peso.libras = q
    return peso
}

Peso.kilos = function(q){
    let peso = new Peso()
    peso.kilos = q
    return peso
}

Peso.suma = function(p1, p2){
    let res = new Peso()
    res.kg = p1.kg + p2.kg
    return res
}



class Talla {
    constructor(){
        this._cm = 0
        this._inch = 0
        this._feet = 0

    }

    get cm(){
        return this._cm
    }

    get inch(){
        return this._inch
    }

    get feet(){
        return this._feet
    }

    get fullInch(){
        return this._feet*12 + this._inch
    }

    set cm(q){
        this._cm = q
        
        let inches = q/2.54
        this._feet = Math.floor(inches/12)
        this._inch = inches % 12
    }

    set inch(q){
        let feet = Math.floor(q/12)
        let inch = q % 12

        this._feet += feet
        this._inch = inch
        
        this._cm = this.fullInch * 2.54
    }

    set feet(q){
        this._feet = q
        this._cm = this.fullInch * 2.54
    }
}


Talla.cm = function(q){
    let talla = new Talla()
    talla.cm = q
    return talla
}

Talla.inch = function(i,f){
    let inches_from_feet = feet_to_inch(f)
    let talla = new Talla()
    talla.inch = i + inches_from_feet
    return talla 
}

class IdealWeight {
    constructor(){
        this.bindEvents = this.bindEvents.bind(this)
        this.update = this.update.bind(this)
        
        this.unit_control = document.getElementById('switch-unit')
        this.weight_control = document.getElementById('weight-box')
        this.height_control = document.getElementById('height-box')

        this.unit = 'standard'
        this.weight = new Peso()
        this.height = new Talla()

        this.weight_control.innerHTML = `
            <input type="number" min="0" class="lb" value="${this.weight.lb}"><span>lb</span>
        `

        this.height_control.innerHTML = `
            <input type="number" min="0" class="feet" value="${this.height.feet}"><span>feet</span>
            <input type="number" min="0" class="inch" value="${this.height.inch}"><span>inch</span>
        `


        this.bindEvents()
    }

    bindEvents(){
        this.unit_control.addEventListener('change', (e)=>{
            if(e.target.checked===true){
                this.unit = "metric"
                this.weight_control.innerHTML = `
                    <input type="number" min="0" class="kg" value="${this.weight.kilos}"><span>kg</span>
                    `
             }
            else{
                 this.unit = "standard"
                 this.weight_control.innerHTML = `
                    <input type="number" min="0" class="lb" value="${this.weight.libras}"><span>lb</span>
                `
                this.update()
            }
        })

        this.unit_control.addEventListener('change', (e)=>{
            if(e.target.checked===true){
                this.unit = "metric"
                this.height_control.innerHTML = `
                    <input type="number" min="0" class="cm" value="${this.height.cm}"><span>cm</span>
                `
            }
            else{
                this.unit = "standard"
                this.height_control.innerHTML = `
                    <input type="number" min="0" class="feet" value="${this.height.feet}"><span>feet</span>
                    <input type="number" min="0" class="inch" value="${this.height.inch}"><span>inch</span>
                `
            }

            this.update()
        })

        this.weight_control.addEventListener('input', (e)=>{
            if(e.target.classList.contains('lb')){
                let lb = e.target.parentElement.querySelector('.lb')

                this.weight.libras = Number.parseInt(lb.value)
                this.update()
            }
            else{ e.target.parentElement.querySelector('.kg')
                this.weight.kilos = e.target.value
                this.update()
            }

            
        })

        this.height_control.addEventListener('input', (e)=>{
            if(e.target.classList.contains('feet') || e.target.classList.contains('inch')){
                let feet = e.target.parentElement.querySelector('.feet')
                let inch = e.target.parentElement.querySelector('.inch')
                
                this.height.feet = Number.parseInt(feet.value)
                this.height.inch = Number.parseInt(inch.value)
                
                if( e.target.classList.contains('inch') ){
                    feet.value = this.height.feet
                    inch.value = this.height.inch
                }

                this.update()
            }   
            else if(e.target.classList.contains('cm')){
                this.height.cm = e.target.value
                this.update()
            }
        })
    }

    update(){

    }
}

function feet_to_inch(feet){
    return feet*12
}

function inches_to_cm(inches){
    return inches*2.54
}

function cm_to_inches(cm){
    return cm/2.54
}

function feetinches_to_cm(feet, inches){
    return (feet*12+inches)*2.54
}

function kg_to_lb(kg){
    return kg*2.20462
}

function lb__to_kg(lb){
    return lb/2.20462
}

function devine(u, g, h, w, a){
    
}


let idealWeight = new IdealWeight()